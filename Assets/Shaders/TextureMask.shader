﻿﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Custom/MaskedTexture"
{
    Properties
    {
      _MainTex ("Base (RGB)", 2D) = "white" {}
      _Mask ("Culling Mask", 2D) = "white" {}
      _Cutoff ("Alpha Cutoff", Range (0,1)) = 0.1
      _Stencil ("Stencil Reference", Int) = 0
    }
    SubShader
    {
        Tags
        { 
            "Queue"="Transparent" 
            "IgnoreProjector"="True" 
            "RenderType"="Transparent" 
            "PreviewType"="Plane"
            "CanUseSpriteAtlas"="True"
        }

      Lighting Off
      ZWrite Off
      Cull Off
      ZTest [unity_GUIZTestMode]

      Blend SrcAlpha OneMinusSrcAlpha

      Pass
      {
        Stencil {
            Ref [_Stencil]
            Comp equal
            Pass keep
        }

         SetTexture [_Mask] {combine texture}
         SetTexture [_MainTex] {combine texture, previous}
      }
    }
}