﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DialogView : MonoBehaviour
{

    [SerializeField]
    GameObject content;

    [SerializeField]
    Text text;

    [SerializeField]
    GameObject okButton;

    [SerializeField]
    GameObject cancelButton;

    static DialogView instance;
    static Action onOkCallback;
    static Action onCancelCallback;

    void Awake()
    {
        instance = this;
        content.SetActive(false);
    }

    public static void Show(string message, Action okCallBack = null, Action cancelCallBack = null)
    {
        // disable graphic raycaster for header
        HeaderView.DisableGraphicRaycaster();

        instance.text.text = message;
        instance.content.SetActive(true);

        if (cancelCallBack != null)
        {
            onCancelCallback = cancelCallBack;
            instance.ShowWithCancelButton();
        }
        else instance.ShowOkButtonOnly();

        onOkCallback = okCallBack;
        PageManager.CurrentPage.HideAllNativeEditBox();
    }

    public void OnOk()
    {
        content.SetActive(false);

        PageManager.CurrentPage.ShowAllNativeEditBox();
        if (onOkCallback != null)
        {
            onOkCallback.Invoke();
        }
        // enable graphic raycaster for header
        HeaderView.EnableGraphicRaycaster();

        content.SetActive(false);
    }

    public void OnCancel()
    {
        onCancelCallback.Invoke();

        content.SetActive(false);
        HeaderView.EnableGraphicRaycaster();
        PageManager.CurrentPage.ShowAllNativeEditBox();
    }

    void ShowWithCancelButton()
    {
        cancelButton.SetActive(true);
    }

    void ShowOkButtonOnly()
    {
        cancelButton.SetActive(false);
    }
    public static bool IsShowing()
    {
        return instance.content.activeInHierarchy;
    }
}
