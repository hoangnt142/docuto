﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderMenuView : View
{

    [SerializeField]
    Text title;

    [SerializeField]
    Image backgroundImage;

    [SerializeField]
    GameObject instructorMenu;

    [SerializeField]
    GameObject noticeBadge;

    [SerializeField]
    GameObject switchButton;

    [SerializeField]
    Text switchButtonText;

    [SerializeField]
    Image switchButtonIcon;

    [SerializeField]
    Sprite iconStudentMenu;

    [SerializeField]
    Sprite iconInstructorMenu;

    MenuType currentMenuType;

    public void Show(string titleText = "", MenuType menuType = MenuType.None)
    {
        base.Show();
        if(menuType != MenuType.Both) 
        {
            currentMenuType = menuType;
        }           

        if (titleText == "") {
            title.gameObject.SetActive (false);
        } else {
            title.gameObject.SetActive (true);
            title.text = titleText;
        }

        // hide instructor menu if user is not registerd as teacher
        if (!Session.IsInstructor())
        {
            instructorMenu.SetActive(false);
        }
        else
        {
            instructorMenu.SetActive(true);
        }

        switch (menuType)
        {
            case MenuType.None:
                break;

            case MenuType.CommonUser:
                backgroundImage.color = ColorManager.CommonUserColor;
                switchButtonText.color = ColorManager.InstructorColor;
                switchButtonText.text = "先生メニュー";
                switchButtonIcon.sprite = iconInstructorMenu;

                break;

            case MenuType.Instructor:
                backgroundImage.color = ColorManager.InstructorColor;
                switchButtonText.color = ColorManager.CommonUserColor;
                switchButtonText.text = "生徒メニュー";
                switchButtonIcon.sprite = iconStudentMenu;

                break;
        }

        //Update notive badge
        NoticeAPI.GetUnreadNumber(menuType == MenuType.Instructor, this.GetUnreadNoticeNumberCallBack);
    }

    void GetUnreadNoticeNumberCallBack(bool isSuccess, NoticeBadgeData data)
    {
        if (isSuccess && data.num > 0)
        {
            noticeBadge.SetActive(true);
            Transform tranfNoticeBadge = noticeBadge.transform.Find("Text");
            tranfNoticeBadge.GetComponent<Text>().text = data.num.ToString();
        }
        else
        {
            noticeBadge.SetActive(false);
        }
    }

    public void ShowInstructorMenu()
    {
        // show my lesson
    }


    public void SwitchUserState()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        switch (currentMenuType)
        {
            case MenuType.None:
                break;

            case MenuType.CommonUser:
                PageManager.Show(PageType.InstructorMyCourse);
                break;

            case MenuType.Instructor:
                PageManager.Show(PageType.CourseList);                
                break;
        }
    }
}
