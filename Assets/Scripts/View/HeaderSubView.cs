﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class HeaderSubView : View
{

    public enum ButtonType
    {
        none,
        arrow,
        close
    }

    [SerializeField]
    Text titleText;

    [SerializeField]
    Image buttonImage;

    [SerializeField]
    Sprite xSprite;

    [SerializeField]
    Sprite arrowSprite;

    [SerializeField]
    Image backgroundImage;

    private ButtonType currentButtonType;

    public Action onClicked;

    public void Show (string title,
                      ButtonType buttonType,
                      MenuType menuType)
    {
        base.Show ();
        titleText.text = title;
        currentButtonType = buttonType;

        switch (buttonType) {
        case ButtonType.none:
            buttonImage.gameObject.SetActive (false);
            break;

        case ButtonType.arrow:
            buttonImage.gameObject.SetActive (true);
            buttonImage.sprite = arrowSprite;
            break;

        case ButtonType.close:
            buttonImage.gameObject.SetActive (true);
            buttonImage.sprite = xSprite;
            break;
        }

        switch (menuType) {
        case MenuType.None:
            backgroundImage.color = Color.white;
            titleText.color = Color.black;
            buttonImage.color = Color.black;
            break;

        case MenuType.CommonUser:
            backgroundImage.color = ColorManager.CommonUserColor;
            titleText.color = Color.white;
            buttonImage.color = Color.white;
            break;

        case MenuType.Instructor:
            backgroundImage.color = ColorManager.InstructorColor;
            titleText.color = Color.white;
            buttonImage.color = Color.white;
            break;
        case MenuType.Both:
            titleText.color = Color.white;
            buttonImage.color = Color.white;
            break;
        }

        if (onClicked != null)
        {
            if (titleText.gameObject.GetComponent<Button>() == null)
                titleText.gameObject.AddComponent<Button>();
            Button headerButton = titleText.gameObject.GetComponent<Button>();

            headerButton.onClick.RemoveAllListeners();
            headerButton.onClick.AddListener(delegate { onClicked(); });
        }
        else
        {
            // remove button component
            if (titleText.gameObject.GetComponent<Button>())
                Destroy(titleText.gameObject.GetComponent<Button>());
        }
    }

    #if UNITY_ANDROID
        void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                if (currentButtonType != ButtonType.none && !DialogView.IsShowing() && !CropImageModal.IsShowing())
                {
                    PageManager.GoBackPage();
                }
            }
        }
    #endif
}
