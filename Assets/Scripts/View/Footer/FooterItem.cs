﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FooterItem : MonoBehaviour
{
    [SerializeField]
    Image itemImage;

    [SerializeField]
    Text itemText;

    [SerializeField]
    PageType page;

    [SerializeField]
    MenuType menu;

    [SerializeField]
    public bool isPublic;

    [SerializeField]
    public List<PageType> referencePages = new List<PageType>();

    public void ShowPage()
    {
        PageManager.Show(page, menu);
    }

    public void Active(Color color)
    {
        itemImage.color = color;
        itemText.color = color;
    }

    public void Deactive(Color color)
    {
        itemImage.color = color;
        itemText.color = color;
    }
}
