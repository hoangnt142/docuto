﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FooterView : MonoBehaviour
{

    [SerializeField]
    GameObject commonUserContent;

    [SerializeField]
    GameObject instructorContent;

    [SerializeField]
    Color32 activeColor;

    [SerializeField]
    Color32 instructorActiveColor;

    [SerializeField]
    Color32 deactiveColor;

    [SerializeField]
    List<FooterItem> commonUserItems;

    [SerializeField]
    List<FooterItem> instructorItems;

    [SerializeField]
    FooterItem initialItem;

    [SerializeField]
    GameObject instructorBadge;

    [SerializeField]
    GameObject commonUserBadge;

    private FooterItem currentItem;

    static FooterView instance;

    static MenuType currentMenuType;

    public static bool IsInstructorMenu {
        get {
            return currentMenuType == MenuType.Instructor;
        }
    }

    void Awake ()
    {
        instance = this;
        InitialItems();
        Hide();
    }

    public static FooterView GetInstance()
    {
        return instance;
    }
    
    public static void Show(MenuType menuType = MenuType.None)
    {
        if(menuType != MenuType.Both)
        {
            currentMenuType = menuType;
        }           

        switch (currentMenuType)
        {
            case MenuType.None:
                break;

            case MenuType.CommonUser:
                instance.commonUserContent.gameObject.SetActive(true);
                instance.instructorContent.gameObject.SetActive(false);
                break;

            case MenuType.Instructor:
                instance.commonUserContent.gameObject.SetActive(false);
                instance.instructorContent.gameObject.SetActive(true);
                break;
        }
        
    }

    static void OnSuccessGetUnreadNum(bool isInstructorMenu,int unreadNum)
    {
        if(unreadNum <= 0)
        {
            instance.commonUserBadge.SetActive(false);
            instance.instructorBadge.SetActive(false);
        }
        else
        {
            GameObject badge = isInstructorMenu ? instance.instructorBadge : instance.commonUserBadge;
            badge.SetActive(true);
            Text unreadText = badge.GetComponentInChildren<Text>(true);
            unreadText.text = unreadNum.ToString();
        }
    }
    static void OnErrorGetUnreadNum()
    {
        instance.commonUserBadge.SetActive(false);
        instance.instructorBadge.SetActive(false);
    }


    public static void Hide()
    {
        instance.commonUserContent.gameObject.SetActive(false);
        instance.instructorContent.gameObject.SetActive(false);
    }

    public void ShowItem(FooterItem item)
    {
        // check authentication
        if (!item.isPublic && !Authenticator.Validate())
        {
            return;
        }

        currentItem.Deactive(deactiveColor);

        Color color = activeColor;
        if (isIntructorItem(item)) color = instructorActiveColor;

        item.Active(color);
        item.ShowPage();
        currentItem = item;
    }

    private void InitialItems()
    {
        DeactiveAll();

        // by default, active Home item menu
        if (initialItem != null)
        {
            Color color = activeColor;
            if (isIntructorItem(initialItem)) color = instructorActiveColor;

            initialItem.Active(color);
            currentItem = initialItem;
        }
    }

    public void DeactiveAll()
    {
        foreach (FooterItem item in commonUserItems)
        {
            item.Deactive(deactiveColor);
        }

        foreach (FooterItem item in instructorItems)
        {
            item.Deactive(deactiveColor);
        }
    }

    public static void Reload(PageType page, MenuType type)
    {
        FooterItem selectedItem = null;

        // call get unread_enum whenever page changed
        bool isInstructorMenu = (type == MenuType.Both) ? IsInstructorMenu : (type == MenuType.Instructor) ? true : false;
        ChatAPI.GetUnreadNum(isInstructorMenu, OnSuccessGetUnreadNum, OnErrorGetUnreadNum);

        if (type == MenuType.Both) return;

        if (selectedItem == null && (type == MenuType.CommonUser || type == MenuType.None))
            selectedItem = instance.commonUserItems.Find(i => i.referencePages.Contains(page));

        if (selectedItem == null && (type == MenuType.Instructor || type == MenuType.None))
            selectedItem = instance.instructorItems.Find(i => i.referencePages.Contains(page));

        if (selectedItem != null)
        {
            instance.currentItem.Deactive(instance.deactiveColor);

            Color color = instance.activeColor;
            if (instance.isIntructorItem(selectedItem)) color = instance.instructorActiveColor;

            selectedItem.Active(color);
            instance.currentItem = selectedItem;
        }
        else
            instance.DeactiveAll();
    }

    bool isIntructorItem(FooterItem item)
    {
        return instructorItems.Contains(item);
    }
}
