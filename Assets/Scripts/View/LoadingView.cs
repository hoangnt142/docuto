﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LoadingView : MonoBehaviour {

    [SerializeField]
    GameObject content;

    static LoadingView instance;

    void Awake ()
    {
        instance = this;
        content.SetActive (false);
    }

    public static void Show ()
    {
        PageManager.ActiveBackPage(false);
        instance.content.SetActive (true);
        HeaderView.DisableGraphicRaycaster();
    }

    public static void Hide ()
    {
        PageManager.ActiveBackPage(true);
        instance.content.SetActive (false);
        HeaderView.EnableGraphicRaycaster();
    }

}
