﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CourseTableScrollerItem : MonoBehaviour, IScrollerItem
{
    public CourseListData data;

    [SerializeField]
    GameObject content, courseNode, loadingPanel;

    [SerializeField]
    Text messenger;

    [SerializeField]
    Color emptyColor;

    bool isActive;
    public bool IsActive
    {
        get { return isActive; }
    }

    List<GameObject> emptyItems = new List<GameObject>();

    public void Setup(IScrollerItemData _data)
    {
        data = (CourseListData)_data;

        loadingPanel.SetActive(false);
        if (!string.IsNullOrEmpty(data.message))
        {
            messenger.text = data.message;
            messenger.gameObject.SetActive(true);
            content.SetActive(false);
            GetComponent<Image>().color = emptyColor;
        }
        else
        {
            messenger.gameObject.SetActive(false);
            GetComponent<Image>().color = Color.white;
            content.SetActive(true);
            foreach (CourseData courseData in data.list)
            {
                var courseNodePrefab = Instantiate(courseNode, content.transform);
                CourseNodeView courseNodeView = courseNodePrefab.GetComponent<CourseNodeView>();
                courseNodeView.UpdateView(courseData);
            }
            //CategoryName.text = data.id;
        }
    }

    public void Setup(IScrollerItemData _data, bool clear)
    {
        if(clear)
            foreach (Transform child in content.transform)
                Destroy(child.gameObject);

        Setup(_data);
    }

    public void ShowLoading(bool isShow)
    {
        if (isShow) loadingPanel.SetActive(true);
        else loadingPanel.SetActive(false);
    }

    public void Activate()
    {
        isActive = true;
    }

    public void Deactivate()
    {
        isActive = false;
    }
}
