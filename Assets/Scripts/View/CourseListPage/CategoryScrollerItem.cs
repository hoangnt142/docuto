﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryScrollerItem : MonoBehaviour, IScrollerItem
{
    public CourseCategoryData data;
    public Text CategoryName;
    public Image Bg;

    bool isActive;
    public bool IsActive
    {
        get { return isActive; }
    }

    public void Setup(IScrollerItemData _data)
    {
        data = (CourseCategoryData)_data;
        CategoryName.text = data.name;
    }

    public void Activate()
    {
        Bg.color = ColorManager.CommonUserColor;
        CategoryName.color = new Color(1, 1, 1);

        isActive = true;
    }

    public void Deactivate()
    {
        Bg.color = new Color(1, 1, 1);
        CategoryName.color = ColorManager.CommonUserColor;

        isActive = false;
    }
}