﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CategoryView : MonoBehaviour
{
    public CourseListPage Controller;
    public HorizontalScroller scroller;

    List<IScrollerItemData> data;
    int currentCategoryId;
    int favoriteCategoryId;

    void Start()
    {
        scroller.OnSelectedItem = OnActivated;
        scroller.OnStartScroll = OnStartScroll;
        scroller.OnEndScroll = OnEndScroll;
        scroller.onItemClick = OnItemClick;
    }

    public void Reload()
    {
        LoadData();

        currentCategoryId = 0;
        Controller.courseCategoryData.Clear();
        foreach (CourseCategoryData _data in data)
        {
            Controller.courseCategoryData.Add(_data);
        }
    }

    private void LoadData()
    {
        data = new List<IScrollerItemData>();
        foreach (CourseCategoryData _data in Controller.courseCategoryData)
        {
            data.Add(_data);
        }

        //Add favorite category
        if (Session.HasToken())
        {
            CourseCategoryData favCategory = new CourseCategoryData();
            favCategory.id = favoriteCategoryId = data.Count + 1;
            favCategory.name = "お気に入り";
            favCategory.isFavorite = true;
            data.Add(favCategory);
        }

        scroller.LoadData(data);
        scroller.Reset();

        if (data.Count <= 4)
        {
            this.GetComponent<ScrollRect>().enabled = false;
        }
        else
        {
            this.GetComponent<ScrollRect>().enabled = true;
        }
    }

    public void OnActivated(IScrollerItem iItem)
    {
        CategoryScrollerItem item = (CategoryScrollerItem)iItem;


        if(favoriteCategoryId == item.data.id)
        {
            Controller.LoadFavoriteCourses(favoriteCategoryId);
            currentCategoryId = item.data.id;
            return;
        }

        if (currentCategoryId != item.data.id)
        {
            Controller.LoadCourseData(item.data.id);
            currentCategoryId = item.data.id;
        }
    }

    public void ActiveItem(int index)
    {
        scroller.GotoItem(index);
    }

    public void StartDrag()
    {
        Controller.DisableSwipe();
        scroller.StartDrag();
    }

    public void EndDrag()
    {
        Controller.EnableSwipe();
        scroller.EndDrag();
    }

    public void OnStartScroll()
    {
        Controller.SetCourseListOpaque(true);
    }

    public void OnEndScroll()
    {
        Controller.SetCourseListOpaque(false);
    }

    public void OnItemClick(IScrollerItem iItem)
    {
        CategoryScrollerItem item = (CategoryScrollerItem)iItem;

        ActiveItem( data.FindIndex((c) => ((CourseCategoryData) c).id == item.data.id) );
    }
}
