﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;

public class CourseTableView : MonoBehaviour
{
    [SerializeField]
    HorizontalScroller courseScroller;

    [SerializeField]
    GameObject opaqueContent;

    Dictionary<int, CourseListData> poolData = new Dictionary<int, CourseListData>();
    List<IScrollerItemData> data;
    CourseTableScrollerItem currentActiveItem;
    CourseListPage controller;
    ScrollRect scroller;
    CourseTableScrollerItem scrollerItem;

    int currentCategoryId;
    bool isGettingNextPage;
    bool isRefreshing;

    void Start()
    {
        courseScroller.OnSelectedItem = OnActivated;
    }

    private void Update()
    {
        if (scroller == null || scrollerItem == null) return;

        if (!isGettingNextPage && scroller.content.anchoredPosition.y - scroller.content.sizeDelta.y >=200)
        {
            if (controller != null && controller.HasNextPage(currentCategoryId))
            {
                isGettingNextPage = true;
                StartCoroutine(GetNextPage());
            }

        }
        if (controller != null && !isRefreshing && scroller.content.anchoredPosition.y <= -150f)
        {
            isRefreshing = true;
            controller.ReLoadCategory();
        }
        //if(isGettingNextPage && scroller.verticalNormalizedPosition > 0.5f)
        //isGettingNextPage = false;
    }

    IEnumerator GetNextPage()
    {
        scroller.content.GetComponent<GridLayoutGroup>().padding.bottom = 210;
        LayoutRebuilder.ForceRebuildLayoutImmediate(scroller.content);
        yield return new WaitForEndOfFrame();
        scroller.verticalNormalizedPosition = 0f;
        scroller.vertical = false;

        scrollerItem.ShowLoading(true);

        controller.NextPage();
    }

    public void Append(List<CourseData> data)
    {
        CourseListData _data = new CourseListData();
        _data.id = currentCategoryId;
        _data.list = data;

        if (poolData.ContainsKey(currentCategoryId))
        {
            CourseListData inPoolData = poolData[currentCategoryId];
            if (inPoolData.list == null)
                inPoolData.list = new List<CourseData>();
            if (!String.IsNullOrEmpty(inPoolData.message))
                inPoolData.message = "";
            
            inPoolData.list.AddRange(data);

            poolData.Remove(currentCategoryId);
            poolData.Add(currentCategoryId, inPoolData);
        }
        else
            poolData.Add(currentCategoryId, _data);

        scrollerItem.Setup(_data);

        // end of get nexpage process
        EndLoadNextPage();
    }

    public void Refresh(List<CourseData> data, int categoryId = 0)
    {
        if (categoryId == 0) categoryId = currentCategoryId;

        CourseListData _data = new CourseListData();
        _data.id = categoryId;
        _data.list = data;
        if (poolData.ContainsKey(categoryId))
        {
            poolData.Remove(categoryId);
            poolData.Add(categoryId, _data);
        }

        int findIdx = this.data.FindIndex((IData) => (((CourseListData)IData).id == categoryId));
        CourseTableScrollerItem item = (CourseTableScrollerItem)courseScroller.GetItem(findIdx);
        item.Setup(_data, true);
    }

    public void Refresh(String message, int categoryId = 0)
    {
        if (categoryId == 0) categoryId = currentCategoryId;

        CourseListData _data = new CourseListData();
        _data.id = categoryId;
        _data.message = message;
        if (poolData.ContainsKey(categoryId))
        {
            poolData.Remove(categoryId);
            poolData.Add(categoryId, _data);
        }

        int findIdx = this.data.FindIndex((IData) => (((CourseListData)IData).id == categoryId));
        CourseTableScrollerItem item = (CourseTableScrollerItem)courseScroller.GetItem(findIdx);
        item.Setup(_data, true);
    }

    public void EndLoadNextPage()
    {
        scrollerItem.ShowLoading(false);
        isGettingNextPage = false;
        scroller.content.GetComponent<GridLayoutGroup>().padding.bottom = 0;
        scroller.vertical = true;
        LayoutRebuilder.ForceRebuildLayoutImmediate(scroller.content);
    }

    public void Reload()
    {
        data = new List<IScrollerItemData>();
        foreach (CourseListData _poolData in poolData.Values)
        {
            data.Add(_poolData);
        }

        courseScroller.LoadData(data);
    }

    public void AddToPool<T>(int categoryId, T theData)
    {
        if (poolData.ContainsKey(categoryId))
        {
            poolData.Remove(categoryId);
        }

        CourseListData scrollerItemData = new CourseListData();
        scrollerItemData.id = categoryId;
        if (typeof(T) == typeof(List<CourseData>))
            scrollerItemData.list = (List<CourseData>)Convert.ChangeType(theData, typeof(T));
        else
            scrollerItemData.message = theData.ToString();

        poolData.Add(categoryId, scrollerItemData);
    }

    public void Add<T>(int categoryId, T theData)
    {
        AddToPool(categoryId, theData);

        Reload();
    }

    public void ClearData()
    {
        if (poolData.Count > 0)
        {
            poolData.Clear();
            Reload();
            courseScroller.Reset(false);
            CourseDetailPage.ClearStack();  
        }
    }

    public void ActiveTable(int categoryId)
    {
        int findIdx = data.FindIndex((IData) => ( ((CourseListData)IData).id == categoryId) );

        currentCategoryId = categoryId;
        courseScroller.GotoItem(findIdx);

        scroller = courseScroller.GetSelected().GetComponent<ScrollRect>();
        scrollerItem = (CourseTableScrollerItem)courseScroller.GetSelectedItem();
    }

    public void OnActivated(IScrollerItem iItem)
    {
        if(currentActiveItem != null) currentActiveItem.Deactivate();

        CourseTableScrollerItem item = (CourseTableScrollerItem)iItem;
        item.Activate();
        currentActiveItem = item;
    }

    public void SetOpaque(bool isActive)
    {
        opaqueContent.SetActive(isActive);
    }

    public void SetController(CourseListPage control)
    {
        controller = control;
    }
    public void SetIsRefreshing(bool refreshing)
    {
        isRefreshing = refreshing;
    }
}