﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Webview : MonoBehaviour
{
    static Webview instance;

    WebViewObject webViewObject;
    string url;

    void Awake()
    {
        instance = this;
        gameObject.SetActive(false);
    }

    public static void Show(string link)
    {
        instance.url = link;

        //webViewObject.enabled = true;
        instance.webViewObject = instance.gameObject.AddComponent<WebViewObject>();
        instance.webViewObject.Init(
            cb: (msg) =>
            {
                Debug.Log(string.Format("CallFromJS[{0}]", msg));
                // close webview
                if(msg.ToLower().Equals("close"))
                {
                    instance.Close();
                }
                
                //if(msg.ToLower().Equals("play"))
                //{
                //    AndroidSystem.statusBarState = AndroidSystem.States.Hidden;
                //}
            },
            err: (msg) =>
            {
                Debug.Log(string.Format("CallOnError[{0}]", msg));

                AndroidSystem.statusBarState = AndroidSystem.States.Visible;
                instance.Close();
                DialogView.Show("Unexpected error. Please contact to admin.", () => { });
            },
            ld: (msg) =>
            {
                Debug.Log(string.Format("CallOnLoaded[{0}]", msg));
                AndroidSystem.statusBarState = AndroidSystem.States.Hidden;

                #if !UNITY_ANDROID || UNITY_EDITOR
                    instance.webViewObject.EvaluateJS(@"
                      window.Unity = {
                        call: function(msg) {
                          window.location = 'unity:' + msg;
                        }
                      }
                    ");
                #endif
            }
        );

        instance.webViewObject.LoadURL(link);
        instance.webViewObject.SetMargins(0, 0, 0, 0);
        instance.webViewObject.SetVisibility(true);

        instance.gameObject.SetActive(true);
        HeaderView.Hide();
        HeaderView.DisableGraphicRaycaster();
        PageManager.CurrentPage.HideAllNativeEditBox();
    }

    public static void Hide()
    {
        AndroidSystem.statusBarState = AndroidSystem.States.Visible;
        HeaderView.Show();

        instance.url = null;

        instance.webViewObject.SetVisibility(false);
        Destroy(instance.webViewObject);

        HeaderView.EnableGraphicRaycaster();
        PageManager.CurrentPage.ShowAllNativeEditBox();
        instance.gameObject.SetActive(false);
    }

    public void Close()
    {
        Hide();
    }
}
