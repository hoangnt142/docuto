﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HeaderView : MonoBehaviour
{
    static HeaderView instance;

    [SerializeField]
    HeaderMenuView menuView;

    [SerializeField]
    HeaderSubView subView;

    [SerializeField]
    GameObject fullContent;

    [SerializeField]
    GameObject content;

    [SerializeField]
    bool isMenu;

    GraphicRaycaster gRaycaster;
    Action onClicked;
    MenuType currentMenuType;
    Action onHeaderIconLicked;

    public static HeaderView Instance
    {
        get
        {
            return instance;
        }
    }

    void Awake()
    {
        instance = this;
        gRaycaster = gameObject.GetComponent<GraphicRaycaster>();
        currentMenuType = MenuType.CommonUser;
        ResetIconButtonEvent();
#if UNITY_ANDROID || UNITY_IOS
        // show status bar on ANDROID and IOS
        float statusBarHeight = 36;
        RectTransform headerRect = gameObject.GetComponent<RectTransform>();
        RectTransform fullContentRect = fullContent.GetComponent<RectTransform>();
        RectTransform contentRect = content.GetComponent<RectTransform>();
        RectTransform menuViewRect = menuView.gameObject.GetComponent<RectTransform>();
        RectTransform subMenuViewRect = subView.gameObject.GetComponent<RectTransform>();

        headerRect.sizeDelta = new Vector2(headerRect.sizeDelta.x, headerRect.sizeDelta.y + statusBarHeight);
        fullContentRect.offsetMax = new Vector2(fullContentRect.offsetMax.x, fullContentRect.offsetMax.y - statusBarHeight);
        contentRect.offsetMax = new Vector2(contentRect.offsetMax.x, contentRect.offsetMax.y - statusBarHeight);
        menuViewRect.offsetMax = new Vector2(menuViewRect.offsetMax.x, menuViewRect.offsetMax.y - statusBarHeight);
        subMenuViewRect.offsetMax = new Vector2(subMenuViewRect.offsetMax.x, subMenuViewRect.offsetMax.y - statusBarHeight);
#endif
    }

    public static void ShowMenu(string titleText = "", MenuType menuType = MenuType.None)
    {
        instance.menuView.Show(titleText, menuType);
        instance.currentMenuType = menuType;
        instance.subView.Hide();
    }

    public static void ShowSub(string title, HeaderSubView.ButtonType buttonType = HeaderSubView.ButtonType.arrow, MenuType menuType = MenuType.None)
    {
        instance.menuView.Hide();
        instance.subView.onClicked = instance.onClicked;
        instance.subView.Show(title, buttonType, menuType);
        instance.currentMenuType = menuType;
        Debug.Log("show sub");
    }

    public void GoBackPage()
    {
        if(instance.onHeaderIconLicked != null)
        {
            instance.onHeaderIconLicked();
        }
    }

    public void ShowNoticeList()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        PageManager.Show(PageType.NoticeList, currentMenuType);
    }
    public static void SwitchUserState()
    {
        instance.menuView.SwitchUserState();
    }

    public static void EnableGraphicRaycaster()
    {
        instance.gRaycaster.enabled = true;
    }

    public static void DisableGraphicRaycaster()
    {
        instance.gRaycaster.enabled = false;
    }

    public static void SetEvent(Action clickEvent)
    {
        instance.onClicked = clickEvent;
    }

    public static void RemoveEvent()
    {
        instance.onClicked = null;
    }

    public static void SetIconButtonEvent(Action clickEvent)
    {
        instance.onHeaderIconLicked = clickEvent;
    }

    public static void ResetIconButtonEvent()
    {
        instance.onHeaderIconLicked = PageManager.GoBackPage;
    }

    public static void Show()
    {
        instance.gameObject.SetActive(true);
    }

    public static void Hide()
    {
        instance.gameObject.SetActive(false);
    }
}
