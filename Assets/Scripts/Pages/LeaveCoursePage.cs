﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LeaveCoursePage : Page
{

    public override void Show ()
    {
        base.Show ();
        HeaderView.ShowSub ("退会手続き", HeaderSubView.ButtonType.close, MenuType);
    }

    public override void Hide ()
    {
        base.Hide ();
    }
}
