﻿using UnityEngine;

public class SelectPicturePage : Page {

    [SerializeField]
    BeneFrameButton[] frames;

    Sprite selectedSprite;
    InstructorEditCoursePage controller;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("画像選択",HeaderSubView.ButtonType.close,MenuType);
        Reset();
    }

    public void OnSelectPicture(BeneFrameButton selectedFrame)
    {
        Reset();
        selectedFrame.ShowSelectedFrame();
        selectedSprite = selectedFrame.GetImage();
    }

    public void SetController(InstructorEditCoursePage page)
    {
        controller = page;
    }

    private void Reset()
    {
        selectedSprite = null;
        foreach (var frame in frames)
        {
            frame.HideSelectedFrame();
        }
    }

    public void OnSelectButtonClick()
    {
        controller.SetCourseImage(selectedSprite);
        PageManager.GoBackPage();
    }
}
