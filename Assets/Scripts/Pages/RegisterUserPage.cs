﻿﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class RegisterUserPage : Page
{

    [SerializeField]
    InputField nameField;

    [SerializeField]
    InputField emailField;

    [SerializeField]
    InputField passwordField;

    int sexType = -1;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("会員登録", HeaderSubView.ButtonType.none, MenuType);

        // Clear PlayerPref one user back to Register page
        Session.Token = "";
    }

    public void Register()
    {
        string emptyFieldNames = "";
        if (string.IsNullOrEmpty(nameField.text))
        {
            emptyFieldNames += "名前";
        }

        if (string.IsNullOrEmpty(emailField.text))
        {
            emptyFieldNames += (emptyFieldNames != "") ? "とメールアドレス" : "メールアドレス";
        }

        if (string.IsNullOrEmpty(passwordField.text))
        {
            emptyFieldNames += (emptyFieldNames != "") ? "とパスワード" : "パスワード";
        }

        if(emptyFieldNames != "")
        {
            DialogView.Show(emptyFieldNames + "を入力して下さい");
            return;
        }

        if (passwordField.text.Length < 6)
        {
            DialogView.Show("パスワードは6文字以上入力して下さい");
            return;
        }

        if (passwordField.text.Length > 128)
        {
            DialogView.Show("パスワードは128文字以下で入力して下さい");
            return;
        }

        if (sexType == -1) {
            DialogView.Show("性別を登録してください");
            return;
        }

        RegisterAPI.Register(
            nameField.text,
            sexType,
            emailField.text,
            passwordField.text,
            OnProcessRegister);
        LoadingView.Show();
    }

    public void ShowLoginPage()
    {
        PageManager.Show(PageType.Login);
    }

    public void SkipLoginPage()
    {
        PageManager.Show(PageType.CourseList);
    }

    public void RegisterFacebook()
    {
        Authenticator.OnLoggedInSuccess = this.OnFacebookLoginSuccess;
        Authenticator.OnLoggedInFail = this.OnFacebookLoginFail;
        Authenticator.LoginWithFacebook();
    }

    public void OnFacebookLoginSuccess()
    {
        if (FB.IsLoggedIn)
        {
            LoadingView.Show();

            // get facebook's user information
            FacebookConnection.GetData(ProcessFacebooksProfileData, "first_name", "last_name", "email");
            // FacebookConnection.GetImage(ProcessFacebooksProfilePicture, "picture", "square", 128, 128);
        }
    }

    private void ProcessFacebooksProfileData(IDictionary<string, object> data)
    {
        Debug.Log("Call process facebook data");
        if (data != null)
        {
            try
            {
                Session.LoggedInUser.fb_id = data["id"].ToString();
                Session.LoggedInUser.email = data["email"].ToString();
                Session.LoggedInUser.name = data["last_name"] + " " + data["first_name"];

                // Call register facebook account and login
                RegisterAPI.RegisterFBAccount(AccessToken.CurrentAccessToken.TokenString, RegisterFBAccountCallback);
            }
            catch (Exception e)
            {
                Debug.Log("LoginPage::ProcessFacebooksProfileData error:" + e.Message);
                RegisterFBAccountCallback(false);
            }
        }
        else
        {
            RegisterFBAccountCallback(false);
        }
    }

    private void RegisterFBAccountCallback(bool isSuccess)
    {
        LoadingView.Hide();

        if (!isSuccess)
        {
            DialogView.Show("Facebookログインに失敗しました");
            if (FB.IsLoggedIn) FB.LogOut();
        }
        else
        {
            Debug.Log("Registered new facebook account for email " + Session.LoggedInUser.email);
            PageManager.Show(PageType.CourseList);
        }
    }

    private void OnFacebookLoginFail()
    {
        DialogView.Show("Facebookログインに失敗しました");
    }


    void OnProcessRegister(bool isSuccess,int errorType)
    {
        LoadingView.Hide();
        if (isSuccess)
        {
            // update player pref
            PlayerPrefs.SetString(ApplicationSystem.CONST.LOGGED_IN_EMAIL_KEY, emailField.text);
            PlayerPrefs.Save();

            PageManager.Show(PageType.Login);
            DialogView.Show("登録しました。");
        }
        else
        {
            Debug.Log("Register user error");
            
            switch (errorType)
            {
                case 2:
                    DialogView.Show("そのメールアドレスは既に登録されています");
                    break;
                default:
                    DialogView.Show("登録に失敗しました。");
                    break;
            }
        }
    }

    public void OpenAgreementPage()
    {
        Webview.Show("https://www.google.com/");
    }

    public void SetSexType(int sex)
    {
        sexType = sex;
    }
}
