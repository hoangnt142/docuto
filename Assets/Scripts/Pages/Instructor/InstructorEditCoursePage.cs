﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImageAndVideoPicker;

public class InstructorEditCoursePage : Page
{
    [SerializeField]
    InputField nameText;

    [SerializeField]
    Dropdown categoryDropdown;

    [SerializeField]
    Dropdown levelDropdown;

    [SerializeField]
    Dropdown updateTypeDropdown;

    [SerializeField]
    InputField explanationText;

    [SerializeField]
    InputField necessityText;

    [SerializeField]
    Dropdown feeDropdown;

    [SerializeField]
    List<int> feeOptions;

    [SerializeField]
    Button publishButton, unpublishButton, deleteButton;

    [SerializeField]
    Image courseImage;

    [SerializeField]
    VideoPlayer videoPlayer;

    bool resetupForm;

    Sprite defaultCourseImageSprite;
    Color defaultCourseImageColor;
    Sprite defaultCourseSprite;
    InstructorCourseDetailPage detailPage;
    CourseData currentCourseData;

    private void Awake()
    {
        defaultCourseSprite = courseImage.sprite;
        videoPlayer.gameObject.SetActive(false);
    }

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("レッスン情報を入力", HeaderSubView.ButtonType.close, MenuType);

        if (defaultCourseImageSprite == null) defaultCourseImageSprite = courseImage.sprite;
        if (defaultCourseImageColor.Equals(default(Color))) defaultCourseImageColor = courseImage.color;

        MasterCourseCategory.GetData((List<CourseCategoryData> categories) =>
        {
            categoryDropdown.ClearOptions();
            var options = new List<Dropdown.OptionData>();
            foreach (var c in categories)
            {
                var option = new Dropdown.OptionData();
                option.text = c.name;
                options.Add(option);
            }
            categoryDropdown.AddOptions(options);
        });

        Debug.Log(feeDropdown.options.Count);
        if (feeDropdown.options.Count == 0)
        {
            var options = new List<Dropdown.OptionData>();
            foreach (var fee in feeOptions)
            {
                var option = new Dropdown.OptionData();
                option.text = fee + " 円";
                options.Add(option);
            }
            feeDropdown.AddOptions(options);
        }

        PickerEventListener.onVideoSelect += OnVideoSelect;

        if (resetupForm)
        {
            videoPlayer.gameObject.SetActive(false);
            SetUp();
        }
        resetupForm = true;
    }

    public void SetUp(CourseData data = default(CourseData))
    {
        currentCourseData = data;
        // create new course
        if (currentCourseData.Equals(default(CourseData)))
        {
            deleteButton.interactable = false;
            deleteButton.GetComponentInChildren<Text>().text = "削除";
            nameText.text = "";
            explanationText.text = "";
            necessityText.text = "";
            levelDropdown.value = 0;
            categoryDropdown.value = 0;
            feeDropdown.value = 0;
            if (updateTypeDropdown.options.Count > 1) updateTypeDropdown.value = 1;
            else updateTypeDropdown.value = 0;

            ResetCourseImage();
            ResetCourseVideo();

            publishButton.GetComponentInChildren<Text>().text = "保存して公開";
            unpublishButton.GetComponentInChildren<Text>().text = "非公開で保存する";
        }
        // update a course
        else
        {
            nameText.text = currentCourseData.title;
            explanationText.text = currentCourseData.explanation;
            necessityText.text = currentCourseData.necessity;

            levelDropdown.value = currentCourseData.level - 1;
            categoryDropdown.value = MasterCourseCategory.GetCategoryIndex(currentCourseData.course_category.id);
            feeDropdown.value = GetFeeIndex(currentCourseData.GetFee());
            updateTypeDropdown.value = currentCourseData.update_type;

            deleteButton.interactable = true;
            if (currentCourseData.num_of_students <= 0)
            {
                deleteButton.GetComponentInChildren<Text>().text = "削除";
            }
            else
            {
                deleteButton.GetComponentInChildren<Text>().text = "削除依頼";
            }

            CourseImageLoader.GetImage(data.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
            {
                if (sprite != null)
                {
                    SetCourseImage(sprite);
                }
            });

            videoPlayer.gameObject.SetActive(true);
            ResetCourseVideo();
            VideoThumbLoader.GetImage(currentCourseData.video_id.ToString(), (Sprite sprite) =>
            {
                if (sprite != null)
                {
                    videoPlayer.ShowPlayerTool(false);
                    videoPlayer.SetVideoThumb(sprite);
                }
            });

            publishButton.GetComponentInChildren<Text>().text = "公開で保存";
            unpublishButton.GetComponentInChildren<Text>().text = "非公開で保存";
            //videoPlayer.SetVideoUrl(Server.MakeUrl("videos/" + currentCourseData.video_id, true));
        }

        InputFieldTool infTool = explanationText.gameObject.GetComponent<InputFieldTool>();
        infTool.ResizeInput(explanationText.text);
    }

    public override void Hide()
    {
        base.Hide();

        PickerEventListener.onVideoSelect -= OnVideoSelect;

        if (resetupForm)
        {
            videoPlayer.Dispose();
            videoPlayer.gameObject.SetActive(false);
        }
    }

    public void SetDetailPage(InstructorCourseDetailPage page)
    {
        detailPage = page;
    }

    public void PublishCourse(bool ispublic)
    {
        CourseData courseData = GetCourseData();
        courseData.is_public = ispublic;

        // validation
        if (!ValidateInput(courseData))
        {
            return;
        }

        LoadingView.Show();
        CourseAPI.CreateCourse(courseData, OnSuccessUpdateCourse, OnErrorUpdateCourse);
    }

    public void OnDeleteCourseButtonClick()
    {
        DialogView.Show("削除してもよろしいですか?", OnOkDeleteCourse, () => { });
    }

    void OnOkDeleteCourse()
    {
        LoadingView.Show();
        CourseAPI.DeleteCourse(currentCourseData.id, OnDeleteCourseSuccess, OnDeleteCourseError);
    }

    void OnDeleteCourseSuccess(DeleteCourseResponseData deleteData)
    {
        if (deleteData.does_send_request)
        {
            LoadingView.Hide();
            DialogView.Show("購読中の生徒がいるため削除依頼を出しました。削除されるまでしばらくお待ち下さい");
        }
        else
        {
            LoadingView.Hide();
            PageManager.RemoveLastPage();
            InstructorMyCoursePage page = (InstructorMyCoursePage)PageManager.Show(PageType.InstructorMyCourse);
            page.Refresh();
            DialogView.Show("削除しました。");
        }
    }

    void OnDeleteCourseError()
    {
        LoadingView.Hide();
        DialogView.Show("削除に失敗しました。");
    }

    string failSaveCourseMessage = "保存に失敗しました。";
    void OnSuccessUpdateCourse(CourseData course)
    {
        LoadingView.Hide();

        // if it is edit course and video is not change
        if (!currentCourseData.Equals(default(CourseData)) && String.IsNullOrEmpty(videoPlayer.GetVideoURL()))
        {
            ShowSaveCourseStatus(course);
        }
        else
        {
            Uploader uploader = gameObject.GetComponent<Uploader>();
            uploader.OnSuccess = () =>
            {
                ShowSaveCourseStatus(course);
            };
            uploader.OnFail = () =>
            {
                if (currentCourseData.Equals(default(CourseData)))
                {
                    LoadingView.Show();
                    print("Delete course " + course.id);
                    CourseAPI.DeleteCourse(course.id, (isSuccess) =>
                    {
                        if (!isSuccess)
                        {
                            // if delete fail, we will try again (5 times)
                            StartCoroutine(TryDeleteCourse(course.id));
                        }
                        else
                        {
                            LoadingView.Hide();
                            DialogView.Show(failSaveCourseMessage);
                        }
                    });
                }
                // if it is edit couse, we will not delete course
                else
                {
                    DialogView.Show(failSaveCourseMessage);
                }
            };
            uploader.SetData(UploaderDataKeys.COURSE_ID, course.id);
            uploader.UploadVideo(videoPlayer.GetVideoURL());
        }
    }

    CourseData MergeData(CourseData oldData, CourseData newData)
    {
        //foreach (System.Reflection.FieldInfo field in newData.GetType().GetFields())
        //{
        //    Debug.Log("---->n " + field.Name + ": " + field.GetValue(newData));
        //    Debug.Log("---->o " + field.Name + ": " + field.GetValue(oldData)); //oldData.GetType().GetField(field.Name).GetValue(oldData));
        //    if (String.IsNullOrEmpty(field.GetValue(newData).ToString()) || (field.Name.Equals("video_id") && field.GetValue(newData).ToString().Equals("0")))
        //        field.SetValue(newData, field.GetValue(oldData));
        //}
        if (newData.video_id == 0) newData.video_id = oldData.video_id;
        if (newData.course_fee == null)
        {
            newData.course_fee = oldData.course_fee;
            if (newData.course_fee == null) newData.course_fee = new CourseFeeData();
            newData.fee_yen = newData.course_fee.fee_yen = feeOptions[feeDropdown.value];
        }

        return newData;
    }

    void OnErrorUpdateCourse()
    {
        LoadingView.Hide();
        DialogView.Show(failSaveCourseMessage);
    }

    void ShowSaveCourseStatus(CourseData course)
    {
        if (course.is_public)
        {
            DialogView.Show("公開で保存しました。");
        }
        else
        {
            DialogView.Show("非公開で保存しました。");
        }

        course = MergeData(currentCourseData, course);

        CourseImageLoader.RemoveImage(course.id.ToString());
        VideoThumbLoader.RemoveImage(course.video_id.ToString());

        // refresh detail page and MyCoursePage
        if (detailPage != null) detailPage.SetData(course);
        SetUp(course);
        PageManager.RefreshPage(PageType.InstructorMyCourse);
    }

    IEnumerator TryDeleteCourse(int id)
    {
        int numberOfTry = 1;
        bool endOfAPIProcess;
        while (numberOfTry < 5)
        {
            print("Try to delete course " + id + " ... " + numberOfTry);
            endOfAPIProcess = false;
            CourseAPI.DeleteCourse(id, (_isSuccess) =>
            {
                if (_isSuccess) numberOfTry = 5;
                else numberOfTry++;

                endOfAPIProcess = true;
            });

            yield return new WaitUntil(() => { return endOfAPIProcess; });
        }

        LoadingView.Hide();
        DialogView.Show(failSaveCourseMessage);
    }

    int GetFeeIndex(int feeValue)
    {
        for (int i = 0; i < feeOptions.Count; i++)
        {
            if (feeValue == feeOptions[i])
            {
                return i;
            }
        }
        return -1;
    }

    CourseData GetCourseData()
    {
        CourseData courseData;
        if (!currentCourseData.Equals(default(CourseData)))
        {
            courseData = currentCourseData;
        }
        else
        {
            courseData = new CourseData();
        }

        courseData.title = nameText.text;

        // category
        var selectedCategory = MasterCourseCategory.data[categoryDropdown.value];
        courseData.course_category_id = selectedCategory.id;

        courseData.level = levelDropdown.value + 1;
        courseData.update_type = updateTypeDropdown.value;
        courseData.fee_yen = feeOptions[feeDropdown.value];

        courseData.explanation = explanationText.text;
        courseData.necessity = necessityText.text;
        courseData.image = Convert.ToBase64String(courseImage.sprite.texture.EncodeToPNG());

        return courseData;
    }

    bool ValidateInput(CourseData data)
    {
        // validation
        if (string.IsNullOrEmpty(nameText.text))
        {
            DialogView.Show("レッスン名を入力してください。");
            return false;
        }

        if (courseImage.sprite == defaultCourseSprite)
        {
            DialogView.Show("イメージ画像を登録してください。");
            return false;
        }

        if (data.is_public)
        {
            if (string.IsNullOrEmpty(explanationText.text))
            {
                DialogView.Show("レッスンの説明を入力してください。");
                return false;
            }

            if (string.IsNullOrEmpty(necessityText.text))
            {
                DialogView.Show("必要なものを入力してください。");
                return false;
            }
        }
        return true;
    }

    public void OnSelectBeneFitnessImages()
    {
        resetupForm = false;

        SelectPicturePage page = (SelectPicturePage)PageManager.Show(PageType.SelectPicture);
        page.SetController(this);
    }

    public void SetCourseImage(Sprite sprite)
    {
        courseImage.sprite = sprite;
        courseImage.color = new Color(255, 255, 255, 255);
        courseImage.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    public void SelectImage()
    {
#if UNITY_EDITOR
        var tex = EditorIO.GetPngTextureFromStrage();
        CropImageModal.ShowCropImage(tex, new Vector2(570, 380));
        CropImageModal.onCrop = OnCropImage;
#elif UNITY_IOS
        IOSCamera.Instance.PickImage(ISN_ImageSource.Library);
        IOSCamera.OnImagePicked += OnPickedImage;
#elif UNITY_ANDROID
        AndroidCamera.Instance.OnImagePicked += OnPickedImage;
        AndroidCamera.Instance.GetImageFromGallery();
#endif

    }

    void OnCropImage(Sprite croppedSprite)
    {
        courseImage.sprite = croppedSprite;
        courseImage.color = new Color(255, 255, 255, 255);
        courseImage.gameObject.transform.GetChild(0).gameObject.SetActive(false);
    }

    void ResetCourseImage()
    {
        courseImage.sprite = defaultCourseImageSprite;
        courseImage.color = defaultCourseImageColor;
        courseImage.gameObject.transform.GetChild(0).gameObject.SetActive(true);
    }

    void ResetCourseVideo()
    {
        videoPlayer.Dispose();
        videoPlayer.ShowPlayerTool(false);
    }


#if UNITY_IOS
    void OnPickedImage (IOSImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, new Vector2(570, 380));
            CropImageModal.onCrop = OnCropImage;
        }
        IOSCamera.OnImagePicked -= OnPickedImage;
    }
#endif

#if UNITY_ANDROID

    void OnPickedImage(AndroidImagePickResult result)
    {
        if (result.IsSucceeded)
        {
            CropImageModal.ShowCropImage(result.Image, new Vector2(570, 380));
            CropImageModal.onCrop = OnCropImage;
        }

        AndroidCamera.Instance.OnImagePicked -= OnPickedImage;
    }
#endif


    public void PickupVideo()
    {
#if UNITY_EDITOR
        string path = UnityEditor.EditorUtility.OpenFilePanel("Select video", "", "mp4");
        OnVideoSelect(path);

#elif UNITY_ANDROID
        AndroidPicker.BrowseVideo();

#elif UNITY_IPHONE
        IOSPicker.BrowseVideo();

#endif
    }

    void OnVideoSelect(string videoPath)
    {
        if (string.IsNullOrEmpty(videoPath)) return;
        if (videoPlayer != null)
        {
            ResetCourseVideo();
        }

        videoPlayer.gameObject.SetActive(true);
        videoPlayer.SetVideoPath(videoPath);
        videoPlayer.ShowPlayerTool(false);
    }

}
