﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorCourseDetailPage : Page
{
    [SerializeField]
    Image courseImage;

    [SerializeField]
    Text studentNumText, moneyText, updatedText, categoryText,levelText, explainationText, necessityText;

    [SerializeField]
    VideoPlayer videoPlayer;

    [SerializeField]
    private GameObject waitingCourseImage;

    string[] updateTypes = {"不定期", "毎週更新", "隔週更新"};

    CourseData currentCourse;
    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("振込口座", HeaderSubView.ButtonType.arrow, MenuType);

        if (!currentCourse.Equals(default(CourseData)))
            Setup(currentCourse);
    }

    public void Setup(CourseData data)
    {
        currentCourse = data;
        HeaderView.ShowSub(data.title, HeaderSubView.ButtonType.arrow, MenuType);

        studentNumText.text = data.num_of_students.ToString();
        moneyText.text = "月々" + data.GetFee().ToString("N0") + "円";
        updatedText.text = updateTypes[data.update_type];
        categoryText.text = data.course_category.name;

        switch (data.level)
        {
            case 1:
                levelText.text = "初心者向け";
                break;
            case 2:
                levelText.text = "中級者向け";
                break;
            case 3:
                levelText.text = "上級者向け";
                break;

            default:
                levelText.text = "初心者向け";
                break;
        }


        explainationText.text = data.explanation;
        necessityText.text = data.necessity;

        waitingCourseImage.SetActive(true);
        CourseImageLoader.GetImage(data.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
        {
            if (sprite != null)
            {
                courseImage.sprite = sprite;
                waitingCourseImage.SetActive(false);
            }    
        });

        VideoThumbLoader.GetImage(data.video_id.ToString(), (Sprite sprite) =>
        {
            if (sprite != null)
            {
                videoPlayer.SetVideoThumb(sprite);
            }
        });

        videoPlayer.SetHtmlUrl(Server.MakeUrl("videos/" + data.video_id + "/html", true));
    }

    public void SetData(CourseData data)
    {
        currentCourse = data;
    }

    public override void Hide()
    {
        if (videoPlayer != null)
            videoPlayer.Dispose();

        base.Hide();
    }

    public void ShowUploadVideoPage()
    {
        PageManager.Show(PageType.UploadVideoPage);
    }

    public void EditCourse()
    {
        InstructorEditCoursePage page = (InstructorEditCoursePage)PageManager.Show(PageType.EditCoursePage);
        page.SetUp(currentCourse);
        page.SetDetailPage(this);
    }
}
