﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorVideoPage : Page
{
    [SerializeField]
    GameObject pubicDetailPrefab, unpublicDetailPrefab, emptyMessagePrefab;

    [SerializeField]
    Transform publicTransform, unpublicTransform;


    string emptyPrivateLessonText = "非公開中の動画はありません。";
    string emptyPublicLessonText = "公開中の動画はありません。";

    public override void Show()
    {
        base.Show();
        HeaderView.ShowMenu("動画", MenuType);

        // get public videos
        InstructorAPI.GetLessons(Session.LoggedInInstructor.id, true, ProcessPublicLessons, OnFailGetPublicLessons);
        //InstructorAPI.GetLessons(Session.LoggedInInstructor.id, false, ProcessPrivateLessons, OnFailGetPrivateLessons);

    }

    void ProcessPublicLessons(List<LessonData> data)
    {
        foreach (Transform child in publicTransform)
        {
            Destroy(child.gameObject);
        }

        InstructorAPI.GetLessons(Session.LoggedInInstructor.id, false, ProcessPrivateLessons, OnFailGetPrivateLessons);

        if (data.Count > 0)
        {
            foreach (LessonData lesson in data)
            {
                var prefapPublicLesson = Instantiate(pubicDetailPrefab, publicTransform);

                prefapPublicLesson.transform.localScale = new Vector3(1, 1, 1);
                PublishLessonDetail lessonDetail = prefapPublicLesson.GetComponent<PublishLessonDetail>();
                lessonDetail.UpdateView(lesson);
            }
        }
        else
        {
            ShowEmptyMessage(emptyPublicLessonText, publicTransform);
        }
    }

    void OnFailGetPublicLessons()
    {
        foreach (Transform child in publicTransform)
        {
            Destroy(child.gameObject);
        }

        ShowEmptyMessage(emptyPublicLessonText, publicTransform);
        InstructorAPI.GetLessons(Session.LoggedInInstructor.id, false, ProcessPrivateLessons, OnFailGetPrivateLessons);
    }

    void ProcessPrivateLessons(List<LessonData> data)
    {
        foreach (Transform child in unpublicTransform)
        {
            Destroy(child.gameObject);
        }

        if (data.Count > 0)
        {
            foreach (LessonData lesson in data)
            {
                var prefapPrivateLesson = Instantiate(unpublicDetailPrefab, unpublicTransform);

                prefapPrivateLesson.transform.localScale = new Vector3(1, 1, 1);
                PrivateLessonDetail lessonDetail = prefapPrivateLesson.GetComponent<PrivateLessonDetail>();
                lessonDetail.UpdateView(lesson);
            }
        }
        else
        {
            ShowEmptyMessage(emptyPrivateLessonText, unpublicTransform);
        }
    }

    void OnFailGetPrivateLessons()
    {
        foreach (Transform child in unpublicTransform)
        {
            Destroy(child.gameObject);
        }

        ShowEmptyMessage(emptyPrivateLessonText, unpublicTransform);
    }

    void ShowEmptyMessage(string text, Transform parent)
    {
        var emptyMessageObject = Instantiate(emptyMessagePrefab, parent);
        Text messageText = emptyMessageObject.GetComponent<Text>();
        messageText.text = text;
    }

    public void ShowUploadVideoPage()
    {
        PageManager.Show(PageType.UploadVideoPage);
    }
}
