﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using ImageAndVideoPicker;

public class UploadVideoPage : Page
{
    [SerializeField]
    InputField title;

    [SerializeField]
    Dropdown listCourses;

    [SerializeField]
    InputField notice, neccessity;

    [SerializeField]
    VideoPlayer videoPlayer;

    [SerializeField]
    Button deleteButton;
    [SerializeField]
    Color publishedColor;

    List<CourseData> poolData = new List<CourseData>();
    LessonData currentData;

    CourseData SelectedCourse
    {
        get
        {
            if (poolData.Count > 0)
                return poolData[listCourses.value];

            return default(CourseData);
        }
    }

    Sprite publishedSprite;

    private void Awake()
    {
        var texture = new Texture2D(1, 1); 
        texture.SetPixel(0, 0, publishedColor);
        texture.Apply();
        publishedSprite = Sprite.Create(texture, new Rect(0, 0, texture.width, texture.height), new Vector2(0, 0));
    }

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("動画情報を入力", HeaderSubView.ButtonType.arrow, MenuType);

        Reset();
        PickerEventListener.onVideoSelect += OnVideoSelect;
    }

    public override void Hide()
    {
        base.Hide();

        PickerEventListener.onVideoSelect -= OnVideoSelect;
        videoPlayer.Dispose();
        videoPlayer.gameObject.SetActive(false);
    }

    public void Reset()
    {
        title.text = "";
        listCourses.value = 0;
        notice.text = "";
        neccessity.text = "";

        deleteButton.interactable = false;

        currentData = default(LessonData);
        GetListCourses();
    }

    void GetListCourses()
    {
        InstructorAPI.GetAllCourses(Session.LoggedInInstructor.id, ProcessListCourses, OnGetCourseFail);
    }

    void ProcessListCourses(List<CourseData> data)
    {
        poolData = data;

        poolData.Sort(delegate (CourseData a, CourseData b) {
            DateTime dt1 = DateTime.Parse(a.updated_at);
            DateTime dt2 = DateTime.Parse(b.updated_at);
            return dt2.CompareTo(dt1);
        });

        // fill to dropdown
        listCourses.ClearOptions();
        foreach (CourseData _c in data)
        {
            if (!String.IsNullOrEmpty(_c.title))
            {
                Dropdown.OptionData dropdownOption;
                if (_c.is_public)
                {
                    dropdownOption = new Dropdown.OptionData(_c.title,publishedSprite);
                }
                else
                {
                    dropdownOption = new Dropdown.OptionData(_c.title);
                }
                listCourses.options.Add(dropdownOption);

            }
        }

        // set current course
        if(!currentData.Equals(default(LessonData)))
        {
            listCourses.value = IndexOfCourse(currentData.course_id);
        }

        listCourses.RefreshShownValue();
    }
    int IndexOfCourse(int courseId)
    {
        for (int i = 0; i < poolData.Count; i++)
        {
            if(poolData[i].id == courseId)
            {
                return i;
            }
        }
        return 0;
    }
    void OnGetCourseFail()
    {
        poolData.Clear();
    }

    public void DeleteLesson()
    {
        if (currentData.Equals(default(LessonData))) return;

        DialogView.Show(
            "削除してもよろしいですか?",
            // on Ok
            () =>
            {
                LoadingView.Show();
                CourseAPI.DeleteLesson(currentData.id, ProcessDeleteLesson);
            },
            // on Cancel, do nothing
            () => { }
        );
    }

    void ProcessDeleteLesson(bool isSuccess,bool doesSendRequest)
    {
        LoadingView.Hide();
        if (!isSuccess)
        {
            DialogView.Show("削除に失敗しました。");
        }
        else
        {
            if (doesSendRequest)
            {
                DialogView.Show("削除リクエストを送信しました");
            }
            else
            {
                PageManager.RemoveLastPage();
                PageManager.Show(PageType.InstructorVideo);
                DialogView.Show("削除しました。");
            }
        }
    }

    public void SaveLesson(bool isPublic)
    {
        LessonData lessonData = new LessonData();
        if (!currentData.Equals(default(LessonData))) lessonData = currentData;

        // validate video
        if (!ValidateVideo()) return;

        lessonData.title = title.text;
        lessonData.necessity = neccessity.text;
        lessonData.explanation = notice.text;

        // validate lesson fields
        if (!ValidateLesson(lessonData)) return;

        LoadingView.Show();
        CourseAPI.CreateLesson(SelectedCourse.id, lessonData, isPublic, OnCreateLessonSuccess, ShowSaveLessonFail);
    }

    void OnCreateLessonSuccess(LessonData data)
    {
        if (!currentData.Equals(default(CourseData)) && String.IsNullOrEmpty(videoPlayer.GetVideoURL()))
        {
            ShowSaveLessonSuccess(data);
        }
        else
        {
            Uploader uploader = GetComponent<Uploader>();
            uploader.OnSuccess = () =>
            {
                ShowSaveLessonSuccess(data);
            };
            uploader.OnFail = () =>
            {
                if (currentData.Equals(default(LessonData)))
                {
                    print("Delete lesson " + data.id);
                    CourseAPI.DeleteLesson(data.id, (isSuccess, doesSendRequest) => {
                        if (!isSuccess)
                        {
                            // if delete fail, we will try again (5 times)
                            StartCoroutine(TryDeleteLesson(data.id));
                        }
                        else
                        {
                            ShowSaveLessonFail();
                        }
                    });
                }
                else
                {
                    ShowSaveLessonFail();
                }
            };
            uploader.SetData(UploaderDataKeys.COURSE_ID, SelectedCourse.id);
            uploader.SetData(UploaderDataKeys.LESSON_ID, data.id);
            uploader.UploadVideo(videoPlayer.GetVideoURL());
        }
    }

    IEnumerator TryDeleteLesson(int id)
    {
        int numberOfTry = 1;
        bool endOfAPIProcess;
        while (numberOfTry < 5)
        {
            print("Try to delete lesson " + id + " ... " + numberOfTry);
            endOfAPIProcess = false;
            CourseAPI.DeleteLesson(id, (_isSuccess, doesSendRequest) =>
            {
                if (_isSuccess) numberOfTry = 5;
                else numberOfTry++;

                endOfAPIProcess = true;
            });

            yield return new WaitUntil(() => { return endOfAPIProcess; });
        }

        ShowSaveLessonFail();
    }

    void ShowSaveLessonSuccess(LessonData lesson)
    {

        videoPlayer.gameObject.SetActive(true);
        deleteButton.interactable = true;

        LoadingView.Hide();
        DialogView.Show("レッスンを保存しました");
    }

    void ShowSaveLessonFail()
    {
        LoadingView.Hide();
        DialogView.Show("レッスンの保存に失敗しました");
    }

    bool ValidateVideo()
    {
        if (currentData.Equals(default(LessonData)) && String.IsNullOrEmpty(videoPlayer.GetVideoURL()))
        {
            DialogView.Show("Please select a video to upload");
            return false;
        }

        return true;
    }

    bool ValidateLesson(LessonData data)
    {
        if(string.IsNullOrEmpty(data.title))
        {
            DialogView.Show("動画タイトルは必須です");
            return false;
        }   

        return true;
    }


    public void PickupVideo()
    {
#if UNITY_EDITOR
        string path = UnityEditor.EditorUtility.OpenFilePanel("Select video", "", "mp4");
        OnVideoSelect(path);

#elif UNITY_ANDROID
        AndroidPicker.BrowseVideo();

#elif UNITY_IPHONE
        IOSPicker.BrowseVideo();

#endif
    }

    void OnVideoSelect(string videoPath)
    {
        if (string.IsNullOrEmpty(videoPath)) return;
        if (videoPlayer != null) videoPlayer.Dispose();

        videoPlayer.gameObject.SetActive(true);
        videoPlayer.SetVideoPath(videoPath);
        videoPlayer.ShowPlayerTool(false);
    }

    public void Setup(LessonData data)
    {
        currentData = data;
        title.text = data.title;
        notice.text = data.explanation;
        neccessity.text = data.necessity;

        videoPlayer.gameObject.SetActive(true);
        VideoThumbLoader.GetImage(data.video_id.ToString(), (Sprite sprite) =>
        {
            videoPlayer.ShowPlayerTool(false);
            videoPlayer.SetVideoThumb(sprite);
        });

        deleteButton.interactable = true;
    }
}
