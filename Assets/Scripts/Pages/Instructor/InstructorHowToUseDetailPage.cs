﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class InstructorHowToUseDetailPage : Page {
    public enum HowTo
    {
        RegisterBank,
        OpenCourses,
        AddVideo,
        CloseCourse
    }
    [SerializeField]
    ScrollRect scroll;

    [SerializeField]
    GameObject[] HowToPages;

    private HowTo currentHowToPage;

    public override void Show()
    {
        base.Show();
        string title = "";
        switch (currentHowToPage)
        {
            case HowTo.RegisterBank:
                title = "振込口座登録";
                break;
            case HowTo.OpenCourses:
                title = "レッスンの開講方法";
                break;
            case HowTo.AddVideo:
                title = "動画の追加方法";
                break;
            case HowTo.CloseCourse:
                title = "レッスンを閉めるには";
                break;
            default:
                title = "振込口座登録";
                break;
        }

        foreach (var item in HowToPages)
        {
            item.SetActive(false);
        }

        HowToPages[(int)currentHowToPage].SetActive(true);
        scroll.verticalNormalizedPosition = 1;
        HeaderView.ShowSub(title, HeaderSubView.ButtonType.close, MenuType);
    }

    public void SetHowToPage(HowTo howTo)
    {
        currentHowToPage = howTo;
    }
}
