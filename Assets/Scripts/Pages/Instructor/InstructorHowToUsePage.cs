﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorHowToUsePage : Page {

    [SerializeField]
    Button[] buttons;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("先生の使い方", HeaderSubView.ButtonType.arrow, MenuType);

        // this trick to make button reset button state
        foreach (var item in buttons)
        {
            item.enabled = false;
            item.enabled = true;
            item.image.color = item.colors.normalColor;
        }
    }

    public void ShowHowToRegisterBankPage()
    {
        InstructorHowToUseDetailPage page = (InstructorHowToUseDetailPage)PageManager.Instantiate(PageType.InstructorHowToUseDetail);
        page.SetHowToPage(InstructorHowToUseDetailPage.HowTo.RegisterBank);
        PageManager.Show(PageType.InstructorHowToUseDetail);
    }

    public void ShowHowToAddVideoPage()
    {
        InstructorHowToUseDetailPage page = (InstructorHowToUseDetailPage)PageManager.Instantiate(PageType.InstructorHowToUseDetail);
        page.SetHowToPage(InstructorHowToUseDetailPage.HowTo.AddVideo);
        PageManager.Show(PageType.InstructorHowToUseDetail);
    }

    public void ShowHowToOpenCoursesPage()
    {
        InstructorHowToUseDetailPage page = (InstructorHowToUseDetailPage)PageManager.Instantiate(PageType.InstructorHowToUseDetail);
        page.SetHowToPage(InstructorHowToUseDetailPage.HowTo.OpenCourses);
        PageManager.Show(PageType.InstructorHowToUseDetail);
    }

    public void ShowHowToCloseCoursesPage()
    {
        InstructorHowToUseDetailPage page = (InstructorHowToUseDetailPage)PageManager.Instantiate(PageType.InstructorHowToUseDetail);
        page.SetHowToPage(InstructorHowToUseDetailPage.HowTo.CloseCourse);
        PageManager.Show(PageType.InstructorHowToUseDetail);
    }
}
