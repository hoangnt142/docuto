﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyCourseNode : MonoBehaviour
{
    [SerializeField]
    Image image;

    [SerializeField]
    Text title;

    [SerializeField]
    Text numberOfSubscripber;

    [SerializeField]
    Text numberOfFee;

    [SerializeField]
    Text updateTypeText;

    [SerializeField]
    Image courseImage;

    CourseData currentCourse;

    public void UpdateView(CourseData courseData)
    {
        title.text = courseData.title;
        numberOfFee.text = courseData.course_fee.fee_yen.ToString();
        numberOfSubscripber.text = courseData.num_of_students.ToString();

        switch(courseData.update_type)
        {
            case 0:
                updateTypeText.text = "不定期";
                break;
            case 1:
                updateTypeText.text = "毎週更新";
                break;
            case 2:
                updateTypeText.text = "隔週更新";
                break;

            default:
                updateTypeText.text = "不定期 ";
                break;
        }

        CourseImageLoader.GetImage(courseData.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
        {
            if(sprite != null)
            {
                courseImage.sprite = sprite;
            }
        });

        currentCourse = courseData;
    }

    public void OnCourseClick()
    {
        InstructorCourseDetailPage page = (InstructorCourseDetailPage) PageManager.Show(PageType.InstructorCourseDetail);

        page.Setup(currentCourse);
    }
}
