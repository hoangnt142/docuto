﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PrivateLessonDetail : MonoBehaviour
{
    [SerializeField]
    Image thumbnail;

    [SerializeField]
    GameObject loadingImage;

    [SerializeField]
    Text lessonTitle, courseTitle, time;

    LessonData currentData;

    public void UpdateView(LessonData data)
    {
        currentData = data;
        lessonTitle.text = data.title;
        courseTitle.text = data.course_title;

        VideoThumbLoader.GetImage(data.video_id.ToString(), (Sprite sprite) =>
        {
            loadingImage.SetActive(false);
            thumbnail.gameObject.SetActive(true);

            if (sprite != null)
                thumbnail.sprite = sprite;
        });

        // time
        string timeString = "未公開";
        if (!String.IsNullOrEmpty(data.published_at))
        {
            DateTime publishedTime;
            if (!DateTime.TryParse(data.published_at, out publishedTime))
            {
                timeString = data.published_at + " ~";
            }
            else
                timeString = publishedTime.ToString("yyyy/MM/dd ~");

            if (!String.IsNullOrEmpty(data.closed_at))
            {
                DateTime closedTime;
                if (!DateTime.TryParse(data.closed_at, out closedTime))
                {
                    timeString += " " + data.closed_at;
                }
                timeString += closedTime.ToString(" yyyy/MM/dd");
            }
        }

        time.text = timeString;
    }

    public void ShowVideoDetailPage()
    {
        InstructorVideoDetailPage page = (InstructorVideoDetailPage)PageManager.Show(PageType.InstructorVideoDetailPage);
        page.Setup(currentData);
    }
}
