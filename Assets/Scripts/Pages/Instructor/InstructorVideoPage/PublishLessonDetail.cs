﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PublishLessonDetail : MonoBehaviour
{
    [SerializeField]
    Image thumbnail;

    [SerializeField]
    GameObject loadingImage;

    [SerializeField]
    Text lessonTitle, courseTitle, time;

    LessonData currentData;

    public void UpdateView(LessonData data)
    {
        currentData = data;
        lessonTitle.text = data.title;
        courseTitle.text = data.course_title;

        VideoThumbLoader.GetImage(data.video_id.ToString(), (Sprite sprite) =>
        {
            loadingImage.SetActive(false);
            thumbnail.gameObject.SetActive(true);

            if (sprite != null)
                thumbnail.sprite = sprite;
        });

        //time.text = data.updated_at;
        DateTime updatedTime;
        if (!DateTime.TryParse(data.updated_at, out updatedTime))
        {
            updatedTime = DateTime.Now;
        }
        time.text = updatedTime.ToString("yyyy/MM/dd HH:mm更新");
    }

    public void ShowVideoDetailPage()
    {
        InstructorVideoDetailPage page = (InstructorVideoDetailPage)PageManager.Show(PageType.InstructorVideoDetailPage);
        page.Setup(currentData);
    }
}
