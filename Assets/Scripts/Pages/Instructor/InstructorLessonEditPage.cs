﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructorLessonEditPage : Page
{
    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("動画情報を入力", HeaderSubView.ButtonType.close, MenuType);
    }
}
