﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorProfileEditPage : Page
{
    [SerializeField]
    Image backgroundImg;

    [SerializeField]
    Image profileImage;

    [SerializeField]
    InputField nameText;

    [SerializeField]
    InputField categoryText;

    [SerializeField]
    InputField profileText;

    [SerializeField]
    Text buttonText;

    Sprite defaultProfileImageSprite;
    Sprite defaultBackgroundImageSprite;

    private Sprite startProfileImageSprite;
    private Sprite startBackgroundImageSprite;
    public enum PageType
    {
        commonUser,
        instructor
    }

    void Awake()
    {
        defaultProfileImageSprite = profileImage.sprite;
        defaultBackgroundImageSprite = backgroundImg.sprite;
    }

    void Finalize()
    {
        profileImage.sprite = defaultProfileImageSprite;
        backgroundImg.sprite = defaultBackgroundImageSprite;
    }

    public override void Show(MenuType menuType = MenuType.None)
    {
        base.Show(menuType);
    }

    public void Setup(PageType pageType = PageType.commonUser)
    {
        profileImage.sprite = defaultProfileImageSprite;
        backgroundImg.sprite = defaultBackgroundImageSprite;

        if (Session.IsInstructor ()) {
            InstructorProfileImageLoader.GetImage (Session.LoggedInInstructor.id.ToString (), ImageSizes.detail, (Sprite sprite) => {
                if (sprite != null) {
                    profileImage.sprite = sprite;
                }
            });

            InstructorBackgroundImageLoader.GetImage(Session.LoggedInInstructor.id.ToString(), (Sprite sprite) => {
                if (sprite != null) {
                    backgroundImg.sprite = sprite;
                }
            });

            nameText.text = Session.LoggedInInstructor.name;
            categoryText.text = Session.LoggedInInstructor.category_text;
            profileText.text = Session.LoggedInInstructor.explanation;
        }
        startProfileImageSprite = profileImage.sprite;
        startBackgroundImageSprite = backgroundImg.sprite;

        switch (pageType) {
            case PageType.commonUser:
                HeaderView.ShowSub("先生登録", HeaderSubView.ButtonType.arrow, MenuType.Instructor);
                buttonText.text = "先生登録";
                break;

            case PageType.instructor:
                HeaderView.ShowSub("先生プロフィール編集", HeaderSubView.ButtonType.arrow, MenuType.Instructor);
                buttonText.text = "更新する";
                break;
        }
    }

    public bool DataChanged()
    {
        if ( nameText.text != (Session.IsInstructor() == false ? string.Empty : (Session.LoggedInInstructor.name ?? string.Empty))
         || categoryText.text != (Session.IsInstructor() == false ? string.Empty : (Session.LoggedInInstructor.category_text ?? string.Empty))
        || profileText.text != (Session.IsInstructor() == false ? string.Empty : (Session.LoggedInInstructor.explanation ?? string.Empty))
        || startProfileImageSprite != profileImage.sprite
        || startBackgroundImageSprite != backgroundImg.sprite
        )
        {
            return true;
        }
        return false;
    }

    public override void Hide ()
    {
        base.Hide ();

        Finalize ();
    }

    public void UpdateProfile ()
    {
        string userName = nameText.text;
        string category_text = categoryText.text;
        string explanation = profileText.text;

        // validation
        if (string.IsNullOrEmpty (userName)) {
            DialogView.Show ("名前を入力して下さい");
            return;
        }

        byte [] profileImageBytes = null;
        if (profileImage.sprite != defaultProfileImageSprite) {
            profileImageBytes = profileImage.sprite.texture.EncodeToPNG ();
        }

        byte [] backgroundImageBytes = null;
        if (backgroundImg.sprite != defaultBackgroundImageSprite) {
            backgroundImageBytes = backgroundImg.sprite.texture.EncodeToPNG ();
        }

        LoadingView.Show ();
        InstructorAPI.UpdateProfile (profileImageBytes, backgroundImageBytes, userName, category_text, explanation, CallbackUpdateProfile);
    }


    void CallbackUpdateProfile (bool isSuccess, InstructorData data)
    {
        LoadingView.Hide ();
        if (isSuccess) {
            bool isCreateNew = false;
            if (!Session.IsInstructor ()) {
                Session.LoggedInInstructor = new InstructorData ();
                Session.LoggedInInstructor.id = data.id;
                Session.LoggedInInstructor.bank = default(BankData);
                isCreateNew = true;
            }

            Session.LoggedInInstructor.name = nameText.text;
            Session.LoggedInInstructor.category_text = categoryText.text;
            Session.LoggedInInstructor.explanation = profileText.text;

            PlayerPrefs.DeleteKey(ApplicationSystem.CONST.USER_DATA_KEY);
            PlayerPrefs.SetString(ApplicationSystem.CONST.USER_DATA_KEY, LitJson.JsonMapper.ToJson(Session.LoggedInUser));
            PlayerPrefs.Save();

            startProfileImageSprite = profileImage.sprite;
            startBackgroundImageSprite = backgroundImg.sprite;

            if (profileImage.sprite != defaultProfileImageSprite) {
                InstructorProfileImageLoader.SaveSprite (Session.LoggedInInstructor.id.ToString (), profileImage.sprite);
            }
            if (backgroundImg.sprite != defaultBackgroundImageSprite) {
                InstructorBackgroundImageLoader.SaveSprite (Session.LoggedInInstructor.id.ToString (), backgroundImg.sprite);
            }

            if (isCreateNew)
            {
                PageManager.Show(global::PageType.InstructorEditFinish);
            }
            else
            {
                DialogView.Show("更新しました");
            }
        } else {
            DialogView.Show ("プロフィールの更新に失敗しました。");
        }

        PageManager.RefreshAllPages();
    }

    public void SelectProfileImage ()
    {
#if UNITY_EDITOR
        var tex = EditorIO.GetPngTextureFromStrage ();
        CropImageModal.ShowCropImage (tex, 256);
        CropImageModal.onCrop = OnCropProfileImage;
#elif UNITY_IOS
        IOSCamera.Instance.PickImage(ISN_ImageSource.Library);
        IOSCamera.OnImagePicked += OnPickedProfileImage;
#elif UNITY_ANDROID
        AndroidCamera.Instance.OnImagePicked += OnPickedProfileImage;
        AndroidCamera.Instance.GetImageFromGallery();
#endif
    }

    public void SelectBackgroundImage ()
    {
#if UNITY_EDITOR
        var tex = EditorIO.GetPngTextureFromStrage ();
        CropImageModal.ShowCropImage (tex, backgroundImg.rectTransform.rect.size);
        CropImageModal.onCrop = OnCropBackgroundImage;
#elif UNITY_IOS
        IOSCamera.Instance.PickImage(ISN_ImageSource.Library);
        IOSCamera.OnImagePicked += OnPickedBackgroundImage;
#elif UNITY_ANDROID
        AndroidCamera.Instance.OnImagePicked += OnPickedBackgroundImage;
        AndroidCamera.Instance.GetImageFromGallery();
#endif
    }

    void OnCropProfileImage (Sprite croppedSprite)
    {
        Debug.Log ("on crop");
        profileImage.gameObject.SetActive (true);
        profileImage.sprite = croppedSprite;
    }

    void OnCropBackgroundImage (Sprite croppedSprite)
    {
        backgroundImg.sprite = croppedSprite;
    }

#if UNITY_IOS
    void OnPickedProfileImage (IOSImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, 256);
            CropImageModal.onCrop = OnCropProfileImage;
        }
        IOSCamera.OnImagePicked -= OnPickedProfileImage;
    }

    void OnPickedBackgroundImage (IOSImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, backgroundImg.rectTransform.rect.size);
            CropImageModal.onCrop = OnCropBackgroundImage;
        }
        IOSCamera.OnImagePicked -= OnPickedBackgroundImage;
    }
#endif

#if UNITY_ANDROID

	void OnPickedProfileImage (AndroidImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, 256);
            CropImageModal.onCrop = OnCropProfileImage;
        }

        AndroidCamera.Instance.OnImagePicked -= OnPickedProfileImage;
    }

    void OnPickedBackgroundImage (AndroidImagePickResult result)
    {
	    if (result.IsSucceeded) {
		    CropImageModal.ShowCropImage (result.Image, backgroundImg.rectTransform.rect.size);
		    CropImageModal.onCrop = OnCropBackgroundImage;
	    }
	    AndroidCamera.Instance.OnImagePicked -= OnPickedBackgroundImage;
    }
#endif
}
