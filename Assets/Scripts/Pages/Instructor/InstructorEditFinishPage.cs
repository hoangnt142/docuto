﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructorEditFinishPage : Page
{
    public override void Show ()
    {
        base.Show ();
        HeaderView.ShowMenu ("先生メニュー", MenuType);
    }

    public void OnRegisterCourseButtonClick()
    {
        InstructorEditCoursePage page = (InstructorEditCoursePage)PageManager.Show(PageType.EditCoursePage);
        page.SetUp();
    }
}
