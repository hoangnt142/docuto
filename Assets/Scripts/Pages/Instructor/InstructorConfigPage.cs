﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorConfigPage : Page
{

    [SerializeField]
    Image profileImage;

    [SerializeField]
    Sprite profileDefaultSprite;

    [SerializeField]
    Image backgroundImage;

    [SerializeField]
    Sprite backgroundDefaultSprite;

    [SerializeField]
    Text name;

    public override void Show ()
    {
        base.Show ();
        HeaderView.ShowMenu ("設定", MenuType);

        InstructorProfileImageLoader.GetImage (Session.LoggedInInstructor.id.ToString (), ImageSizes.detail, (Sprite sprite) => {
            if (sprite == null) {
                profileImage.sprite = profileDefaultSprite;
            } else {
                profileImage.sprite = sprite;
            }
        });

        InstructorBackgroundImageLoader.GetImage (Session.LoggedInInstructor.id.ToString (), (Sprite sprite) => {
            if (sprite == null) {
                backgroundImage.sprite = backgroundDefaultSprite;
            } else {
                backgroundImage.sprite = sprite;
            }
        });

        name.text = Session.LoggedInInstructor.name;
    }

    public void ShowInstructorProfileEditPage ()
    {
        var page = (InstructorProfileEditPage)PageManager.Show (PageType.InstructorProfileEdit, MenuType.Instructor);
        page.Setup (InstructorProfileEditPage.PageType.instructor);
    }

    public void ShowSettingPage()
    {
        PageManager.Show(PageType.Setting, MenuType.Instructor);
    }

    public void ShowBankAccountPage()
    {
        PageManager.Show(PageType.BankAccount, MenuType.Instructor);
    }

    public void ShowQueryPage()
    {
        PageManager.Show(PageType.Query, MenuType.Instructor);
    }

    public void ShowStudentMenu()
    {
        HeaderView.SwitchUserState();
    }

    public void ShowUploadVideoPage()
    {
        PageManager.Show(PageType.UploadVideoPage);
    }

    public void ShowHowToUsePage()
    {
        PageManager.Show(PageType.InstructorHowToUse);
    }
}
