﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructorMyCoursePage : Page
{
    [SerializeField]
    Transform listContent;

    [SerializeField]
    GameObject courseNode;

    private enum Tabs
    {
        None,
        Public,
        Private
    }
    private Tabs currentTab = Tabs.None;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowMenu("レッスン", MenuType);

        // by default, the subscribing courses will be showed
        if (currentTab == Tabs.None) GetCourses(true);
    }

    public void GetCourses(bool isPublic)
    {
        if ((isPublic && currentTab == Tabs.Public) || (!isPublic && currentTab == Tabs.Private))
            return;
        
        if (isPublic) currentTab = Tabs.Public;
        else currentTab = Tabs.Private;

        LoadingView.Show();
        InstructorAPI.GetCourses(Session.LoggedInInstructor.id, isPublic, ProcessListCourses, OnGetCourseFail);
    }

    void ProcessListCourses(List<CourseData> courses)
    {
        LoadingView.Hide();
        UpdateCourseTable(courses);
    }

    void OnGetCourseFail()
    {
        LoadingView.Hide();

        CourseData dummyCourse = new CourseData();
        dummyCourse.id = 1;
        dummyCourse.title = "【初心者大歓迎】やさしいヨガダイエット";
        dummyCourse.explanation = "初めての方・運動不足の方も安心して受けられる基本のコース。全身をしっかりと動かしながら約20種類のヨガポーズを行うことで、初回からホットヨガの効果を体感できます。";
        dummyCourse.necessity = "・ヨガマット\n・お水";

        CourseFeeData courseFee = new CourseFeeData();
        courseFee.id = 1;
        courseFee.fee_yen = 1000;

        dummyCourse.course_fee = courseFee;

        InstructorData instructor = new InstructorData();
        instructor.id = 1;
        instructor.name = "サオリ";
        instructor.title = "LAVA ヨガインストラクター";
        instructor.explanation = "秋田県出身3歳の頃からモダンバレエ、ジャズダンスなどを学び、舞台で表現すること、人を笑顔と幸せにするために役者を目指し上京。舞台で自分の身体を使い続ける中、YOGAに出会う。2007年、LAVA調布店にてインストラクターデビュー。フラやダンスの要素を取り入れたオリジナルレッスンも開催している。どんな時もどんな人にも元気と愛を。\n";

        dummyCourse.instructor = instructor;

        List<CourseData> data = new List<CourseData>();
        if (currentTab == Tabs.Public)
            for (int i = 0; i < 3; i++)
            {
                data.Add(dummyCourse);
            }
        else
            for (int i = 0; i < 5; i++)
            {
                data.Add(dummyCourse);
            }

        //init view
        UpdateCourseTable(data);
    }

    void UpdateCourseTable(List<CourseData> courses)
    {
        // Clear the current data
        foreach (Transform child in listContent)
        {
            Destroy(child.gameObject);
        }

        foreach (CourseData data in courses)
        {
            var prefapCourseNode = Instantiate(courseNode, listContent);

            prefapCourseNode.transform.localScale = new Vector3(1, 1, 1);
            prefapCourseNode.GetComponent<MyCourseNode>().UpdateView(data);
        }
    }

    public void ShowInstructorEditCoursePage()
    {
        InstructorEditCoursePage page =  (InstructorEditCoursePage)PageManager.Show(PageType.EditCoursePage);
        page.SetUp();
    }

    public override void Refresh()
    {
        if (currentTab == Tabs.Public)
        {
            InstructorAPI.GetCourses(Session.LoggedInInstructor.id, true, (c) => { UpdateCourseTable(c); }, () => { });
        }
        if (currentTab == Tabs.Private)
        {
            InstructorAPI.GetCourses(Session.LoggedInInstructor.id, false, (c) => { UpdateCourseTable(c); }, () => { });
        }
    }
}
