﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class InstructorVideoDetailPage : Page
{
    [SerializeField]
    Text updateTime, explaination, necessity;

    [SerializeField]
    VideoPlayer videoPlayer;


    LessonData currentData;

    public override void Show()
    {
        base.Show();

        if (!currentData.Equals(default(LessonData)))
        {
            Setup(currentData);
        }
    }

    public void Setup(LessonData data)
    {
        // show header title
        HeaderView.ShowSub(data.title, HeaderSubView.ButtonType.arrow, MenuType);
        currentData = data;

        explaination.text = currentData.explanation;
        necessity.text = currentData.necessity;
        DateTime updatedDate;
        if (!DateTime.TryParse(currentData.updated_at, out updatedDate))
        {
            updatedDate = DateTime.Now;
        }
        updateTime.text = updatedDate.ToString("yyyy/MM/dd HH:mm") + "更新";

        VideoThumbLoader.GetImage(currentData.video_id.ToString(), (Sprite sprite) =>
        {
            if (sprite != null)
            {
                videoPlayer.SetVideoThumb(sprite);
            }
            else
            {
                videoPlayer.SetVideoThumb(null);
            }
        });
        videoPlayer.SetHtmlUrl(Server.MakeUrl("videos/" + data.video_id + "/html", true));
    }

    public void ShowUploadVideoPage()
    {
        UploadVideoPage page = (UploadVideoPage)PageManager.Show(PageType.UploadVideoPage);
        page.Setup(currentData);
    }
}
