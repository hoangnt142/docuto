﻿using System;
using UnityEngine;
using UnityEngine.UI;

public class BankAccountPage : Page
{
    [SerializeField]
    InputField bankName;

    [SerializeField]
    Dropdown bankType;

    [SerializeField]
    InputField accountNumber;

    Text bankTypeLabel;
    BankData currentBankInfo;
    BankData newBankInfo;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("振込口座", HeaderSubView.ButtonType.arrow, MenuType);

        currentBankInfo = Session.LoggedInInstructor.bank;
        SetupView(currentBankInfo);
    }

    void SetupView(BankData bankInfo)
    {
        bankTypeLabel = bankType.captionText;
        float alphaTextColor = (128f / 255f);

        if (bankInfo.Equals(default(BankData)))
        {
            bankType.options.Add(new Dropdown.OptionData(""));
            bankType.value = bankType.options.Count - 1;
            bankType.options.RemoveAt(bankType.options.Count - 1);

            bankTypeLabel.text = "普通";
            bankTypeLabel.color = new Color(bankTypeLabel.color.r, bankTypeLabel.color.g, bankTypeLabel.color.b, alphaTextColor);
        }
        else
        {
            bankName.text = bankInfo.name;
            accountNumber.text = bankInfo.number.ToString();

            bankType.value = bankInfo.type - 1;
            bankTypeLabel.color = new Color(bankTypeLabel.color.r, bankTypeLabel.color.g, bankTypeLabel.color.b, 1f);
        }
    }

    public void OnBankTypeChange()
    {
        bankTypeLabel.color = new Color(bankTypeLabel.color.r, bankTypeLabel.color.g, bankTypeLabel.color.b, 1f);
    }

    public void UpdateBankInfo()
    {
        newBankInfo = new BankData();
        newBankInfo.name = bankName.text;
        newBankInfo.type = bankType.value + 1;
        try
        {
            newBankInfo.number = long.Parse(accountNumber.text);
        }
        catch (Exception ex)
        {
            Debug.Log("Error on update bank information: " + ex.Message);
            DialogView.Show("Invalid account number");
            return;
        }

        LoadingView.Show();
        InstructorAPI.UpdateBankInfo(Session.LoggedInInstructor.id, newBankInfo, processUpdateBankInfo);
    }

    void processUpdateBankInfo(bool isSucccess)
    {
        LoadingView.Hide();

        Session.LoggedInInstructor.bank = newBankInfo;
    }
}
