﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AgreementPage : Page
{
    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("利用規約", HeaderSubView.ButtonType.arrow, MenuType);
    }
}
