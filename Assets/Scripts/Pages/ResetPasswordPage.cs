﻿using UnityEngine;
using UnityEngine.UI;

public class ResetPasswordPage : Page
{
    [SerializeField]
    Text email;

    public override void Show()
    {
        base.Show();

        HeaderView.ShowSub("パスワードの再設定", HeaderSubView.ButtonType.arrow, MenuType);
    }

    public void ResetPassword()
    {
        UsersAPI.ResetPassword(email.text, ProcessResetPassword);
    }

    public void ProcessResetPassword(bool isSuccess,int errorType)
    {
        if (isSuccess)
        {
            DialogView.Show("メールを送信しました。", () => { PageManager.GoBackPage(); });
        }
        else
        {
            switch (errorType)
            {
                case 2:
                    DialogView.Show("Emailアドレスが存在しません");
                    break;
                default:
                    DialogView.Show("送信に失敗しました。");
                    break;
            }
        }
    }
}
