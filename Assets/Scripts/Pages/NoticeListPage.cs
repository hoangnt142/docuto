﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoticeListPage : Page
{
    [SerializeField]
    GameObject noticeNode;

    [SerializeField]
    Transform listContent;

    public override void Show ()
    {
        base.Show ();
        HeaderView.ShowMenu ("お知らせ", MenuType);

        LoadingView.Show();
        // Clear the current data
        foreach (Transform child in listContent)
        {
            Destroy(child.gameObject);
        }

        // Call API to get list of notices
        NoticeAPI.GetNotices (IsInstructorPage, this.GetNoticesCallBack);
    }

    void GetNoticesCallBack (bool isSucess, List<NoticeData> listData)
    {
        LoadingView.Hide();

        if (isSucess) {
            // Display data
            if (listData == null) {
                Debug.Log ("no notice data");
                // TODO show message on view
            } else {
                foreach (NoticeData data in listData) {
                    var PrefapNoticeNode = Instantiate (noticeNode, listContent);

                    PrefapNoticeNode.transform.localScale = new Vector3 (1, 1, 1);
                    NoticeNode node = PrefapNoticeNode.GetComponent<NoticeNode>();
                    if(IsInstructorPage) node.SetMode(NoticeNode.Mode.Instructor);
                    else node.SetMode(NoticeNode.Mode.User);
                    node.SetData (data);
                }
            }
        }
    }

    public void ShowNoticePage (NoticeNode node)
    {
        // Mark notice as read
        NoticeData data = node.GetData();
        if (data.Equals(null)) return;
        if(data.notice_type == 1)
        {
            // get notice
            int noticeId = node.GetData ().id;
            Debug.Log ("read notice id " + noticeId);

            NoticeAPI.GetNotice (data.id, GetNoticeCallBack);

        }
        else if(data.notice_type == 2)
        {
            // show chat page
            ChatAPI.GetChatGroup(data.chat_group_id, GetChatGroupCallBack);
        }
    }

    void GetChatGroupCallBack(bool isSuccess, ChatGroupData data)
    {
        // show ChatPage and setup it's data
        ChatPage chatPage = (ChatPage)PageManager.Show(PageType.Chat, PageManager.CurrentPage.MenuType);
        chatPage.Setup(data);
    }

    public void GetNoticeCallBack (bool isSuccess, NoticeData data)
    {
        if (isSuccess) {
            // Show detail page
            NoticePage page = (NoticePage)PageManager.Show (PageType.Notice);
            page.Setup (data);
        }
    }

}
