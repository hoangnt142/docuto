﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoticePage : Page
{
    [SerializeField]
    Text title;

    [SerializeField]
    Text time;

    [SerializeField]
    Text content;


    public void Setup(NoticeData data)
    {
        HeaderView.ShowSub("お知らせ", HeaderSubView.ButtonType.arrow, MenuType);

        // Title
        title.text = data.title;

        // Published at
        DateTime published;
        if (!DateTime.TryParse(data.publish_at, out published))
        {
            published = DateTime.Now;
        }

        time.text = published.ToString("yyyy/MM/dd (ddd)");

        // Content
        content.text = data.content;

        // finally, call api to mark notice as read
        NoticeAPI.ReadNotice(data.id);
    }

    public override void Show()
    {
        base.Show();
    }
}
