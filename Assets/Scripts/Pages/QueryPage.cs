﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QueryPage : Page
{
    [SerializeField]
    InputField queryText;


    bool isSending;

    public override void Show (MenuType menuType = MenuType.None)
    {
        base.Show (menuType);

        HeaderView.ShowSub ("お問い合わせ", HeaderSubView.ButtonType.close, MenuType);

        queryText.text = string.Empty;
    }

    public override void Hide ()
    {
        base.Hide ();
    }

    public void Send ()
    {
        if (isSending) return;
        int userId = 0, instructorId = 0;
        if (IsInstructorPage) instructorId = Session.LoggedInInstructor.id;
        else userId = Session.LoggedInUser.id;

        LoadingView.Show();
        isSending = true;

        InquiryAPI.Send(queryText.text, userId, instructorId, ProcessSendInquiry);
    }

    public void ProcessSendInquiry(bool isSuccess)
    {
        LoadingView.Hide();
        isSending = false;

        if (isSuccess)
            DialogView.Show("お問い合わせを送信しました", () =>
            {
                PageManager.GoBackPage();
            });
        else
            DialogView.Show("お問い合わせ送信に失敗しました。再度時間をあけて送信お願いします。");
    }
}
