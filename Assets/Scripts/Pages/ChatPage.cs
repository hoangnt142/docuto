﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using BestHTTP.WebSocket;
using LitJson;
using System.Runtime.InteropServices;
using System.Collections;
using System;

public class ChatPage : Page
{
#if UNITY_IOS && !UNITY_EDITOR
   [DllImport ("__Internal")]
   private static extern void NE_setKeepOpen (bool isKeepOpen);
#endif

    [SerializeField]
    InputField inputField;

    [SerializeField]
    GameObject conversationMe;

    [SerializeField]
    GameObject conversationYou;

    [SerializeField]
    Transform scrollContentTrn;

    TouchScreenKeyboard keyboard;

    bool canAdjustInputAreaForAndroid = false;
    float pageHeight;
    RectTransform pageRect;
    ChatGroupData currentData;

    void Start()
    {
        //inputField.OnPointerClick (null);
        pageRect = gameObject.GetComponent<RectTransform>();
        pageHeight = pageRect.rect.height;
    }

    public override void Show ()
    {
        base.Show ();

        inputField.gameObject.SetActive (true);

#if UNITY_IOS && !UNITY_EDITOR
        keyboard = TouchScreenKeyboard.Open("", TouchScreenKeyboardType.Default, autocorrection: false, multiline: false);
        TouchScreenKeyboard.hideInput = true;
        inputField.gameObject.SetActive (true);
        
        inputField.shouldHideMobileInput = false;
        inputField.gameObject.GetComponent<NativeEditBox> ().SetFocus (true);
        
#elif UNITY_ANDROID && !UNITY_EDITOR
        inputField.shouldHideMobileInput = true;
        //chatInputTrn.anchoredPosition = new Vector3 (0, 100000f);

        canAdjustInputAreaForAndroid = false;
        StartCoroutine(_ShowInputFieldLater());
#endif


        // 
#if UNITY_IOS && !UNITY_EDITOR
       NE_setKeepOpen (true);
#endif
    }

    IEnumerator _ShowInputFieldLater ()
    {
        yield return null;
        inputField.gameObject.GetComponent<NativeEditBox> ().SetFocus (true);

        if (!TouchScreenKeyboard.visible) {
            yield return null;
        }

        yield return new WaitForSeconds (0.5f);

        canAdjustInputAreaForAndroid = true;

    }

    public override void Hide ()
    {
        base.Hide ();

        HeaderView.RemoveEvent();

        if (keyboard != null) {
            keyboard.active = false;
        }
        ChatWebsocket.Close ();
        inputField.gameObject.SetActive (false);

        inputField.gameObject.GetComponent<NativeEditBox> ().SetFocus (false);
#if UNITY_IOS && !UNITY_EDITOR
       NE_setKeepOpen (false);
#endif
    }

    public void Setup (ChatGroupData data)
    {
        currentData = data;
        SetupHeader(data);

        // Clear the current data
        foreach (Transform child in scrollContentTrn) {
            Destroy (child.gameObject);
        }

        foreach (ChatData chat in data.chats) {
            // for Me's prefab
            if (chat.user.id == Session.LoggedInUser.id) {
                var prefapConversationMe = Instantiate (conversationMe, scrollContentTrn);
                ChatMessageNode chatNode = prefapConversationMe.GetComponent<ChatMessageNode> ();

                chatNode.SetData(chat);
            }

            // for You's prefab
            else {
                var prefapConversationYou = Instantiate (conversationYou, scrollContentTrn);
                ChatMessageNode chatNode = prefapConversationYou.GetComponent<ChatMessageNode>();

                chatNode.SetData (chat);
            }
        }

        ChatWebsocket.Setup (data.id, IsInstructorPage, OnMessage);
    }

    void OnMessage (WebSocket ws, string message)
    {
        JsonData chunk = JsonMapper.ToObject (message);

        if (chunk.Keys.Contains ("type") && (string)chunk ["type"] == "ping") {
            Debug.Log (message);
        } else if (chunk.Keys.Contains ("type") && (string)chunk ["type"] == "confirm_subscription") {
            Debug.Log (message);
        } else if (!chunk.Keys.Contains ("type")) {
            var chat = new ChatData () {
                user = new UserData () {
                    id = int.Parse((string)chunk ["message"] ["user_id"]),
                    instructor = !string.IsNullOrEmpty((string)chunk ["message"] ["instructor_id"]) ? new InstructorData () { id = int.Parse((string)chunk ["message"] ["instructor_id"]) } : null
                },
                text = (string)chunk ["message"] ["text"],
                time = DateTime.Parse ((string)chunk ["message"] ["created_at"]).ToString ("yyyy/M/d H:m")
            };
            if ((string)chunk ["message"] ["user_id"] != Session.LoggedInUser.id.ToString ()) {
                var prefapConversationYou = Instantiate (conversationYou, scrollContentTrn);
                prefapConversationYou.GetComponent<ChatMessageNode> ().SetData (chat);
            } else {
                var prefapConversationMe = Instantiate (conversationMe, scrollContentTrn);
                prefapConversationMe.GetComponent<ChatMessageNode> ().SetData (chat);
            }
        }
    }

    public void SubmitMessage ()
    {
        ChatWebsocket.SendMessage (inputField.text);
        inputField.text = "";
        inputField.gameObject.GetComponent<NativeEditBox> ().text = "";
        inputField.gameObject.GetComponent<NativeEditBox> ().SetFocus(true);
    }

    void LateUpdate ()
    {
        if (!IsShown || (Application.platform == RuntimePlatform.Android && !canAdjustInputAreaForAndroid)) {
            return;
        }

        float keyboardHeightRatio;
        if (Application.platform == RuntimePlatform.Android && canAdjustInputAreaForAndroid)
        {
            keyboardHeightRatio = ((float)ApplicationSystem.GetAndroidKeyboardHeight()) / ((float)Screen.height);

            if (keyboardHeightRatio < 0.05) keyboardHeightRatio = 0;
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            keyboardHeightRatio = TouchScreenKeyboard.area.height / Screen.height;
        }
        else
        {
            keyboardHeightRatio = 0;
        }

        pageRect.offsetMin = new Vector2(0, (pageHeight + 36) * keyboardHeightRatio);
    }

    void SetupHeader(ChatGroupData data)
    {
        //Set title
        string headerName;
        if (!IsInstructorPage)
        {
            HeaderView.SetEvent(() =>
            {
                InstructorCourseListPage page = (InstructorCourseListPage)PageManager.Instantiate(PageType.InstructorCourseList);
                page.SetUp(data.to_user.instructor);
                PageManager.Show(PageType.InstructorCourseList);
            });

            headerName = data.to_user.instructor.name;
        }
        else
        {
            HeaderView.RemoveEvent();
            headerName = data.to_user.name;
        }
        HeaderView.ShowSub(headerName, HeaderSubView.ButtonType.arrow, MenuType);
    }
}
