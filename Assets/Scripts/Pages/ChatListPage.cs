﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ChatListPage : Page
{
    [SerializeField]
    GameObject chatListNode;

    [SerializeField]
    Transform listContent;

    [SerializeField]
    GameObject emptyMessage;
    public override void Show()
    {
        base.Show();
        HeaderView.ShowMenu("チャット", MenuType);

        // load Chat Group Data
        LoadChatGroup();

    }
    public override void Hide()
    {
        base.Hide();

    }
    void LoadChatGroup()
    {
        LoadingView.Show();
        ChatAPI.GetListChatGroup(IsInstructorPage, ProcessGetChatGroup, OnErrorGetChatGroup);
    }

    void ProcessGetChatGroup(List<ChatGroupData> chatgroups)
    {
        LoadingView.Hide();

        // Clear the current data
        foreach (Transform child in listContent)
        {
            Destroy(child.gameObject);
        }

        if (chatgroups.Count <= 0)
        {
            emptyMessage.SetActive(true);
        }
        else
        {
            emptyMessage.SetActive(false);

            // Sort data
            chatgroups.Sort((m1, m2) => { return m2.last_chat.time.CompareTo(m1.last_chat.time); });

            // Display data
            foreach (ChatGroupData data in chatgroups)
            {
                var prefapChatListNode = Instantiate(chatListNode, listContent);

                prefapChatListNode.transform.localScale = new Vector3(1, 1, 1);
                ChatListNode listNode = prefapChatListNode.GetComponent<ChatListNode>();
                listNode.IsInInstructorPage = IsInstructorPage;
                listNode.SetData(data);
            }
        }
    }

    void OnErrorGetChatGroup()
    {
        LoadingView.Hide();
        DialogView.Show("チャット情報の取得に失敗しました");
    }

    public void ShowChat(ChatListNode node)
    {
        // Show chat detail
        Debug.Log("read chat group to user " + node.GetData().chat_group_id);
        ChatAPI.GetChatGroup(node.GetData().chat_group_id, GetChatGroupCallBack);
    }

    void GetChatGroupCallBack(bool isSuccess, ChatGroupData data)
    {
        // show ChatPage and setup it's data
        ChatPage chatPage = (ChatPage)PageManager.Show(PageType.Chat, PageManager.CurrentPage.MenuType);
        chatPage.Setup(data);
    }

    void OnErrorCallChatPage()
    {
        DialogView.Show("チャットの読み込みに失敗しました。");
    }

}
