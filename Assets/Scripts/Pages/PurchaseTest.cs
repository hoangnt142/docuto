﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PurchaseTest : MonoBehaviour
{

    // Use this for initialization
    void Start ()
    {
        UM_InAppPurchaseManager.Client.OnServiceConnected += OnBillingConnectFinishedAction;
        UM_InAppPurchaseManager.Client.OnPurchaseFinished += OnPurchaseFlowFinishedAction;
        UM_InAppPurchaseManager.Client.Connect ();
    }

    // Update is called once per frame
    void Update ()
    {

    }

    public void BuyTest ()
    {
        UM_InAppPurchaseManager.Client.Purchase ("500_1");
    }

    private void OnBillingConnectFinishedAction (UM_BillingConnectionResult result)
    {
        UM_InAppPurchaseManager.Client.OnServiceConnected -= OnBillingConnectFinishedAction;
        if (result.isSuccess) {
            Debug.Log ("Connected");
        } else {
            Debug.Log ("Failed to connect");
        }
    }

    private void OnPurchaseFlowFinishedAction (UM_PurchaseResult result)
    {
        UM_InAppPurchaseManager.Client.OnPurchaseFinished -= OnPurchaseFlowFinishedAction;
        if (result.isSuccess) {
            Debug.Log( "Product " + result.product.id + " purchase Success");
        } else {
            Debug.Log( "Product " + result.product.id + " purchase Failed");
        }
    }
}
