﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SwitchButtonView : MonoBehaviour
{

    [SerializeField]
    Toggle toggle;

    [SerializeField]
    Text text;

    [SerializeField]
    Image backgroundImage;

    [SerializeField]
    Color normalColor;

    [SerializeField]
    Color highlightedColor;

    void Awake ()
    {
        toggle.onValueChanged.AddListener (OnToggleValueChanged);
        OnToggleValueChanged (toggle.isOn);
    }

    void OnToggleValueChanged (bool isChecked)
    {
        if (isChecked) {
            backgroundImage.color = highlightedColor;
            text.color = normalColor;
        } else {
            backgroundImage.color = normalColor;
            text.color = highlightedColor;
        }
    }

}
