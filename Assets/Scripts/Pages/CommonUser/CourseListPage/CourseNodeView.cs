using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CourseNodeView : MonoBehaviour
{
    static Color32 WHITE_STAR_COLOR = new Color32(255, 255, 255, 255);
    static Color32 YELLOW_STAR_COLOR = new Color32(255, 237, 0, 255);
    internal static System.Object Locker = new System.Object();

    private CourseData curData;

    [SerializeField]
    private Image defaultCourseImage, courseImage, star;

    [SerializeField]
    private Text title;

    [SerializeField]
    private Text fee;

    [SerializeField]
    private Text instructor;

    [SerializeField]
    private Text levelText;

    [SerializeField]
    private Image instructorAvatar;

    [SerializeField]
    Sprite defaultInstructorAvatar, defaultCourseImageSprite;

    bool isProcessingFavoriteButton;
    bool isProcessingGetDetail;
    DetailPage detailPage = DetailPage.Course;
    public enum DetailPage
    {
        Course,
        Lesson
    }

    public void SetDetailPage(DetailPage detail)
    {
        detailPage = detail;
    }

    public void UpdateView(CourseData courseData)
    {
        curData = courseData;
        title.text = curData.title;

        if (curData.is_subscribing)
        {
            fee.text = "入会中";
        }
        else
        {
            fee.text = string.Format("月々{0:0,0}円", curData.course_fee.fee_yen);
        }

        instructor.text = curData.instructor.name;
        star.color = courseData.is_favorite ? YELLOW_STAR_COLOR : WHITE_STAR_COLOR;

        switch (courseData.level)
        {
            case 1:
                levelText.text = "初心者向け";
                break;
            case 2:
                levelText.text = "中級者向け";
                break;
            case 3:
                levelText.text = "上級者向け";
                break;

            default:
                levelText.text = "初心者向け";
                break;
        }

        InstructorProfileImageLoader.GetImage(curData.instructor.id.ToString(), ImageSizes.icon, (Sprite sprite) =>
        {
            if (instructorAvatar == null) return;
            if (sprite != null)
            {
                instructorAvatar.sprite = sprite;
            }
            else
            {
                instructorAvatar.sprite = defaultInstructorAvatar;
            }
        });

        CourseImageLoader.GetImage(curData.id.ToString(), ImageSizes.thumb, (Sprite sprite) =>
        {
            if (courseImage == null) return;
            if (sprite != null)
            {
                courseImage.sprite = sprite;
            }
            else
                courseImage.sprite = defaultCourseImageSprite;

            defaultCourseImage.gameObject.SetActive(false);
            courseImage.gameObject.SetActive(true);
        });
    }

    public void OnCurCourseClick()
    {
        if (System.Threading.Monitor.TryEnter(Locker))
        {
            if (curData.id > 0 && !isProcessingGetDetail)
            {
                lock (Locker)
                {
                    isProcessingGetDetail = true;
                    if (detailPage == DetailPage.Course)
                        CourseAPI.GetDetail(curData.id, ShowDetailPage, OnGetDetailError);

                    if (detailPage == DetailPage.Lesson)
                        CourseAPI.GetLatestLessonDetail(curData.id, ShowLessonDetailPage, OnGetLessonDetailError);
                }
            }

            System.Threading.Monitor.Exit(Locker);
        }
    }

    public void ShowDetailPage(CourseData course)
    {
        CourseDetailPage page = (CourseDetailPage)PageManager.Instantiate(PageType.CourseDetail);

        page.SetCourseNode(this);
        page.Setup(course);

        PageManager.Show(PageType.CourseDetail);
        isProcessingGetDetail = false;
    }

    public void OnGetDetailError()
    {
        CourseDetailPage page = (CourseDetailPage)PageManager.Show(PageType.CourseDetail);

        // create dummy data
        CourseData dummyCourse = new CourseData();
        dummyCourse.id = 1;
        dummyCourse.title = "【初心者大歓迎】やさしいヨガダイエット";
        dummyCourse.explanation = "初めての方・運動不足の方も安心して受けられる基本のコース。全身をしっかりと動かしながら約20種類のヨガポーズを行うことで、初回からホットヨガの効果を体感できます。";
        dummyCourse.necessity = "・ヨガマット\n・お水";
        dummyCourse.is_favorite = true;

        CourseFeeData courseFee = new CourseFeeData();
        courseFee.id = 1;
        courseFee.fee_yen = 987;
        dummyCourse.course_fee = courseFee;

        InstructorData instructorData = new InstructorData();
        instructorData.id = 1;
        instructorData.name = "サオリ";
        instructorData.title = "LAVA ヨガインストラクター";
        instructorData.explanation = "秋田県出身3歳の頃からモダンバレエ、ジャズダンスなどを学び、舞台で表現すること、人を笑顔と幸せにするために役者を目指し上京。舞台で自分の身体を使い続ける中、YOGAに出会う。2007年、LAVA調布店にてインストラクターデビュー。フラやダンスの要素を取り入れたオリジナルレッスンも開催している。どんな時もどんな人にも元気と愛を。\n";
        dummyCourse.instructor = instructorData;

        page.Setup(dummyCourse);

        isProcessingGetDetail = false;
    }

    public void OnFavoriteClick()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        // prevent bubbling click
        if (isProcessingFavoriteButton) return;

        if (curData.id > 0)
        {
            isProcessingFavoriteButton = true;

            if (curData.is_favorite)
            {
                Debug.Log("Remove " + curData.id + " from favorite");
                CourseAPI.RemoveFavorite(curData.id, UpdateFavoriteStarColor, UpdateFavoriteError);
            }
            else
            {
                Debug.Log("Add " + curData.id + " to favorite");
                CourseAPI.AddFavorite(curData.id, UpdateFavoriteStarColor, UpdateFavoriteError);
            }
        }
    }

    public void UpdateFavoriteStarColor(bool isAdd)
    {
        star.color = isAdd ? YELLOW_STAR_COLOR : WHITE_STAR_COLOR;
        curData.is_favorite = isAdd;

        //update page controller
        CourseListPage page = (CourseListPage) PageManager.GetActivedPage(PageType.CourseList);
        page.RefreshCategory(curData);

        isProcessingFavoriteButton = false;
    }

    public void UpdateFavoriteError()
    {
        isProcessingFavoriteButton = false;
    }

    public void ShowLessonDetailPage(LessonData lessonDetail)
    {
        // set header text = course title
        lessonDetail.title = curData.title;

        LessonDetailPage page = (LessonDetailPage)PageManager.Show(PageType.LessonDetailPage);
        page.Setup(lessonDetail);

        isProcessingGetDetail = false;
    }

    public void OnGetLessonDetailError()
    {
        // users can show lesson detail page to unsubscribe, so we still show page with a empty lesson
        LessonData emptyLesson = new LessonData();
        emptyLesson.instructor = curData.instructor;
        emptyLesson.title = curData.title;

        LessonDetailPage page = (LessonDetailPage)PageManager.Show(PageType.LessonDetailPage);
        page.Setup(emptyLesson);

        isProcessingGetDetail = false;
    }
}