using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MyCoursePage : Page
{
    [SerializeField]
    Transform listContent;

    [SerializeField]
    GameObject courseNode;

    [SerializeField]
    ScrollRect scroller;

    [SerializeField]
    GridLayoutGroup gridLayoutGroup;

    [SerializeField]
    GameObject noCourseText;

    private Tabs currentTab = Tabs.None;

    private enum Tabs
    {
        None,
        Subscribing,
        Subscribed
    }

    public override void Show()
    {
        base.Show();
        HeaderView.ShowMenu("マイレッスン", MenuType);

        if(currentTab == Tabs.Subscribed)
        {
            GetSubscribed();
        }
        else
        {
            GetSubscribing();
        }
    }

    public void GetSubscribingCourses()
    {
        if (currentTab != Tabs.Subscribing)
        {
            GetSubscribing();
        }
    }

    private void GetSubscribing()
    {
        LoadingView.Show();
        currentTab = Tabs.Subscribing;

        UsersAPI.GetSubscribeCourses(Session.LoggedInUser.id, true, ProcessListCourses, OnGetCourseFail);
    }

    public void GetSubscribedCourses()
    {
        if (currentTab != Tabs.Subscribed)
        {
            GetSubscribed();
        }
    }

    private void GetSubscribed()
    {
        LoadingView.Show();
        currentTab = Tabs.Subscribed;

        UsersAPI.GetSubscribeCourses(Session.LoggedInUser.id, false, ProcessListCourses, OnGetCourseFail);
    }

    public void ProcessListCourses(List<CourseData> course)
    {
        LoadingView.Hide();

        UpdateCourseTable(course);
    }

    private void UpdateCourseTable(List<CourseData> course)
    {
        scroller.verticalNormalizedPosition = 1;
        gridLayoutGroup.enabled = false;

        // Clear the current data
        foreach (Transform child in listContent)
        {
            Destroy(child.gameObject);
        }

        CourseNodeView.DetailPage pageDetailType = CourseNodeView.DetailPage.Lesson;
        if (currentTab == Tabs.Subscribed)
        {
            pageDetailType = CourseNodeView.DetailPage.Course;
        }

        if(course.Count > 0)
        {
            noCourseText.SetActive(false);
            foreach (CourseData data in course)
            {
                var prefapCourseNode = Instantiate(courseNode, listContent);

                prefapCourseNode.transform.localScale = new Vector3(1, 1, 1);
                prefapCourseNode.GetComponent<CourseNodeView>().SetDetailPage(pageDetailType);
                prefapCourseNode.GetComponent<CourseNodeView>().UpdateView(data);
            }
        }
        else
        {
            noCourseText.SetActive(true);
        }
       
        gridLayoutGroup.enabled = true;
    }

    private void OnGetCourseFail()
    {
        LoadingView.Hide();

        CourseData dummyCourse = new CourseData();
        dummyCourse.id = 1;
        dummyCourse.title = "【初心者大歓迎】やさしいヨガダイエット";
        dummyCourse.explanation = "初めての方・運動不足の方も安心して受けられる基本のコース。全身をしっかりと動かしながら約20種類のヨガポーズを行うことで、初回からホットヨガの効果を体感できます。";
        dummyCourse.necessity = "・ヨガマット\n・お水";

        CourseFeeData courseFee = new CourseFeeData();
        courseFee.id = 1;
        courseFee.fee_yen = 1000;

        dummyCourse.course_fee = courseFee;

        InstructorData instructor = new InstructorData();
        instructor.id = 1;
        instructor.name = "サオリ";
        instructor.title = "LAVA ヨガインストラクター";
        instructor.explanation = "秋田県出身3歳の頃からモダンバレエ、ジャズダンスなどを学び、舞台で表現すること、人を笑顔と幸せにするために役者を目指し上京。舞台で自分の身体を使い続ける中、YOGAに出会う。2007年、LAVA調布店にてインストラクターデビュー。フラやダンスの要素を取り入れたオリジナルレッスンも開催している。どんな時もどんな人にも元気と愛を。\n";

        dummyCourse.instructor = instructor;

        List<CourseData> data = new List<CourseData>();
        if (currentTab == Tabs.Subscribing)
            for (int i = 0; i < 3; i++)
            {
                data.Add(dummyCourse);
            }
        else
            for (int i = 0; i < 5; i++)
            {
                data.Add(dummyCourse);
            }

        //init view
        UpdateCourseTable(data);
    }
}
