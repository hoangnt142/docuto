﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnSubscribePage : Page
{
    [SerializeField]
    Text reasonText;

    CourseData data;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("退会手続き", HeaderSubView.ButtonType.close, MenuType);
    }

    public void Setup(CourseData data)
    {
        this.data = data;

        //setup the view
    }

    public void ShowConfirmPage()
    {
        //dummy data
        this.data = new CourseData();
        this.data.title = "【初心者大歓迎】\r\n やさしいヨガダイ エット";
        this.data.course_fee = new CourseFeeData();
        this.data.course_fee.fee_yen = 978;


        UnSubscribeConfirmPage confirmPage = (UnSubscribeConfirmPage)PageManager.Show(PageType.UnSubscribeConfirm);

        this.data.unsubsribe_reason = reasonText.text;
        confirmPage.Setup(this.data);
    }
}
