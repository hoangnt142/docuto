﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HedgehogTeam.EasyTouch;

public class CourseListPage : Page
{
    public List<CourseCategoryData> courseCategoryData = new List<CourseCategoryData>();
    public List<CourseData> courseData = new List<CourseData>();

    [SerializeField]
    CourseTableView courseTableController;

    [SerializeField]
    CategoryView categoryView;

    int currentCategoryId = 0;
    int favoriteCategoryId = 0;
    int currentCategoryIndex = 0;
    int currentLevel = 0;
    Tabs currentTab = Tabs.None;
    enum Tabs
    {
        None,
        Begin,
        Junior,
        Senior
    }
    PageModes pageMode;
    PageModes currentPageMode;
    enum PageModes
    {
        NoneAuth,
        Auth
    }

    Dictionary<int, List<CourseData>> coursePool = new Dictionary<int, List<CourseData>> ();
    Dictionary<int, int>  nextPage = new Dictionary<int, int>();
    Dictionary<int, bool> hasNextPage = new Dictionary<int, bool>();
    Dictionary<int, bool> isGettingCourse = new Dictionary<int, bool>();

    string emptyMessage = "レッスンがありません。";

    public bool HasNextPage(int categoryId)
    {
        if (!hasNextPage.ContainsKey(categoryId)) return false;

        return hasNextPage[categoryId];
    }

    public override void Show()
    {
        base.Show();
        HeaderView.ShowMenu("", MenuType);

        if (!Session.HasToken()) pageMode = PageModes.NoneAuth;
        else pageMode = PageModes.Auth;

        bool initedCategory = false;
        if(currentPageMode != pageMode)
        {
            currentPageMode = pageMode;
            InitCategoryData();
            initedCategory = true;
        }
            
        if (currentTab == Tabs.None)
        {
            currentTab = Tabs.Begin;
            if ( !initedCategory ) InitCategoryData();
        }

        courseTableController.SetController(this);

        EnableSwipe();
    }

    public override void Hide()
    {
        base.Hide();

        MasterCourseCategory.Clear();
        DisableSwipe();
    }

    public void EnableSwipe()
    {
        EasyTouch.On_Swipe += On_Swipe;
        EasyTouch.On_SwipeEnd += On_SwipeEnd;
    }

    public void DisableSwipe()
    {
        EasyTouch.On_Swipe -= On_Swipe;
        EasyTouch.On_SwipeEnd -= On_SwipeEnd;
    }

    private void InitCategoryData()
    {
        coursePool.Clear();
        courseTableController.ClearData();

        MasterCourseCategory.GetData (ProcessGetCategories, OnFailGetCategories);
    }

    private void ProcessGetCategories(List<CourseCategoryData> categories)
    {
        courseCategoryData.Clear();

        // user foreach to add item to the list instead of reference variable
        foreach (CourseCategoryData c in categories)
            courseCategoryData.Add(c);

        categoryView.Reload();

        EmptyScroller();
    }

    private void OnFailGetCategories()
    {
        // on fail, just show dummy data
        courseCategoryData.Clear();
        courseCategoryData.Add(new CourseCategoryData() { name = "ストレッチ", id = 1 });
        courseCategoryData.Add(new CourseCategoryData() { name = "ヨガ", id = 2 });
        courseCategoryData.Add(new CourseCategoryData() { name = "ピラティス", id = 3 });
        courseCategoryData.Add(new CourseCategoryData() { name = "筋トレ", id = 4 });

        categoryView.Reload();
    }

    public void GetCourseByLevel(int level)
    {
        if (currentLevel == level) return;

        //currentLevel = level;
        hasNextPage.Clear();
        nextPage.Clear();

        CourseCategoryData currentCategoryData =  courseCategoryData.Find((d) => (d.id == currentCategoryId));
        if(currentCategoryData.isFavorite)
            LoadFavoriteCourses(currentCategoryId, true);
        else 
            LoadCourseData(currentCategoryId, true);
    }

    public void LoadCourseData(int courseCategoryId, bool forceReload = false)
    {
        currentCategoryId = courseCategoryId;
        currentCategoryIndex = courseCategoryData.FindIndex((d) => ( d.id == courseCategoryId));
        if (forceReload)
        {
            coursePool.Clear();
            courseTableController.ClearData();
            EmptyScroller();
        }

        courseTableController.ActiveTable(courseCategoryId);
        if (coursePool.ContainsKey(courseCategoryId))
        {
            courseData = coursePool[courseCategoryId];
            //return;
        }

        LoadingView.Show();
        isGettingCourse[courseCategoryId] = true;
        CourseAPI.GetByCategoryId(courseCategoryId, currentLevel, 1, ProcessGetByCategoryId, OnFailGetByCategoryId, Pagination);
    }

    private void Pagination(bool hasNext)
    {
        if (hasNext)
        {
            if (!nextPage.ContainsKey(currentCategoryId))
                nextPage.Add(currentCategoryId, 2);
            else
                nextPage[currentCategoryId] = nextPage[currentCategoryId] + 1;

            if (!hasNextPage.ContainsKey(currentCategoryId))
                hasNextPage.Add(currentCategoryId, true);
            else
                hasNextPage[currentCategoryId] = true;
        }
        else
        {
            if (!hasNextPage.ContainsKey(currentCategoryId))
                hasNextPage.Add(currentCategoryId, false);
            else
                hasNextPage[currentCategoryId] = false;
        }
    }

    private void ProcessGetByCategoryId(List<CourseData> courses)
    {
        LoadingView.Hide();

        //save to pool
        if (!coursePool.ContainsKey(currentCategoryId))
        {
            coursePool.Add(currentCategoryId, courses);
            if (courses.Count > 0)
            {
                courseTableController.Add(currentCategoryId, courses);
            }
            else
            {
                courseTableController.Add(currentCategoryId, emptyMessage);
            }
        }

        courseData = courses;
        courseTableController.ActiveTable(currentCategoryId);
        isGettingCourse[currentCategoryId] = false;
        courseTableController.SetIsRefreshing(false);
    }

    private void OnFailGetByCategoryId()
    {
        LoadingView.Hide();

        courseData.Clear();
        for (int i = 0; i < currentCategoryId; i++)
        {
            courseData.Add(new CourseData()
            {
                id = i,
                title = "【初心者大歓迎】やさしいヨガやさしいヨガやさしいヨガ",
                image_path = "",
                course_fee = new CourseFeeData()
                {
                    fee_yen = 1000
                },
                instructor = new InstructorData()
                {
                    name = "サオリ"
                }
            });
        }

        courseTableController.Add(currentCategoryId, courseData);
        courseTableController.ActiveTable(currentCategoryId);
        isGettingCourse[currentCategoryId] = false;
        courseTableController.SetIsRefreshing(false);
    }

    public void LoadFavoriteCourses(int favCategoryId, bool forceReload = false)
    {
        currentCategoryId = favoriteCategoryId = favCategoryId;
        currentCategoryIndex = courseCategoryData.FindIndex((d) => ( d.id == favCategoryId));
        if (forceReload)
        {
            coursePool.Clear();
            courseTableController.ClearData();
            EmptyScroller();
        }

        courseTableController.ActiveTable(favCategoryId);
        if (coursePool.ContainsKey(favCategoryId))
        {
            courseData = coursePool[favCategoryId];
            return;
        }

        LoadingView.Show();
        isGettingCourse[favCategoryId] = true;
        CourseAPI.GetListFavorite(currentLevel, ProcessGetListFavorite, OnFailGetListFavorite);
    }

    private void ProcessGetListFavorite(List<CourseData> courses)
    {
        LoadingView.Hide();

        if (courses.Count > 0)
        {
            //save to pool
            if (!coursePool.ContainsKey(currentCategoryId))
            {
                coursePool.Add(currentCategoryId, courses);
                courseTableController.Add(currentCategoryId, courses);
            }
        }
        else
        {
            coursePool.Add(currentCategoryId, null);
            courseTableController.Add(currentCategoryId, "お気に入りはまだありません。");
        }

        courseTableController.ActiveTable(currentCategoryId);
        isGettingCourse[currentCategoryId] = false;
        courseTableController.SetIsRefreshing(false);
    }

    private void OnFailGetListFavorite()
    {
        LoadingView.Hide();

        //Show dummy data
        courseData.Clear();
        for (int i = 0; i < 6; i++)
        {
            courseData.Add(new CourseData()
            {
                id = i,
                title = "【初心者大歓迎】やさしいヨガやさしいヨガやさしいヨガ",
                image_path = "",
                is_favorite = true,
                course_fee = new CourseFeeData()
                {
                    fee_yen = 1000
                },
                instructor = new InstructorData()
                {
                    name = "サオリ"
                }
            });
        }

        courseTableController.Add(currentCategoryId, courseData);
        courseTableController.ActiveTable(currentCategoryId);
        isGettingCourse[currentCategoryId] = false;
        courseTableController.SetIsRefreshing(false);
    }

    bool moveContent = true;
    void On_Swipe(Gesture gesture)
    {
        float xDistance = gesture.swipeVector.x;
        float yDistance = gesture.swipeVector.y;
        if (Mathf.Abs(xDistance) > 5.5f && Mathf.Abs(yDistance) < 5.5f && moveContent)
        {
            //courseTableController.SetOnBoundary(false);

            if(xDistance > 0) currentCategoryIndex--;
            if(xDistance < 0) currentCategoryIndex++;
            if (currentCategoryIndex > courseCategoryData.Count - 1)
            {
                currentCategoryIndex = 0;
            }
            if (currentCategoryIndex < 0)
            {
                currentCategoryIndex = courseCategoryData.Count - 1;
            }

            currentCategoryId = courseCategoryData[currentCategoryIndex].id;

            categoryView.ActiveItem(currentCategoryIndex);

            moveContent = false;
        }
    }

    void On_SwipeEnd(Gesture gesture)
    {
        moveContent = true;
    }

    public void SetCourseListOpaque(bool isActive)
    {
        courseTableController.SetOpaque(isActive);
    }

    void EmptyScroller()
    {
        foreach (CourseCategoryData c in courseCategoryData)
        {
            if (c.isFavorite) 
                courseTableController.AddToPool(c.id, "お気に入りはまだありません。");
            else 
                courseTableController.AddToPool(c.id, emptyMessage);
        }

        courseTableController.Reload();
    }

    public void NextPage()
    {
        if (!isGettingCourse[currentCategoryId])
        {
            //LoadingView.Show();

            isGettingCourse[currentCategoryId] = true;
            CourseAPI.GetByCategoryId(currentCategoryId, currentLevel, nextPage[currentCategoryId], ProcessGetNextPage, OnFailGetNextPage, Pagination);
        }
    }

    void ProcessGetNextPage(List<CourseData> courses)
    {
        //LoadingView.Hide();

        courseTableController.Append(courses);
        isGettingCourse[currentCategoryId] = false;
    }

    void OnFailGetNextPage()
    {
        //LoadingView.Hide();

        courseTableController.EndLoadNextPage();
        isGettingCourse[currentCategoryId] = false;
    }

    public void ReLoadCategory()
    {
        LoadCourseData(currentCategoryId, false);
    }

    public override void Refresh()
    {
        base.Refresh();

        coursePool.Clear();
        courseTableController.ClearData();
        EmptyScroller();

        if (currentCategoryId == favoriteCategoryId)
            CourseAPI.GetListFavorite(currentLevel, ProcessGetListFavorite, OnFailGetListFavorite);
        else 
            CourseAPI.GetByCategoryId(currentCategoryId, currentLevel, 1, ProcessGetByCategoryId, OnFailGetByCategoryId, Pagination);
    }

    public void RefreshCategory(CourseData updatedCourse)
    {
        // find favorite category id
        if (favoriteCategoryId == 0)
        {
            CourseCategoryData favCategory = courseCategoryData.Find(c => c.isFavorite);
            favoriteCategoryId = favCategory.id;
        }
        
        // if current is favorite category
        CourseCategoryData currentCategoryData = courseCategoryData.Find((d) => (d.id == currentCategoryId));
        if (currentCategoryData.isFavorite)
        {
            if (coursePool.ContainsKey(favoriteCategoryId))
            {
                if (!updatedCourse.is_favorite)
                {
                    coursePool[favoriteCategoryId].RemoveAt(coursePool[favoriteCategoryId].FindIndex(c => c.id == updatedCourse.id));
                    //refresh itself
                    if (coursePool[favoriteCategoryId].Count > 0)
                        courseTableController.Refresh(coursePool[favoriteCategoryId], favoriteCategoryId);
                    else
                        courseTableController.Refresh(emptyMessage, favoriteCategoryId);
                }
            }

            foreach(int cid in coursePool.Keys)
            {
                if (coursePool[cid] == null || coursePool[cid].Count == 0) continue;

                int itemIndex = coursePool[cid].FindIndex(c => c.id == updatedCourse.id);
                if (itemIndex > -1)
                {
                    CourseData newCourseData = coursePool[cid][itemIndex];
                    newCourseData.is_favorite = updatedCourse.is_favorite;
                    coursePool[cid].RemoveAt(itemIndex);
                    coursePool[cid].Insert(itemIndex, newCourseData);
                    // refresh other categories
                    courseTableController.Refresh(coursePool[cid], cid);
                    break;
                }
            }
        }
        // if not
        else
        {
            if (coursePool.ContainsKey(favoriteCategoryId))
            {
                if (coursePool[favoriteCategoryId] == null) coursePool[favoriteCategoryId] = new List<CourseData>();

                if (updatedCourse.is_favorite) coursePool[favoriteCategoryId].Insert(0, updatedCourse);
                else
                {
                    coursePool[favoriteCategoryId].RemoveAt(coursePool[favoriteCategoryId].FindIndex(c => c.id == updatedCourse.id));
                }
                if(coursePool[favoriteCategoryId].Count > 0)
                    courseTableController.Refresh(coursePool[favoriteCategoryId], favoriteCategoryId);
                else
                    courseTableController.Refresh(emptyMessage, favoriteCategoryId);
            }
            else
            {
                CourseAPI.GetListFavorite(currentLevel, (c) =>
                {
                    if (c.Count > 0)
                    {
                        coursePool.Add(favoriteCategoryId, c);
                        courseTableController.Refresh(c, favoriteCategoryId);
                    }
                    else
                    {
                        coursePool.Add(favoriteCategoryId, null);
                        courseTableController.Refresh(emptyMessage, favoriteCategoryId);
                    }
                }, () => { });
            }

            // update current category pool
            int itemIndex = -1;
            if(coursePool.ContainsKey(currentCategoryId))
                itemIndex = coursePool[currentCategoryId].FindIndex(c => c.id == updatedCourse.id);
            if (itemIndex > -1)
            {
                CourseData newCourseData = coursePool[currentCategoryId][itemIndex];
                newCourseData.is_favorite = updatedCourse.is_favorite;
                coursePool[currentCategoryId].RemoveAt(itemIndex);
                coursePool[currentCategoryId].Insert(itemIndex, newCourseData);
            }
        }
    }
}
