﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HowToUseDetailPage : Page {
    public enum HowTo
    {
        FindCourse,
        JoinCourse,
        Subscribe,
        BecomeInstructor,
        Pay,
        Chat,
        ResignCourseIOS,
        ResignCourseAndroid,
    }
    [SerializeField]
    ScrollRect scroll;

    [SerializeField]
    GameObject[] HowToPages;


    private HowTo currentHowToPage;
    public override void Show()
    {
        base.Show();

        string title = "";
        switch (currentHowToPage)
        {
            case HowTo.FindCourse:
                title = "教室の探し方";
                break;
            case HowTo.JoinCourse:
                title = "レッスンへの入会方法";
                break;
            case HowTo.Subscribe:
                title = "レッスンを受ける";
                break;
            case HowTo.BecomeInstructor:
                title = "先生になるには";
                break;
            case HowTo.Pay:
                title = "決済方法";
                break;
            case HowTo.Chat:
                title = "メッセージのやり取り";
                break;
            case HowTo.ResignCourseIOS:
                title = "レッスンの退会方法";
                break;
            case HowTo.ResignCourseAndroid:
                title = "レッスンの退会方法";
                break;
            default:
                title = "先生になるには";
                break;
        }

        foreach (var page in HowToPages)
        {
            page.SetActive(false);
        }
        HowToPages[(int)currentHowToPage].SetActive(true);
        scroll.verticalNormalizedPosition = 1;
        HeaderView.ShowSub(title, HeaderSubView.ButtonType.close, MenuType);
    }

    public void SetHowToPage(HowTo howTo)
    {
        currentHowToPage = howTo;
    }
}
