using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CourseDetailPage : Page
{
    // course text
    [SerializeField]
    Text explaination, necessity, fee;

    // instructor text
    [SerializeField]
    Text instructorName, instructorTitle, instructorExplaination;

    [SerializeField]
    Image instructorAvatar;

    [SerializeField]
    Sprite defaultInstructorAvatar, defaultCoourseImage;

    [SerializeField]
    Image courseImage;

    [SerializeField]
    VideoPlayer courseVideo;

    [SerializeField]
    private Button favoriteButton;

    [SerializeField]
    private Text favoriteText;

    [SerializeField]
    private Color favoriteButtonNormalColor, favoriteButtonDisableColor, favoriteTextNormalColor, favoriteTextDisableColor;

    [SerializeField]
    private GameObject waitingCourseImage;

    CourseData currentCourse;
    CourseNodeView currentCourseNode;
    ScrollRect scroller;

    static Stack<CourseData> courseDataStack = new Stack<CourseData>();

    public override void Show()
    {
        base.Show();
        currentCourse = courseDataStack.Pop();
        FillData();
    }

    public static void ClearStack()
    {
        courseDataStack.Clear();
    }

    public void ShowSubscribePage()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        courseDataStack.Push(currentCourse);
        SubscribePage page = (SubscribePage)PageManager.Show(PageType.Subscribe);
        page.Setup(currentCourse, new PaymentData());
    }

    public void ShowChatPage()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        // open chat with instructor
        Debug.Log("Create new chat group to " + currentCourse.instructor.user_id);
        ChatAPI.NewChatGroup(currentCourse.instructor.user_id,true, ProcessNewChatGroup, OnNewChatGroupError);
    }

    void ProcessNewChatGroup(ChatGroupData data)
    {
        courseDataStack.Push(currentCourse);
        ChatPage chatPage = (ChatPage)PageManager.Show(PageType.Chat,MenuType.CommonUser);
        chatPage.Setup(data);
    }

    void OnNewChatGroupError()
    {
        DialogView.Show("チャットの読み込みに失敗しました。");
    }

    public void ShowInstructorCourseListPage()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        courseDataStack.Push(currentCourse);
        InstructorCourseListPage page = (InstructorCourseListPage)PageManager.Instantiate(PageType.InstructorCourseList);
        page.SetUp(currentCourse.instructor);
        PageManager.Show(PageType.InstructorCourseList);
    }

    public void SetCourseNode(CourseNodeView courseNode)
    {
        currentCourseNode = courseNode;
    }

    public void Setup(CourseData courseData)
    {
        courseDataStack.Push(courseData);
    }

    void FillData()
    {
        HeaderView.ShowSub(currentCourse.title, HeaderSubView.ButtonType.arrow, MenuType);
        explaination.text = currentCourse.explanation;
        necessity.text = currentCourse.necessity;
        fee.text = string.Format("隔週更新\n月々 ¥{0:0#,0}", currentCourse.course_fee.fee_yen);

        instructorName.text = currentCourse.instructor.name;
        instructorTitle.text = currentCourse.instructor.title;
        instructorExplaination.text = currentCourse.instructor.explanation;

        InstructorProfileImageLoader.GetImage(currentCourse.instructor.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
        {
            if (sprite != null)
            {
                instructorAvatar.sprite = sprite;
            }
            else
            {
                instructorAvatar.sprite = defaultInstructorAvatar;
            }
        });

        waitingCourseImage.SetActive(true);
        CourseImageLoader.GetImage(currentCourse.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
        {
            waitingCourseImage.SetActive(false);
            if (sprite != null)
                courseImage.sprite = sprite;
            else
                courseImage.sprite = defaultCoourseImage;
        });

        VideoThumbLoader.GetImage(currentCourse.video_id.ToString(), (Sprite sprite) =>
        {
            if (sprite != null)
            {
                courseVideo.SetVideoThumb(sprite);
            }
        });

        courseVideo.SetHtmlUrl(Server.MakeUrl("videos/" + currentCourse.video_id + "/html", true));

        ChangeFavoriteButton(currentCourse.is_favorite);
        ResetScroller();
    }


    // On Mark Favoris course button 
    public void OnMarkFavoriteCourse()
    {
        // check authentication
        if (!Authenticator.Validate())
        {
            DialogView.Show("ユーザー登録が必要です。登録しますか？", () =>
            {
                PageManager.Show(PageType.RegisterUser);
            },() =>
            {
                return;
            });
        }
        else
        {
            CourseAPI.AddFavorite(currentCourse.id, UpdateFavoriteSuccess, UpdateFavoriteError);
        }
    }

    private void UpdateFavoriteError()
    {
        DialogView.Show("お気に入り登録に失敗しました");
    }

    private void UpdateFavoriteSuccess(bool obj)
    {
        DialogView.Show("お気に入り登録しました");

        // update course node on the list
        currentCourse.is_favorite = true;
        ChangeFavoriteButton(currentCourse.is_favorite);
        currentCourseNode.UpdateView(currentCourse);

        //update CourseListPage
        CourseListPage page = (CourseListPage)PageManager.GetActivedPage(PageType.CourseList);
        page.RefreshCategory(currentCourse);
    }

    private void ChangeFavoriteButton(bool isFavorited)
    {

        favoriteButton.interactable = !isFavorited;
        if (isFavorited)
        {
            favoriteButton.image.color = favoriteButtonDisableColor;
            favoriteText.color = favoriteTextDisableColor;
        }
        else
        {
            favoriteButton.image.color = favoriteButtonNormalColor;
            favoriteText.color = favoriteTextNormalColor;
        }
    }

    void ResetScroller()
    {
        if (scroller == null) scroller = gameObject.GetComponent<ScrollRect>();
        if (scroller != null) scroller.verticalNormalizedPosition = 1;
    }

    public override void Hide()
    {
        if (courseVideo != null)
            courseVideo.Dispose();

        base.Hide();
    }
}
