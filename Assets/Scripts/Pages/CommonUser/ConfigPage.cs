﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfigPage : Page
{
    [SerializeField]
    Image profileImage;

    [SerializeField]
    Image backgroundImage;

    [SerializeField]
    Text name;

    [SerializeField]
    Text registerInstructorText;

    Sprite defaultProfileSprite;
    Sprite defaultBackgroundSprite;

    void Awake()
    {
        defaultProfileSprite = profileImage.sprite;
        defaultBackgroundSprite = backgroundImage.sprite;
    }

    public override void Show (MenuType menuType = MenuType.None)
    {
        base.Show (menuType);
        HeaderView.ShowMenu ("設定", MenuType);

        ProfileImageLoader.GetImage (Session.LoggedInUser.id.ToString (), ImageSizes.detail, (Sprite sprite) => {
            if (sprite == null) {
                profileImage.sprite = defaultProfileSprite;
            } else {
                profileImage.sprite = sprite;
            }
        });

        BackgroundImageLoader.GetImage (Session.LoggedInUser.id.ToString (), (Sprite sprite) => {
            if (sprite == null) {
                backgroundImage.sprite = defaultBackgroundSprite;
            } else {
                backgroundImage.sprite = sprite;
            }
        });

        name.text = Session.LoggedInUser.name;

        if (Session.IsInstructor())
        {
            registerInstructorText.text = "先生メニュー";
        }
        else
        {
            registerInstructorText.text = "先生登録";
        }
    }

    public void ShowHowToUsePage()
    {
        PageManager.Show(PageType.HowToUse);
    }

    public void ShowProfileEditPage ()
    {
        PageManager.Show (PageType.ProfileEdit);
    }

    public void ShowInstructorRegisterPage ()
    {     
        if (Session.IsInstructor())
        {
            HeaderView.SwitchUserState();
        }
        else
        {
            string prefsKey = ApplicationSystem.CONST.KEY_IS_FIRST_REGISTER_INSTRUCTOR + Session.LoggedInUser.id;
            if (ApplicationSystem.IsDebugTutorial() ||
                !PlayerPrefs.HasKey(prefsKey) ||
                PlayerPrefs.GetInt(prefsKey) != 1)
            {
                TutorialModal.Show(TutorialModal.TutorialType.instructor, () =>
                {
                    var _page = (InstructorProfileEditPage)PageManager.Show(PageType.InstructorProfileEdit);
                    _page.Setup(InstructorProfileEditPage.PageType.commonUser);
                });
                PlayerPrefs.SetInt(prefsKey, 1);
                PlayerPrefs.Save();

                return;
            }
       
            var page = (InstructorProfileEditPage)PageManager.Show(PageType.InstructorProfileEdit);
            page.Setup(InstructorProfileEditPage.PageType.commonUser);
        }
    }

    public void ShowQueryPage()
    {
        PageManager.Show(PageType.Query, MenuType.CommonUser);
    }

    public void ShowSettingPage()
    {
        PageManager.Show(PageType.Setting, MenuType.CommonUser);
    }
}
