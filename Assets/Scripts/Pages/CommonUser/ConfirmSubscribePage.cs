﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ConfirmSubscribePage : Page
{
    [SerializeField]
    Text message;

    CourseData currentCourse;

    public override void Show ()
    {
        base.Show ();
        HeaderView.SetIconButtonEvent(() =>
        {
            GotoCourseDetailPage();
        });
        HeaderView.ShowSub ("入会手続き完了", HeaderSubView.ButtonType.close, MenuType);
    }

    public override void Hide()
    {
        base.Hide();

        HeaderView.ResetIconButtonEvent();
    }
    public void Setup(CourseData courseData)
    {
        message.text = courseData.title + "\nの入会が完了しました。";

        currentCourse = courseData;
    }

    public void GotoCourseDetailPage()
    {
        LoadingView.Show();

        // call api to get latest lesson
        CourseAPI.GetLatestLessonDetail(currentCourse.id, ShowLessonDetailPage, OnGetLessonDetailError);
    }

    public void ShowLessonDetailPage(LessonData lessonDetail)
    {
        LoadingView.Hide();

        // set header text = course title
        lessonDetail.title = currentCourse.title;

        // insert page history
        PageManager.ResetPageHistory();
        Page myCoursePage = PageManager.Instantiate(PageType.MyCourse);
        myCoursePage.Hide();
        PageManager.AddPageHistory(myCoursePage);

        LessonDetailPage page = (LessonDetailPage)PageManager.Show(PageType.LessonDetailPage);
        page.Setup(lessonDetail);
    }

    public void OnGetLessonDetailError()
    {
        LoadingView.Hide();

        // users can show lesson detail page to unsubscribe, so we still show page with a empty lesson
        LessonData emptyLesson = new LessonData();
        emptyLesson.instructor = currentCourse.instructor;
        emptyLesson.title = currentCourse.title;

        // insert page history
        PageManager.ResetPageHistory();
        Page myCoursePage = PageManager.Instantiate(PageType.MyCourse);
        myCoursePage.Hide();
        PageManager.AddPageHistory(myCoursePage);

        LessonDetailPage page = (LessonDetailPage)PageManager.Show(PageType.LessonDetailPage);
        page.Setup(emptyLesson);
    }
}
