﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SubscribePage : Page
{
    [SerializeField]
    Image courseImage;

    [SerializeField]
    Text courseTitle;

    [SerializeField]
    Text courseFee;

    [SerializeField]
    Text paymentAmount;

    CourseData course;

    public override void Show()
    {
        base.Show();

        HeaderView.ShowSub("入会手続き", HeaderSubView.ButtonType.close, MenuType);
    }

    public void Setup(CourseData data, PaymentData pay)
    {
        course = data;

        //image

        //title
        courseTitle.text = data.title;

        //fee
        courseFee.text = "月々¥" + data.course_fee.fee_yen;
        paymentAmount.text = "月々¥" + data.course_fee.fee_yen;

        //TODO setup for payment data
    }

    public void ConfirmSubscribe()
    {

        Debug.Log("Subscribe course " + course.id);
        LoadingView.Show();
        AppPurchase.Instance.PurchaseAppProduct(course.course_fee.fee_yen, OnPurchaseSuccess, OnGetAppProductError, OnConnectPurchaseServiceFailed, OnPurchaseFailed);
    }

    void OnGetAppProductError(int errorCode)
    {
        LoadingView.Hide();
        DialogView.Show("商品情報が取得できませんでした");
    }

    void OnConnectPurchaseServiceFailed()
    {
        LoadingView.Hide();
        DialogView.Show("ストアに接続できません");
    }

    void OnPurchaseSuccess(UM_PurchaseResult result,AppProductData appProductData, int osType)
    {
        CourseAPI.Subscribe(course, result.TransactionId,"visa", appProductData.id.ToString(), osType, ShowConfirmSubscribePage, ShowErrorDialog);
    }

    void OnPurchaseFailed()
    {
        LoadingView.Hide();
        DialogView.Show("購読に失敗しました");
    }

    void ShowConfirmSubscribePage()
    {
        LoadingView.Hide();
        ConfirmSubscribePage page = (ConfirmSubscribePage)PageManager.Show(PageType.ConfirmSubscribe);
        page.Setup(course);
    }

    void ShowErrorDialog()
    {
        LoadingView.Hide();
        DialogView.Show("入会に失敗しました。");
    }
}
