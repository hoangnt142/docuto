using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InstructorCourseListPage : Page
{
    [SerializeField]
    GameObject courseNode, theList, theListLoading;

    [SerializeField]
    ScrollRect scroller;

    [SerializeField]
    VerticalLayoutGroup layoutGroup;
    
    // instructor text
    [SerializeField]
     Text ins_name, ins_title, ins_explaination;

    [SerializeField]
    Image instructorAvatar;


    InstructorData instructorData;

    public override void Show ()
    {
        base.Show ();
        if (!instructorData.Equals(default(InstructorData)))
        {
            HeaderView.ShowSub(instructorData.name + "先生のレッスン一覧", HeaderSubView.ButtonType.arrow, MenuType);
            SetupInstructorData();
            LoadData();
        }
    }

    public void SetUp(InstructorData data)
    {
        instructorData = data;
    }

    private void LoadData()
    {
        ShowListLoading(true);
        InstructorAPI.GetCourses(instructorData.id, true, OnProcessListCourse, OnFailCallback);
    }

    private void OnProcessListCourse(List<CourseData> listCourse)
    {
        CreateCourseList(listCourse);
        ShowListLoading(false);
    }

    private void OnFailCallback()
    {
        ShowListLoading(false);
        DialogView.Show("読み込みに失敗しました");
    }

    public void CreateCourseList(List<CourseData> courseData)
    {
        StartCoroutine(DelayUi());
        //scroller.verticalNormalizedPosition = 1;

        foreach(Transform child in theList.transform)
        {
            Destroy(child.gameObject);    
        }

        foreach (var course in courseData)
        {
            GameObject node = Instantiate(courseNode, theList.transform);
            node.GetComponent<CourseNodeView>().UpdateView(course);
        }
    }

    IEnumerator DelayUi()
    {
        yield return null;
        layoutGroup.enabled = false;
        yield return null;
        layoutGroup.enabled = true;
    }

    public void OnChatBtnClick()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        // open chat with instructor
        ChatAPI.NewChatGroup(instructorData.user_id,true, ProcessNewChatGroup, OnNewChatGroupError);
    }

    void ProcessNewChatGroup(ChatGroupData data)
    {
        ChatPage chatPage = (ChatPage)PageManager.Show(PageType.Chat,MenuType.CommonUser);
        chatPage.Setup(data);
    }

    void OnNewChatGroupError()
    {
        DialogView.Show("チャットの読み込みに失敗しました。");
    }

    void ShowListLoading(bool isShow)
    {
        theListLoading.SetActive(isShow);
        theList.SetActive(!isShow);
    }

    void SetupInstructorData()
    {
        InstructorProfileImageLoader.GetImage(instructorData.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
        {
            if (sprite != null)
            {
                instructorAvatar.sprite = sprite;
            }
        });

        ins_name.text = instructorData.name;
        ins_title.text = instructorData.title;
        ins_explaination.text = instructorData.explanation;
    }
}
