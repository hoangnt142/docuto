﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class HowToUsePage : Page {

    [SerializeField]
    Button[] buttons;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("BeneFitnessの使い方", HeaderSubView.ButtonType.arrow, MenuType);

        // this trick to make button reset button state
        foreach (var item in buttons)
        {
            item.enabled = false;
            item.enabled = true;
            item.image.color = item.colors.normalColor;
        }
    }

    public void ShowHowToFindPage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);
        page.SetHowToPage(HowToUseDetailPage.HowTo.FindCourse);
        PageManager.Show(PageType.HowToUseDetail);
    }

    public void ShowHowToJoinPage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);
        page.SetHowToPage(HowToUseDetailPage.HowTo.JoinCourse);
        PageManager.Show(PageType.HowToUseDetail);
    }

    public void ShowHowToSubscibrePage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);
        page.SetHowToPage(HowToUseDetailPage.HowTo.Subscribe);
        PageManager.Show(PageType.HowToUseDetail);
    }

    public void ShowHowToBecomeInstructorPage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);
        page.SetHowToPage(HowToUseDetailPage.HowTo.BecomeInstructor);
        PageManager.Show(PageType.HowToUseDetail);
    }

    public void ShowHowToPayPage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);
        page.SetHowToPage(HowToUseDetailPage.HowTo.Pay);
        PageManager.Show(PageType.HowToUseDetail);
    }

    public void ShowHowToChatPage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);
        page.SetHowToPage(HowToUseDetailPage.HowTo.Chat);
        PageManager.Show(PageType.HowToUseDetail);
    }

    public void ShowHowToRedesignPage()
    {
        HowToUseDetailPage page = (HowToUseDetailPage)PageManager.Instantiate(PageType.HowToUseDetail);

#if UNITY_IOS
        page.SetHowToPage(HowToUseDetailPage.HowTo.ResignCourseIOS);
#elif UNITY_ANDROID
        page.SetHowToPage(HowToUseDetailPage.HowTo.ResignCourseAndroid);
#endif
        PageManager.Show(PageType.HowToUseDetail);
    }
}
