﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SettingPage : Page
{
    [SerializeField]
    Slider pushNotification;

    [SerializeField]
    Image sliderBackground;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("設定・その他", HeaderSubView.ButtonType.arrow, MenuType);
    }

    public void SetPushNotification()
    {
        float val = pushNotification.value;
        if (val < 0.5f)
        {
            sliderBackground.color = new Color32(184, 184, 184, 255);
        }
        else
        {
            sliderBackground.color = new Color32(70, 204, 87, 255);
        }
    }

    public void LogOut()
    {
        LoadingView.Show();
        //Destroy clone page
        PageManager.Clean();
        LoginAPI.Logout(() =>
        {
            LoadingView.Hide();
            DialogView.Show("ログアウトしました。",()=> {
                PageManager.Show(PageType.Login);
            });
        });
    }
    
    public void ShowPage()
    {
        PageManager.Show(PageType.Agreement);
    }
}