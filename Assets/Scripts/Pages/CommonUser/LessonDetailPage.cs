﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LessonDetailPage : Page
{
    [SerializeField]
    Text updateTime;

    [SerializeField]
    Text explaination;

    [SerializeField]
    Text necessity;

    [SerializeField]
    Image instructorImage;

    [SerializeField]
    Text instructorName;

    [SerializeField]
    Text instructorTitle;

    [SerializeField]
    Text validTerm;

    [SerializeField]
    GameObject noLessonText, lessonContainer;

    [SerializeField]
    VideoPlayer videoPlayer;

    [SerializeField]
    Sprite defaultInstructorAvatar;

    private LessonData lessonData;
    public override void Show()
    {
        base.Show();

        if(!lessonData.Equals(default(LessonData)))
        {
            Setup(lessonData);
        }
    }

    public void ShowInstructorCourseListPage()
    {
        if (!Authenticator.Validate()) return;

        InstructorCourseListPage page = (InstructorCourseListPage)PageManager.Instantiate(PageType.InstructorCourseList);
        page.SetUp(lessonData.instructor);
        PageManager.Show(PageType.InstructorCourseList);
    }

    public void Setup(LessonData lessonDetail)
    {
        // show header title
        HeaderView.ShowSub(lessonDetail.title, HeaderSubView.ButtonType.arrow, MenuType);

        if (!lessonDetail.has_lesson)
        {
            noLessonText.SetActive(true);
            lessonContainer.SetActive(false);
        }
        else
        {
            noLessonText.SetActive(false);
            lessonContainer.SetActive(true);
        }

        lessonData = lessonDetail;
        explaination.text = lessonDetail.explanation;
        necessity.text = lessonDetail.necessity;

        DateTime updatedDate;
        if (!DateTime.TryParse(lessonDetail.updated_at, out updatedDate))
        {
            updatedDate = DateTime.Now;
        }
        updateTime.text = updatedDate.ToString("yyyy/MM/dd HH:mm") + "更新";

        instructorName.text = lessonDetail.instructor.name;
        instructorTitle.text = lessonDetail.instructor.title;
        InstructorProfileImageLoader.GetImage(lessonDetail.instructor.id.ToString(), ImageSizes.detail, (Sprite sprite) =>
        {
            if (sprite != null)
            {
                instructorImage.sprite = sprite;
            }
            else
            {
                instructorImage.sprite = defaultInstructorAvatar;
            }
        });

        if (lessonDetail.has_lesson)
        {
            VideoThumbLoader.GetImage(lessonDetail.video_id.ToString(), (Sprite sprite) =>
            {
                if (sprite != null)
                {
                    videoPlayer.SetVideoThumb(sprite);
                }
                else
                {
                    videoPlayer.SetVideoThumb(defaultInstructorAvatar);
                }
            });
            videoPlayer.SetHtmlUrl(Server.MakeUrl("videos/" + lessonDetail.video_id + "/html", true));
        }

    }

    public void OnChatBtnClick()
    {
        // check authentication
        if (!Authenticator.Validate()) return;

        // open chat with instructor
        LoadingView.Show();
        ChatAPI.NewChatGroup(lessonData.instructor.user_id, true, ProcessNewChatGroup, OnNewChatGroupError);
    }

    void ProcessNewChatGroup(ChatGroupData data)
    {
        LoadingView.Hide();
        ChatPage chatPage = (ChatPage)PageManager.Show(PageType.Chat, MenuType.CommonUser);
        chatPage.Setup(data);
    }

    void OnNewChatGroupError()
    {
        LoadingView.Hide();
        DialogView.Show("チャットの読み込みに失敗しました。");
    }

    public void OnUnSubscribeClick()
    {
        PageManager.Show(PageType.LeaveCourse);
    }
}
