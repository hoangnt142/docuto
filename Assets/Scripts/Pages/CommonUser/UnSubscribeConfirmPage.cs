﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UnSubscribeConfirmPage : Page
{
    [SerializeField]
    Text titleText;

    [SerializeField]
    Text feeText;

    [SerializeField]
    Text reasonText;

    public override void Show()
    {
        base.Show();
        HeaderView.ShowSub("退会手続き", HeaderSubView.ButtonType.close, MenuType);
    }

    public void Setup(CourseData data)
    {
        titleText.text = data.title;
        feeText.text = "月々¥" + data.course_fee.fee_yen;

        reasonText.text = data.unsubsribe_reason;
    }

}
