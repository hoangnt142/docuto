﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileEditPage : Page
{
    static int BOD_RANGE_YEAR = 100;

    [SerializeField]
    Image backgroundImg;

    [SerializeField]
    Image profileImage;

    [SerializeField]
    InputField nameText;

    [SerializeField]
    ToggleGroup gender;

    [SerializeField]
    Toggle genderMale;

    [SerializeField]
    Toggle genderFemale;

    [SerializeField]
    Toggle genderOther;

    [SerializeField]
    Dropdown DOBYear;

    [SerializeField]
    Dropdown DOBMonth;

    [SerializeField]
    Dropdown DOBDay;

    [SerializeField]
    InputField passwordText;

    [SerializeField]
    InputField emailText;

    Sprite defaultProfileImageSprite;
    Sprite defaultBackgroundImageSprite;

    private Sprite startProfileImageSprite;
    private Sprite startBackgroundImageSprite;
    private int startYearValue;
    private int startMonthValue;
    private int startDayValue;
    int sexType = -1;
    DateTime dob;
    void Awake ()
    {
        defaultProfileImageSprite = profileImage.sprite;
        defaultBackgroundImageSprite = backgroundImg.sprite;
    }

    void Finalize ()
    {
        profileImage.sprite = defaultProfileImageSprite;
        backgroundImg.sprite = defaultBackgroundImageSprite;
    }

    public override void Show ()
    {
        base.Show ();
        HeaderView.ShowSub ("プロフィール編集", HeaderSubView.ButtonType.arrow, MenuType);

        // setup BOD
        var now = DateTime.Now;
        var listYears = new List<Dropdown.OptionData>();
        var listMonths = new List<Dropdown.OptionData>();
        var listDays = new List<Dropdown.OptionData>();
        listYears.Add(new Dropdown.OptionData("年"));
        for (int i = now.Year; i >= now.Year - BOD_RANGE_YEAR; i--)
        {
            listYears.Add(new Dropdown.OptionData(i.ToString()));
        }
        listMonths.Add(new Dropdown.OptionData("月"));
        for (int i = 1; i <= 12; i++)
        {
            string itemText = i.ToString();
            if (i < 10) itemText = "0" + itemText;
            listMonths.Add(new Dropdown.OptionData(itemText));
        }
     
        listDays.Add(new Dropdown.OptionData("日"));

        DOBYear.onValueChanged.AddListener(delegate {
            OnYearChange();
        });
        DOBMonth.onValueChanged.AddListener(delegate {
            OnMonthChange();
        });

        DOBYear.ClearOptions();
        DOBYear.AddOptions(listYears);
        DOBMonth.ClearOptions();
        DOBMonth.AddOptions(listMonths);
        DOBDay.ClearOptions();
        DOBDay.AddOptions(listDays);

        DateTime birthday;
        if (Session.LoggedInUser.birthday != null && DateTime.TryParse(Session.LoggedInUser.birthday, out birthday))
        {
            string year = birthday.Year.ToString();
            string month = birthday.Month.ToString().PadLeft(2,'0');
            string day = birthday.Day.ToString().PadLeft(2, '0');
            int yearValue = GetDropDownValue(year, DOBYear);
            if (yearValue!= -1)
            {
                DOBYear.value = yearValue;
            }

            int monthValue = GetDropDownValue(month, DOBMonth);
            if (monthValue != -1)
            {
                DOBMonth.value = monthValue;
            }

            int dayValue = GetDropDownValue(day, DOBDay);
            if (dayValue != -1)
            {
                DOBDay.value = dayValue;
            }
        }
        else
        {
            DOBYear.value = 0;
            DOBMonth.value = 0;
            DOBDay.enabled = false;
            DOBDay.value = 0;
        }

        startYearValue = DOBYear.value;
        startMonthValue = DOBMonth.value;
        startDayValue = DOBDay.value;

        gender.SetAllTogglesOff();
        sexType = Session.LoggedInUser.sex;
        if (sexType == 0) genderMale.isOn = true;
        if (sexType == 1) genderFemale.isOn = true;
        if (sexType == 2) genderOther.isOn = true;

        nameText.text = Session.LoggedInUser.name;
        emailText.text = Session.LoggedInUser.email;
        passwordText.text = "";

        ProfileImageLoader.GetImage (Session.LoggedInUser.id.ToString(), ImageSizes.detail, (Sprite sprite) => {
            if (sprite == null) {
                profileImage.sprite = defaultProfileImageSprite;
            } else {
                profileImage.sprite = sprite;
            }
            startProfileImageSprite = profileImage.sprite;
        });

        BackgroundImageLoader.GetImage (Session.LoggedInUser.id.ToString(), (Sprite sprite) => {
            if (sprite == null) {
                backgroundImg.sprite = defaultBackgroundImageSprite;
            } else {
                backgroundImg.sprite = sprite;
            }
            startBackgroundImageSprite = backgroundImg.sprite;
        });
    }

    public bool DataChanged()
    {
        if (sexType != Session.LoggedInUser.sex
            || nameText.text != (Session.LoggedInUser.name??"")
            || emailText.text != (Session.LoggedInUser.email??"")
            || passwordText.text != "" 
            || startProfileImageSprite != profileImage.sprite
            || startBackgroundImageSprite != backgroundImg.sprite
            || startYearValue != DOBYear.value
            || startMonthValue != DOBMonth.value
            || startDayValue != DOBDay.value

            )
        {
            return true;
        }

        return false;
    }


    int GetDropDownValue(string text,Dropdown dropdown)
    {
        for (int i = 0; i < dropdown.options.Count; i++)
        {
            Dropdown.OptionData data = dropdown.options[i];
            if (data.text.Equals(text))
            {
                return i;
            }
        }
        return -1;
    }

    void OnYearChange()
    {
        if(DOBMonth.value > 0)
        {
            OnMonthChange();
        }
    }

    void OnMonthChange()
    {
        // do not select day
        if (DOBYear.value < 1 || DOBMonth.value < 1) return;

        var selectYear = int.Parse(DOBYear.options[DOBYear.value].text);
        var selectMonth = int.Parse(DOBMonth.options[DOBMonth.value].text);

        var dayOfMonth = DateTime.DaysInMonth(selectYear, selectMonth);
        var listDays = new List<Dropdown.OptionData>();
        listDays.Add(new Dropdown.OptionData("日"));
        for (int i = 1; i <= dayOfMonth; i++)
        {
            string itemText = i.ToString();
            if (i < 10) itemText = "0" + itemText;
            listDays.Add(new Dropdown.OptionData(itemText));
        }

        DOBDay.ClearOptions();
        DOBDay.AddOptions(listDays);
        DOBDay.enabled = true;
    }

    public override void Hide ()
    {
        base.Hide ();

        Finalize ();

    }

    public void SetSexType(int sex)
    {
        sexType = sex;
    }

    public void UpdateProfile ()
    {
        if(DOBYear.value == 0 || DOBMonth.value == 0 || DOBDay.value == 0)
        {
            DialogView.Show("生年月日を入力してください");
            return;
        }

        if((DOBYear.value + DOBMonth.value + DOBDay.value) > 0 && (DOBYear.value * DOBMonth.value * DOBDay.value) == 0)
        {
            DialogView.Show("生年月日を入力して下さい");
            return;
        }

        string userName = nameText.text;
        string email = emailText.text;
        string password = passwordText.text;

        if(string.IsNullOrEmpty(userName) && string.IsNullOrEmpty(email))
        {
            DialogView.Show("名前とメールを入力してください。");
            return;
        }

        if(string.IsNullOrEmpty(userName))
        {
            DialogView.Show("名前を入力してください。");
            return;
        }

        if(string.IsNullOrEmpty(email))
        {
            DialogView.Show("メールを入力してください。");
            return;
        }

        byte [] profileImageBytes = null;
        if (profileImage.sprite != defaultProfileImageSprite) {
            profileImageBytes = profileImage.sprite.texture.EncodeToPNG ();
        }

        byte [] backgroundImageBytes = null;
        if (backgroundImg.sprite != defaultBackgroundImageSprite) {
            backgroundImageBytes = backgroundImg.sprite.texture.EncodeToPNG ();
        }

        dob = new DateTime(0);
        if(DOBYear.value > 0 && DOBMonth.value > 0 && DOBDay.value > 0)
            dob = new DateTime(int.Parse(DOBYear.options[DOBYear.value].text), DOBMonth.value, DOBDay.value);


        LoadingView.Show ();
        ProfileAPI.UpdateProfile (profileImageBytes, backgroundImageBytes, userName, sexType, dob, email, password, OnGetAPIResponse);

    }

    void OnGetAPIResponse (bool isSuccess)
    {
        LoadingView.Hide ();
        if (isSuccess) {
            passwordText.text = "";
            Session.LoggedInUser.name = nameText.text;
            Session.LoggedInUser.email = emailText.text;
            Session.LoggedInUser.sex = sexType;
            string dobString = null;
            if (dob.Ticks > 0) dobString = dob.ToString("yyyy-MM-dd");
            Session.LoggedInUser.birthday = dobString;

            if (profileImage.sprite != defaultProfileImageSprite) {
                ProfileImageLoader.SaveSprite (Session.LoggedInUser.id.ToString (), profileImage.sprite);
            }
            if (backgroundImg.sprite != defaultBackgroundImageSprite) {
                BackgroundImageLoader.SaveSprite (Session.LoggedInUser.id.ToString (), backgroundImg.sprite);
            }

            startYearValue = DOBYear.value;
            startMonthValue = DOBMonth.value;
            startDayValue = DOBDay.value;
            startProfileImageSprite = profileImage.sprite;
            startBackgroundImageSprite = backgroundImg.sprite;

            DialogView.Show("更新しました");
        }
        else
        {
            DialogView.Show("プロフィールの更新に失敗しました。", Show);
        }
    }

    public void SelectProfileImage ()
    {
#if UNITY_EDITOR
        var tex = EditorIO.GetPngTextureFromStrage ();
        CropImageModal.ShowCropImage (tex, 256);
        CropImageModal.onCrop = OnCropProfileImage;
#elif UNITY_IOS
        IOSCamera.Instance.PickImage(ISN_ImageSource.Library);
        IOSCamera.OnImagePicked += OnPickedProfileImage;
#elif UNITY_ANDROID
        AndroidCamera.Instance.OnImagePicked += OnPickedProfileImage;
        AndroidCamera.Instance.GetImageFromGallery();
#endif
    }

    public void SelectBackgroundImage ()
    {
#if UNITY_EDITOR
        var tex = EditorIO.GetPngTextureFromStrage ();
        CropImageModal.ShowCropImage (tex, backgroundImg.rectTransform.rect.size);
        CropImageModal.onCrop = OnCropBackgroundImage;
#elif UNITY_IOS
        IOSCamera.Instance.PickImage(ISN_ImageSource.Library);
        IOSCamera.OnImagePicked += OnPickedBackgroundImage;
#elif UNITY_ANDROID
        AndroidCamera.Instance.OnImagePicked += OnPickedBackgroundImage;
        AndroidCamera.Instance.GetImageFromGallery();
#endif


    }

    void OnCropProfileImage (Sprite croppedSprite)
    {
        Debug.Log ("on crop");
        profileImage.gameObject.SetActive (true);
        profileImage.sprite = croppedSprite;
    }

    void OnCropBackgroundImage (Sprite croppedSprite)
    {
        backgroundImg.sprite = croppedSprite;
    }

#if UNITY_IOS
    void OnPickedProfileImage (IOSImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, 256);
            CropImageModal.onCrop = OnCropProfileImage;
        }
        IOSCamera.OnImagePicked -= OnPickedProfileImage;
    }

    void OnPickedBackgroundImage (IOSImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, backgroundImg.rectTransform.rect.size);
            CropImageModal.onCrop = OnCropBackgroundImage;
        }
        IOSCamera.OnImagePicked -= OnPickedBackgroundImage;
    }
#endif

#if UNITY_ANDROID

	void OnPickedProfileImage (AndroidImagePickResult result)
    {
        if (result.IsSucceeded) {
            CropImageModal.ShowCropImage (result.Image, 256);
            CropImageModal.onCrop = OnCropProfileImage;
        }

        AndroidCamera.Instance.OnImagePicked -= OnPickedProfileImage;
    }

    void OnPickedBackgroundImage (AndroidImagePickResult result)
    {
	    if (result.IsSucceeded) {
		    CropImageModal.ShowCropImage (result.Image, backgroundImg.rectTransform.rect.size);
		    CropImageModal.onCrop = OnCropBackgroundImage;
	    }
	    AndroidCamera.Instance.OnImagePicked -= OnPickedBackgroundImage;
    }
#endif

}
