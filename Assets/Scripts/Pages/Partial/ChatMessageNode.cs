﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatMessageNode : MonoBehaviour
{
    [SerializeField]
    GameObject chatText;

    [SerializeField]
    Text chatTime;

    [SerializeField]
    Image avartar;

    [SerializeField]
    GameObject background;

    private ChatData data;

    public void SetData(ChatData data)
    {
        this.data = data;
    }

    // Use this for initialization
    void Start()
    {
        //Set conversation background image sortorder here to make the message can be show
        //Do not know why the setting property on prefab is not work
        background.GetComponent<Canvas>().overrideSorting = true;
        background.GetComponent<Canvas>().sortingOrder = -1;

        // main message
        if(chatText.GetComponent<TextMeshProUGUI>() != null)
        {
            chatText.GetComponent<TextMeshProUGUI>().text = data.text;
        }
        chatTime.text = data.time;

        // avatar will be set for sender at the moment, but not for 'me'
        if (avartar != null)
        {
            // set avatar image source
            if(data.user.instructor == null)
                ProfileImageLoader.GetImage(data.user.id.ToString(), ImageSizes.icon2x, (Sprite sprite) =>
                {
                    if (sprite != null)
                    {
                        avartar.sprite = sprite;
                    }
                });
            else
                InstructorProfileImageLoader.GetImage(data.user.instructor.id.ToString(), ImageSizes.icon2x, (Sprite sprite) =>
                {
                    if (sprite != null)
                    {
                        avartar.sprite = sprite;
                    }
                });
        }

        // time
        DateTime time;
        if (!DateTime.TryParse(data.time, out time))
        {
            time = DateTime.Now;
        }

        chatTime.text = time.ToString("yyyy/MM/dd HH:mm");
    }

    public void OpenInstructorCourseListPage()
    {
        InstructorCourseListPage page = (InstructorCourseListPage)PageManager.Instantiate(PageType.InstructorCourseList);
        page.SetUp(data.user.instructor);
        PageManager.Show(PageType.InstructorCourseList);
    }
}
