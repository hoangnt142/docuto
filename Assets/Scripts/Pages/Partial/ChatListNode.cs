﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ChatListNode : MonoBehaviour
{

    [SerializeField]
    Text lastChatTime;

    [SerializeField]
    TextMeshProUGUI lastChatText;

    [SerializeField]
    Text senderName;

    [SerializeField]
    Image senderImage;

    [SerializeField]
    GameObject noReadNumberObject;

    [SerializeField]
    Text noReadNumber;

    public bool IsInInstructorPage
    {
        get;
        set;
    }

    private ChatGroupData data;

    public void SetData(ChatGroupData data)
    {
        this.data = data;
    }

    public ChatGroupData GetData()
    {
        return data;
    }

    void Start()
    {
        //datetime
        lastChatTime.text = data.last_chat.time;

        //chat text
        int maxMessageLength = 18;
        string lastMessage = data.last_chat.text;
        if (lastMessage.Length > maxMessageLength) lastMessage = lastMessage.Substring(0, maxMessageLength) + "...";
        lastChatText.text = lastMessage;

        //new conversation
        if(data.unread_num <= 0)
        {
            noReadNumberObject.SetActive(false);
        }
        else
        {
            noReadNumberObject.SetActive(true);
            noReadNumber.text = data.unread_num.ToString();
        }

        //set image
        if (!IsInInstructorPage)
        {
            //sender name
            senderName.text = data.to_user.instructor.name;

            InstructorProfileImageLoader.GetImage(data.to_user.instructor.id.ToString(), ImageSizes.icon2x, (Sprite sprite) =>
            {
                if(sprite != null)
                {
                    senderImage.sprite = sprite;
                }
            });
        }
        else
        {
            //sender name
            senderName.text = data.to_user.name;

            ProfileImageLoader.GetImage(data.to_user.id.ToString(), ImageSizes.icon2x, (Sprite sprite) =>
            {
                if (sprite != null)
                {
                    senderImage.sprite = sprite;
                }
            });
        }
    }
}
