using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class NoticeNode : MonoBehaviour
{
    [SerializeField]
    Image background;

    [SerializeField]
    Text dateTime;

    [SerializeField]
    Text title;

    [SerializeField]
    Image avatar;

    [SerializeField]
    Sprite iconNotice;

    [SerializeField]
    Font boldFont, regularFont;

    [SerializeField]
    Color32 userBackgroundColor, instructorBackgroundColor;

    private NoticeData data;
    private Mode currentMode;

    public enum Mode
    {
        Read,
        User,
        Instructor
    }

    public void SetData (NoticeData data)
    {
        this.data = data;
    }

    public NoticeData GetData ()
    {
        return data;
    }

    public void SetMode (Mode mode)
    {
        currentMode = mode;
    }

    void Start ()
    {
        //datetime
        DateTime updatedTime = DateTime.Now;

        if (data.notice_type == 1) {
            if (!DateTime.TryParse (data.publish_at, out updatedTime)) {
                updatedTime = DateTime.Now;
            }

            //set image here...
            avatar.sprite = iconNotice;
            title.text = data.title;
        }

        if (data.notice_type == 2) {
            if (!DateTime.TryParse (data.chat_time, out updatedTime)) {
                updatedTime = DateTime.Now;
            }

            //title
            string chatMessage = data.chat_name;
            if (PageManager.CurrentPage.IsInstructorPage) {
                chatMessage += "さんからメッセージが届きました。";

                // get image
                ProfileImageLoader.GetImage (data.chat_user_id.ToString (), ImageSizes.icon2x, (Sprite sprite) => {
                    if (sprite != null) {
                        avatar.sprite = sprite;
                    }
                });
            } else {
                chatMessage += "先生からメッセージが届きました。";

                // get image
                InstructorProfileImageLoader.GetImage (data.chat_instructor_id.ToString (), ImageSizes.icon2x, (Sprite sprite) => {
                    if (sprite != null) {
                        avatar.sprite = sprite;
                    }
                });
            }
            title.text = chatMessage;
        }

        string format = "yyyy/MM/dd HH:mm";
        // only show time if it is today notices
        if (System.DateTime.Now.ToString ("yyyyMMdd") == updatedTime.ToString ("yyyyMMdd")) format = "HH:mm";
        dateTime.text = updatedTime.ToString (format);


        if (data.is_read) {
            int fontSize = title.fontSize;
            title.font = regularFont;
            title.fontSize = fontSize;
            currentMode = Mode.Read;
        } else {
            switch (currentMode) {
            case Mode.Instructor:
                background.color = instructorBackgroundColor;
                break;

            case Mode.User:
                background.color = userBackgroundColor;
                break;

            case Mode.Read:
                background.color = Color.white;
                break;

            // default
            default:
                background.color = Color.white;
                break;
            }

            int fontSize = title.fontSize;
            title.font = boldFont;
            title.fontSize = fontSize;
        }
    }
}
