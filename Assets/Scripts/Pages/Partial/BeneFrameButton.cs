﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BeneFrameButton : MonoBehaviour {
    [SerializeField]
    Image Image;

    [SerializeField]
    GameObject Frame;

    public void ShowSelectedFrame()
    {
        Frame.SetActive(true);
    }
    public void HideSelectedFrame()
    {
        Frame.SetActive(false);
    }

    public Sprite GetImage()
    {
        return Image.sprite;
    }
}
