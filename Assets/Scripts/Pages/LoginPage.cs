﻿﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class LoginPage : Page
{

    [SerializeField]
    InputField emailField;

    [SerializeField]
    InputField passwordField;

    private PageType defaultPage = PageType.CourseList;
    private bool isLoginWithFacebook = false;

    void Awake()
    {
        Authenticator.Init(this.OnLoggedInSuccess, this.OnLoggedInFail, this.OnLoggedOut);
    }

    public override void Show()
    {
        base.Show();

        HeaderView.ShowSub("ログイン", HeaderSubView.ButtonType.arrow, MenuType);

        // Clear PlayerPref one user back to Login page
        Session.Token = "";

        emailField.GetComponent<NativeEditBox>().text = "";
        passwordField.GetComponent<NativeEditBox>().text = "";

        if (PlayerPrefs.HasKey(ApplicationSystem.CONST.LOGGED_IN_EMAIL_KEY))
        {
            string userEmail = PlayerPrefs.GetString(ApplicationSystem.CONST.LOGGED_IN_EMAIL_KEY);
            if (!string.IsNullOrEmpty(userEmail))
            {
                emailField.GetComponent<NativeEditBox>().text = userEmail;
            }
        }
    }

    public void ShowCourseList()
    {
        PageManager.Show(PageType.CourseList);
    }

    public void LoginWithFacebook()
    {
        // Login with facebook
        // Facebook's plugin will be show, so no need show LoadingView
        isLoginWithFacebook = true;
        Authenticator.LoginWithFacebook();
    }

    public void Login()
    {
        isLoginWithFacebook = false;

        // validation
        if (string.IsNullOrEmpty(this.emailField.text) && string.IsNullOrEmpty(this.passwordField.text))
        {
            DialogView.Show("メールアドレスとパスワードを入力して下さい");
            return;
        }
        if (string.IsNullOrEmpty(this.emailField.text))
        {
            DialogView.Show("メールアドレスを入力して下さい ");
            return;
        }
        if (string.IsNullOrEmpty(this.passwordField.text))
        {
            DialogView.Show("パスワードを入力して下さい");
            return;
        }

        LoadingView.Show();
        Authenticator.CallLoginAPI(this.emailField.text, this.passwordField.text);
    }

    // call back functions
    public void OnLoggedInSuccess()
    {
        // facebook account login
        if (FB.IsLoggedIn)
        {
            LoadingView.Show();

            // get facebook's user information
            FacebookConnection.GetData(ProcessFacebooksProfileData, "first_name", "last_name", "email");
            // FacebookConnection.GetImage(ProcessFacebooksProfilePicture, "picture", "square", 128, 128);
        }
        // normal account login
        else
        {
            PlayerPrefs.SetString(ApplicationSystem.CONST.LOGGED_IN_EMAIL_KEY, emailField.text);
            LoadingView.Hide();
            PageManager.Show(defaultPage);
            PlayerPrefs.Save();
        }
    }

    public void OnLoggedInFail()
    {
        LoadingView.Hide();
        Debug.Log("login error");

        if (isLoginWithFacebook)
        {
            DialogView.Show("Facebookログインに失敗しました");
        }
        else
        {
            DialogView.Show("ログインに失敗しました。");
        }
    }

    public void OnLoggedOut()
    {
        LoadingView.Hide();

        PageManager.Show(PageType.Login);
    }


    private void ProcessFacebooksProfileData(IDictionary<string, object> data)
    {
        Debug.Log("Call process facebook data");
        if (data != null)
        {
            try
            {
                Session.LoggedInUser.fb_id = data["id"].ToString();
                Session.LoggedInUser.email = data["email"].ToString();
                Session.LoggedInUser.name = data["last_name"] + " " + data["first_name"];

                // Call register facebook account and login
                RegisterAPI.RegisterFBAccount(AccessToken.CurrentAccessToken.TokenString, RegisterFBAccountCallback);
            }
            catch (Exception e)
            {
                Debug.Log("LoginPage::ProcessFacebooksProfileData error:" + e.Message);
                RegisterFBAccountCallback(false);
            }
        }
        else
        {
            RegisterFBAccountCallback(false);
        }
    }

    private void ProcessFacebooksProfilePicture(byte[] data)
    {
        Debug.Log("Call process facebook image");
        if (data != null)
        {
            // use fb_profile_image as byte[] now as we can load it on Sprite later
            Session.LoggedInUser.fb_profile_picture = data;
        }
    }

    private void RegisterFBAccountCallback(bool isSuccess)
    {
        LoadingView.Hide();

        if (!isSuccess)
        {
            DialogView.Show("Facebookログインに失敗しました");
            if (FB.IsLoggedIn) FB.LogOut();
        }
        else
        {
            Debug.Log("Registered new facebook account for email " + Session.LoggedInUser.email);
            PageManager.Show(defaultPage);
        }
    }

    public void ShowResetPasswordPage()
    {
        PageManager.Show(PageType.ResetPassword);
    }
}
