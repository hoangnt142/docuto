﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class NoticeAPI
{

    static Action<bool, NoticeData> processGetNotice;

    // Get unread notice number
    public static void GetUnreadNumber(bool isInstructor, Action<bool, NoticeBadgeData> processGetUnreadNumber)
    {
        var data = new { is_instructor = isInstructor};

        var api = new API("notices/unread", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            processGetUnreadNumber(true, JsonMapper.ToObject<NoticeBadgeData>(dataJson));
        };
        api.onError = (errorJson) =>
        {
            processGetUnreadNumber(false, new NoticeBadgeData());
        };

        api.Call();
    }

    // Get list notices
    public static void GetNotices(bool isInstructor, Action<bool, List<NoticeData>> processGetNotices)
    {
        var data = new { is_instructor = isInstructor };

        var api = new API("notices", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            processGetNotices(true, JsonMapper.ToObject<List<NoticeData>>(dataJson));
        };

        api.Call();
    }

    //Get notice's detail
    public static void GetNotice(int id, Action<bool, NoticeData> processGetNotice)
    {
        var data = new { };

        var api = new API("notices/" + id, BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            processGetNotice(true, JsonMapper.ToObject<NoticeData>(dataJson));
        };

        api.Call();
    }

    public static void ReadNotice(int id)
    {
        var data = new { };

        var api = new API("notices/" + id + "/read", BestHTTP.HTTPMethods.Post, data);
        api.onError = (errorJson) =>
        {
            Debug.Log("Could not mark notice as read with error " + errorJson);
        };

        api.Call();
    }
}