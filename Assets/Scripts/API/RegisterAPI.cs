﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class RegisterAPI
{
    const string NORMAL_REGISTER_API = "register";
    const string FACEBOOK_REGISTER_API = "facebook_session";

    public static void Register(string name, int sex, string email, string password, Action<bool,int> registerCallback)
    {
        var data = new
        {
            name,
            email,
            sex,
            password
        };

        var api = new API(NORMAL_REGISTER_API, BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) =>
        {
            registerCallback(true, 0);
        };
        api.onError = (errorJson) =>
        {
            JsonData jsonData = JsonMapper.ToObject(errorJson);
            int errorType = 0;
            if (jsonData.Keys.Contains("error_type"))
            {
                errorType = (int)jsonData["error_type"];
            }
            registerCallback(false, errorType);
        };

        api.Call();
    }

    public static void RegisterFBAccount( string token, Action<bool> registerCallback)
    {
        var data = new
        {
            facebook_token = token
        };

        var api = new API(FACEBOOK_REGISTER_API, BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) =>
        {
            try
            {
                var userData = JsonMapper.ToObject<UserData>(dataJson);
                Session.Token = userData.auth_token;
                Session.LoggedInUser = userData;
                ProfileImageLoader.GetImage (userData.id.ToString (), ImageSizes.detail, null);
                BackgroundImageLoader.GetImage (userData.id.ToString (), null);

                //save auth token
                PlayerPrefs.SetString(ApplicationSystem.CONST.AUTH_TOKEN_KEY, userData.auth_token);
                PlayerPrefs.SetString(ApplicationSystem.CONST.USER_DATA_KEY, dataJson);
                PlayerPrefs.Save();

                registerCallback(true);
            }
            catch (Exception e)
            {
                Debug.Log("RegisterAPI::RegisterFBAccount error: " + e.Message);
                registerCallback(false);
            }
        };
        api.onError = (errorJson) =>
        {
            registerCallback(false);
        };

        api.Call();
    }
}
