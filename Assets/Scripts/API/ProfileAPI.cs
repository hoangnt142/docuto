﻿﻿using System;
using System.Collections;

public class ProfileAPI
{

    static string API = "profile";

    public static void UpdateProfile (byte [] profileImage, byte [] backgroundImage, string name, int sex, DateTime dob, string email, string password, Action<bool> callback)
    {
        var data = new Hashtable ();
        if (profileImage != null) {
            data.Add ("profile_image", Convert.ToBase64String (profileImage));
        }
        if (backgroundImage != null) {
            data.Add ("background_image", Convert.ToBase64String (backgroundImage));
        }
        data.Add ("name", name);
        data.Add ("email", email);
        data.Add("sex", sex);

        string dobString = null;
        if (dob.Ticks > 0) dobString = dob.ToString("yyyy-MM-dd");
        data.Add("birthday", dobString);

        if (!String.IsNullOrEmpty(password)) {
            data.Add ("password", password);
        }

        var api = new API (API, BestHTTP.HTTPMethods.Patch, data);
        api.onSuccess = (dataJson) => {

            callback (true);
        };
        api.onError = (errorJson) => {

            callback (false);
        };

        api.Call ();
    }
}
