﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using BestHTTP.WebSocket;
using LitJson;

using BestHTTP.WebSocket;
using BestHTTP.Examples;

public class ChatWebsocket : MonoBehaviour
{
    const string SERVER_URL = "ws://160.16.231.104/cable";
    const string CHANNEL_NAME = "ChatGroupChannel";

    static int chat_group_id;
    static WebSocket ws;
    static bool isFromInstructor = false;

    public static void Setup (int chat_group_id, bool isFromInstructor, OnWebSocketMessageDelegate OnMessage)
    {
        ChatWebsocket.isFromInstructor = isFromInstructor;
        var webSocket = new WebSocket (new Uri (SERVER_URL));
        Debug.Log (webSocket.InternalRequest.IsCookiesEnabled);
        webSocket.InternalRequest.Cookies.Add (new BestHTTP.Cookies.Cookie ("token", Session.Token));
        webSocket.OnOpen += (ws) => {
            JoinChannel (ws, chat_group_id);
        };
        webSocket.OnError += OnError;
        webSocket.OnErrorDesc += OnErrorDesc;
        webSocket.OnClosed += OnWebSocketClosed;
        webSocket.OnMessage += (ws, message) => {
            Debug.Log ("message : " + message);
            JsonData jobject= JsonMapper.ToObject(message);
            if (!jobject.Keys.Contains("type"))
            {
                int messageId = (int)jobject ["message"] ["message_id"];
                Debug.Log ("message : " + messageId);
                SendRead(messageId);
            }
        };
        webSocket.OnMessage += OnMessage;
        webSocket.Open ();
        ChatWebsocket.chat_group_id = chat_group_id;
        ChatWebsocket.ws = webSocket;
    }

    public static void Close ()
    {
        if (ws == null) {
            return;
        }

        ws.Close ();
        ws = null;
    }

    static void JoinChannel (WebSocket ws, int chat_group_id)
    {
        Debug.Log (chat_group_id);
        var dict = new Dictionary<string, object> ();
        var identifier = new Dictionary<string, string> ();
        identifier ["channel"] = CHANNEL_NAME;
        identifier ["chat_group_id"] = chat_group_id.ToString ();
        dict ["command"] = "subscribe";
        dict ["identifier"] = JsonMapper.ToJson (identifier);
        Debug.Log (JsonMapper.ToJson (dict));
        ws.Send (JsonMapper.ToJson (dict));
    }

    public static void SendMessage (string message)
    {
        if (ws == null) {
            return;
        }

        var dict = new Dictionary<string, object> ();
        var identifier = new Dictionary<string, string> ();
        var data = new Dictionary<string, object> ();
        identifier ["channel"] = CHANNEL_NAME;
        identifier ["chat_group_id"] = chat_group_id.ToString();
        data ["action"] = "speak";
        data ["message"] = message;
        data ["user_id"] = Session.LoggedInUser.id.ToString ();
        data ["instructor_id"] = isFromInstructor ? Session.LoggedInInstructor.id.ToString () : "";
        dict ["command"] = "message";
        dict ["identifier"] = JsonMapper.ToJson (identifier);
        dict ["data"] = JsonMapper.ToJson (data);

        ws.Send (JsonMapper.ToJson (dict));
    }

    public static void SendRead(int messageId)
    {
        var dict = new Dictionary<string, object>();
        var identifier = new Dictionary<string, string>();
        var data = new Dictionary<string, object>();
        identifier["channel"] = CHANNEL_NAME;
        identifier["chat_group_id"] = chat_group_id.ToString();
        data["action"] = "read";
        data["message_id"] = messageId.ToString();
        data["chat_group_id"] = chat_group_id.ToString();
        dict["command"] = "message";
        dict["identifier"] = JsonMapper.ToJson(identifier);
        dict["data"] = JsonMapper.ToJson(data);
        ws.Send(JsonMapper.ToJson(dict));
    }

    static void OnError (WebSocket ws, Exception ex)
    {
        string errorMsg = string.Empty;
        if (ws.InternalRequest.Response != null)
            errorMsg = string.Format ("Status Code from Server: {0} and Message: {1}", ws.InternalRequest.Response.StatusCode, ws.InternalRequest.Response.Message);
        Debug.Log ("An error occured: " + (ex != null ? ex.Message : "Unknown: " + errorMsg));
    }

    static void OnErrorDesc (WebSocket ws, string error)
    {
        Debug.Log ("Error: " + error);
    }

    static void OnWebSocketClosed (WebSocket webSocket, UInt16 code, string message)
    {
        Debug.Log ("WebSocket Closed!");
    }

}
