﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class InstructorAPI
{
    public static void UpdateProfile (byte [] profileImage, byte[] backgroundImage, string name, string category_text, string explanation, Action<bool, InstructorData> callback)
    {
        var data = new Hashtable ();
        if (profileImage != null) {
            data.Add ("profile_image", Convert.ToBase64String (profileImage));
        }
        if (backgroundImage != null) {
            data.Add ("background_image", Convert.ToBase64String (backgroundImage));
        }
        if (!string.IsNullOrEmpty (name)){
            data.Add ("name", name);
        }
        if (!string.IsNullOrEmpty (category_text)) {
            data.Add ("category_text", category_text);
        }
        if (!string.IsNullOrEmpty (explanation)) {
            data.Add ("explanation", explanation);
        }

        var api = new API ("instructors/profile", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) => {
            try
            {
                InstructorData instructor = JsonMapper.ToObject<InstructorData>(dataJson);
                callback(true, instructor);
            }
            catch (Exception e)
            {
                Debug.Log("InstructorAPI::UpdateProfile error: " + e.Message);
                callback(false, null);
            }

        };
        api.onError = (errorJson) => {
            callback(false, null);
        };

        api.Call ();

    }

    public static void GetCourses(int id, bool isPublic, Action<List<CourseData>> processListCourses, Action onFailCallback)
    {
        var data = new
        {
            is_public = isPublic
        };

        var api = new API ("instructors/" + id + "/courses", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) => {
            try
            {
                var responseData = JsonMapper.ToObject<GetCoursesResponseData>(dataJson);
                processListCourses(responseData.courses);
            }
            catch (Exception e)
            {
                Debug.Log("InstructorAPI::GetCourses error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) => {
            onFailCallback();
        };

        api.Call ();

    }

    public static void GetAllCourses(int id, Action<List<CourseData>> processListCourses, Action onFailCallback)
    {
        var data = new
        {
            
        };

        var api = new API("instructors/" + id + "/courses/all", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) => {
            try
            {
                var responseData = JsonMapper.ToObject<GetCoursesResponseData>(dataJson);
                processListCourses(responseData.courses);
            }
            catch (Exception e)
            {
                Debug.Log("InstructorAPI::GetAllCourses error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) => {
            onFailCallback();
        };

        api.Call();

    }

    public static void UpdateBankInfo(int id, BankData bankInfo, Action<bool> callback)
    {
        var data = new Hashtable ();
        data.Add ("id", id);
        data.Add ("bank_name", bankInfo.name);
        data.Add ("bank_type", bankInfo.type);
        data.Add ("bank_number", bankInfo.name);

        var api = new API ("instructors/profile", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) => {
            callback (true);
        };
        api.onError = (errorJson) => {
            callback (false);
        };

        api.Call ();
    }

    public static void GetLessons(int id, bool is_public, Action<List<LessonData>> ProcessListLessons, Action onFailCallback)
    {
        var data = new Hashtable();
        data.Add("is_public", is_public);

        var api = new API("instructors/" + id + "/lessons", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) => {
            try
            {
                var responseData = JsonMapper.ToObject<GetLessonsResponseData>(dataJson);
                ProcessListLessons(responseData.lessons);
            }
            catch (Exception e)
            {
                Debug.Log("InstructorAPI::GetCourses error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) => {
            onFailCallback();
        };

        api.Call();
    }
}

struct GetCoursesResponseData
{
    public List<CourseData> courses;
    public InstructorData instructor;
}
struct GetLessonsResponseData
{
    public List<LessonData> lessons;
}

