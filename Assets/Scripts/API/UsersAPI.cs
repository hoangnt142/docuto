﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class UsersAPI
{
    public static void ResetPassword(string email, Action<bool,int> callback)
    {
        var data = new
        {
            email
        };
        Debug.Log("Call API reset password for email: " + data.email);

        var api = new API("reset_password", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) =>
        {
            Debug.Log("Reset password success");
            callback(true,0);
        };
        api.onError = (errorJson) =>
        {
            Debug.Log("Reset password fail with error " + errorJson);
            JsonData jsonErrors = JsonMapper.ToObject(errorJson);
            int errorType = 0;
            if (jsonErrors.Keys.Contains("error_type"))
            {
                errorType = (int)jsonErrors["error_type"];
            }
            callback(false, errorType);
        };

        api.Call();

    }

    public static void GetSubscribeCourses(int id, bool isSubscribing, Action<List<CourseData>> processUpdateListCourses, Action onFailCallback)
    {
        Debug.Log("Call API to get list subscribing courses for " + id);

        var data = new
        {
            is_now = isSubscribing
        };
        var api = new API("users/" + id + "/courses", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            try
            {
                List<CourseData> courseData = JsonMapper.ToObject<List<CourseData>>(dataJson);
                processUpdateListCourses(courseData);
            }
            catch (Exception e)
            {
                Debug.Log("UsersAPI::GetSubscribingCourses error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) =>
        {
            Debug.Log("Get subscribing fail with error " + errorJson);

            onFailCallback();
        };


        api.Call();
    }
}
