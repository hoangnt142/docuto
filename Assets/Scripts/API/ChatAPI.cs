﻿using System;
using UnityEngine;
using LitJson;
using System.Collections.Generic;
using System.Collections;

public class ChatAPI
{
    public static void GetChatGroup(int chatGroupId, Action<bool, ChatGroupData> processGetChatGroup)
    {
        var data = new {};

        var api = new API("chat/groups/" + chatGroupId, BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            processGetChatGroup(true, JsonMapper.ToObject<ChatGroupData>(dataJson));
        };
        api.Call();
    }

    public static void NewChatGroup(int toUserId,bool isInstructor, Action<ChatGroupData> processNewChatGroup, Action onErrorCallback)
    {
        var data = new Hashtable();
        data.Add("to_id", toUserId);
        data.Add("is_instructor", isInstructor);

        var api = new API("chat/groups/", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) =>
        {
            try
            {
                ChatGroupData chatGroupData = JsonMapper.ToObject<ChatGroupData>(dataJson);
                processNewChatGroup(chatGroupData);
            }
            catch (Exception e)
            {
                Debug.Log("ChatAPI::NewChatGroup error: " + e.Message);
                onErrorCallback();
            }
        };
        api.onError = (errorJson) =>
        {
            onErrorCallback();
        };

        api.Call();
    }

    public static void GetListChatGroup(bool isInstructor, Action<List<ChatGroupData>> processGetChatGroup, Action onErrorGetChatGroup)
    {
        var data = new {is_instructor = isInstructor};
        var api = new API("chat/groups/", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            List<ChatGroupData> chatGroupData = JsonMapper.ToObject<List<ChatGroupData>>(dataJson);
            processGetChatGroup(chatGroupData);
        };
        api.onError = (errorJson) =>
        {
            onErrorGetChatGroup();
        };
        api.Call();
    }
    public static void GetUnreadNum(bool isInstructor, Action<bool,int> onSuccess,Action onError)
    {
        var data = new { is_instructor = isInstructor };
        var api = new API("chat/groups/unread_num/", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            JsonData jsonData = JsonMapper.ToObject(dataJson);
            int unreadNum = 0;
            if (jsonData.Keys.Contains("unread_num"))
            {
                unreadNum = (int)jsonData["unread_num"];
            }
            onSuccess(isInstructor,unreadNum);
        };
        api.onError = (errorJson) =>
        {
            Debug.Log("Error get unread num");
            onError();
        };
        api.Call();
    }
}

