﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoAPI
{
    static VideoAPI instance;
    public static VideoAPI Instance
    {
        get
        {
            if (instance == null)
            {
                instance = new VideoAPI();
            }
            return instance;
        }
    }

    List<API> stack = new List<API>();

    public static void Upload(byte[] videoBytes, string videoName, int part, int total_part, int lessonId, int courseId, Action<int, bool> processUpload, int queuePriority = 0)
    {
        Instance._Upload(videoBytes, videoName, part, total_part, lessonId, courseId, processUpload, queuePriority);
    }

    public void _Upload(byte[] videoBytes,string videoName, int part, int total_part, int lessonId, int courseId, Action<int, bool> processUpload, int queuePriority)
    {
        var data = new Hashtable();
        //{ course_id = courseId, lesson_id = lessonId, video = videoBase64 };
        if (lessonId < 1 && courseId < 1)
        {
            processUpload(0, false);
            return;
        }

        if(courseId > 0) data.Add("course_id", courseId);
        if (lessonId > 0)  data.Add("lesson_id", lessonId);

        // add total_part to data
        data.Add("total_part", total_part);
        data.Add("video_name", videoName);
        data.Add("part", part);
        data.Add("video", Convert.ToBase64String(videoBytes));

        var api = new API("videos", BestHTTP.HTTPMethods.Post, data);
        // to free up memory space, we remove stack if API done
        api.onSuccess = (dataJson) =>
        {
            stack.Remove(api);
            processUpload(part, true);
        };
        api.onError = (errorJson) =>
        {
            stack.Remove(api);
            processUpload(part, false);
        };
        api.Call(queuePriority);

        stack.Add(api);
    }

    public static void Dispose()
    {
        if (instance == null) return;

        foreach(API api in instance.stack)
        {
            api.Dispose();
        }
        instance = null;
    }
}
