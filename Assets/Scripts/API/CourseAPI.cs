﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;

public class CourseAPI
{
    public static void AddFavorite(int id, Action<bool> processUpdateFavorite, Action onFailUpdateFavorite)
    {
        var data = new { };

        var api = new API ("courses/" + id + "/favorite", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) => {
            processUpdateFavorite (true);
        };
        api.onError = (errorJson) => {
            onFailUpdateFavorite ();
        };

        api.Call ();
    }

    public static void RemoveFavorite(int id, Action<bool> processUpdateFavorite, Action onFailUpdateFavorite)
    {
        var data = new { };

        var api = new API ("courses/" + id + "/favorite", BestHTTP.HTTPMethods.Delete, data);
        api.onSuccess = (dataJson) => {
            processUpdateFavorite (false);
        };
        api.onError = (errorJson) => {
            onFailUpdateFavorite ();
        };


        api.Call ();
    }

    public static void Subscribe (CourseData course,string receipet, string paymentType,string appProductId,int osType, Action processSubscribeCource, Action onFailCallback)
    {
        var data = new {
            receipet = receipet,
            payment_type = paymentType,
            app_product_id = appProductId,
            os_type = osType
        };

        var api = new API ("courses/" + course.id + "/subscribes", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) => {
            processSubscribeCource ();
        };
        api.onError = (errorJson) => {
            onFailCallback ();
        };

        api.Call ();
    }

    public static void GetDetail(int id, Action<CourseData> processGetDetail, Action onFailCallback)
    {
        Debug.Log ("Get detail for course " + id);
        var data = new { };

        var api = new API ("courses/" + id, BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) => {
            try {
                CourseData courseData = JsonMapper.ToObject<CourseData> (dataJson);
                processGetDetail (courseData);
            } catch (Exception e) {
                Debug.Log ("CourseAPI::GetDetail error: " + e.Message);
                onFailCallback ();
            }
        };
        api.onError = (errorJson) => {
            onFailCallback ();
        };

        api.Call ();
    }

    public static void GetByCategoryId (int categoryId, int level, int page, Action<List<CourseData>> processListCourses, Action onFailCallback, Action<bool> onPagination)
    {
        var data = new {
            course_category_id = categoryId,
            level = level,
            page_id = page
        };

        var api = new API("courses", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            try
            {
                List<CourseData> courseData = JsonMapper.ToObject<List<CourseData>>(dataJson);
                processListCourses(courseData);
            }
            catch (Exception e)
            {
                Debug.Log("CourseAPI::GetByCategoryId error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) => {
            onFailCallback ();
        };
        api.onPagination = (hasNext) => {
            onPagination(hasNext);
        };

        api.Call ();
    }

    public static void GetLatestLessonDetail(int courseId, Action<LessonData> processLessonDetail, Action onFailCallback)
    {
        var data = new { };

        var api = new API ("courses/" + courseId + "/latest_lesson", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) => {
            try
            {
                LessonData lessonData = JsonMapper.ToObject<LessonData>(dataJson);
                processLessonDetail(lessonData);
            }
            catch (Exception e)
            {
                Debug.Log("CourseAPI::GetLessonDetail error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) => {
            onFailCallback ();
        };

        api.Call ();
    }

    public static void CreateCourse (CourseData courseData, Action<CourseData> onSuccess, Action onError)
    {
        Hashtable data = new Hashtable ();
        if (courseData.id > 0) {
            data.Add ("id", courseData.id);
        }
        data.Add ("title", courseData.title);
        data.Add ("explanation", courseData.explanation);
        data.Add ("necessity", courseData.necessity);
        data.Add ("level", courseData.level);
        data.Add ("course_category_id", courseData.course_category_id);
        data.Add ("fee_yen", courseData.fee_yen);
        data.Add ("update_type", courseData.update_type);
        data.Add("is_public", courseData.is_public);
        data.Add ("image", courseData.image);

        var api = new API ("courses", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) => {
            try
            {
                CourseData newCourseData = JsonMapper.ToObject<CourseData>(dataJson);
                Debug.Log("success : " + dataJson);
                onSuccess(newCourseData);
            }
            catch (Exception e)
            {
                Debug.Log("CourseAPI::CreateCourse error: " + e.Message);
                onError();
            }
        };
        api.onError = (errorJson) => {
            Debug.Log ("error : " + errorJson);
            onError();
        };
        api.Call ();
    }

    public static void GetListFavorite(int level, Action<List<CourseData>> processListCourses, Action onFailCallback)
    {
        var data = new { level };

        var api = new API("courses/favorites", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            try
            {
                List<CourseData> courses = JsonMapper.ToObject<List<CourseData>>(JsonMapper.ToObject(dataJson)["courses"].ToJson());
                processListCourses(courses);
            }
            catch (Exception e)
            {
                Debug.Log("CourseAPI::GetListFavorite error: " + e.Message);
                onFailCallback();
            }
        };
        api.onError = (errorJson) =>
        {
            onFailCallback();
        };

        api.Call();

    }

    public static void CreateLesson(int courseId, LessonData lessonData, bool isPublic, Action<LessonData> onSuccess, Action onError)
    {
        Hashtable data = new Hashtable();
        if (lessonData.id > 0)
        {
            data.Add("id", lessonData.id);
        }
        data.Add("title", lessonData.title);
        data.Add("course_id", courseId);
        data.Add("explanation", lessonData.explanation);
        data.Add("necessity", lessonData.necessity);
        data.Add("is_public", isPublic);

        var api = new API("courses/" + courseId + "/lessons", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) =>
        {
            try
            {
                LessonData lesson = JsonMapper.ToObject<LessonData>(dataJson);
                onSuccess(lesson);
            }
            catch (Exception e)
            {
                Debug.Log("CourseAPI::GetListFavorite error: " + e.Message);
                onError();
            }
        };
        api.onError = (errorJson) =>
        {
            onError();
        };

        api.Call();
    }

    public static void DeleteCourse(int courseId, Action<DeleteCourseResponseData> onSuccess, Action onError)
    {
        Hashtable data = new Hashtable();

        var api = new API("courses/" + courseId, BestHTTP.HTTPMethods.Delete, data);
        api.onSuccess = (dataJson) =>
        {
            DeleteCourseResponseData deleteData = JsonMapper.ToObject<DeleteCourseResponseData>(dataJson);
            onSuccess(deleteData);
        };
        api.onError = (errorJson) =>
        {
            onError();
        };

        api.Call();
    }

    public static void DeleteCourse(int courseId, Action<bool> onSuccess)
    {
        Hashtable data = new Hashtable();

        var api = new API("courses/" + courseId, BestHTTP.HTTPMethods.Delete, data);
        api.onSuccess = (dataJson) =>
        {
            onSuccess(true);
        };
        api.onError = (errorJson) =>
        {
            onSuccess(false);
        };

        api.Call();
    }

    public static void DeleteLesson(int lessonId, Action<bool,bool> processDeleteLesson)
    {
        Hashtable data = new Hashtable();

        var api = new API("lessons/" + lessonId, BestHTTP.HTTPMethods.Delete, data);
        api.onSuccess = (dataJson) =>
        {
            JsonData jsonData = JsonMapper.ToObject(dataJson);
            bool doesSendRequest = false;
            if (jsonData.Keys.Contains("does_send_request"))
            {
                doesSendRequest = (bool)jsonData["does_send_request"];
            }
            processDeleteLesson(true,doesSendRequest);
        };
        api.onError = (errorJson) =>
        {
            processDeleteLesson(false,false);
        };

        api.Call();
    }
}
