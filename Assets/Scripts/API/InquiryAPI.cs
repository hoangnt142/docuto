﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InquiryAPI
{
    public static void Send(string message, int userId, int instructorId, Action<bool> processSend)
    {
        Hashtable data = new Hashtable();
        data.Add("content", message);
        if(userId > 0) data.Add("user_id", userId);
        if(instructorId > 0) data.Add("instructor_id", instructorId);

        var api = new API("inquiry", BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) => {
            processSend(true);
        };
        api.onError = (errorJson) => {
            processSend(false);
        };

        api.Call();
    }
}
