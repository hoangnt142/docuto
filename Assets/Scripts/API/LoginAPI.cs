﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using LitJson;


public class LoginAPI
{

    const string API = "sessions";

    public static void Login(string email, string password, Action<bool> loginCallback)
    {
        var data = new
        {
            email = email,
            password = password
        };
        var api = new API(API, BestHTTP.HTTPMethods.Post, data);
        api.onSuccess = (dataJson) =>
        {
            Debug.Log("Login Success");
            var userData = JsonMapper.ToObject<UserData>(dataJson);
            Session.Token = userData.auth_token;
            Session.LoggedInUser = userData;
            ProfileImageLoader.GetImage(userData.id.ToString(), ImageSizes.detail, null);
            BackgroundImageLoader.GetImage(userData.id.ToString(), null);

            if (Session.IsInstructor())
            {
                InstructorProfileImageLoader.GetImage(Session.LoggedInInstructor.id.ToString(), ImageSizes.detail, null);
                InstructorBackgroundImageLoader.GetImage(Session.LoggedInInstructor.id.ToString(), null);
            }

            loginCallback(true);

            //save auth token
            PlayerPrefs.SetString(ApplicationSystem.CONST.AUTH_TOKEN_KEY, userData.auth_token);
            PlayerPrefs.SetString(ApplicationSystem.CONST.USER_DATA_KEY, dataJson);
            PlayerPrefs.Save();
        };
        api.onError = (errorJson) =>
        {
            Debug.Log("Login fail with error " + errorJson);

            loginCallback(false);
        };

        api.Call();
    }

    public static void Logout(Action processLogout)
    {
        var api = new API("sessions/" + Session.Token, BestHTTP.HTTPMethods.Delete, new {} );
        api.onSuccess = (dataJson) =>
        {
            processLogout();
        };
        api.onError = (errorJson) =>
        {
            processLogout();
        };

        api.Call();

        RemovePlayerPref();
    }

    public static void IsAlive(Action<bool> ProcessCheckAlive)
    {
        Session.Token = PlayerPrefs.GetString(ApplicationSystem.CONST.AUTH_TOKEN_KEY, "");
        if (String.IsNullOrEmpty(Session.Token))
        {
            ProcessCheckAlive(false);
            return;
        }

        var api = new API("sessions/alive", BestHTTP.HTTPMethods.Get, new {});
        api.onSuccess = (dataJson) =>
        {
            try
            {
                IsAliveData responseData = JsonMapper.ToObject<IsAliveData>(dataJson);
                if (responseData.is_alive)
                {
                    Debug.Log("Sesson is alive");
                    var userData = JsonMapper.ToObject<UserData>(PlayerPrefs.GetString(ApplicationSystem.CONST.USER_DATA_KEY, ""));
                    Session.Token = userData.auth_token;
                    Session.LoggedInUser = userData;
                    ProcessCheckAlive(true);
                }
                else
                {
                    Debug.Log("Sesson is expired");
                    RemovePlayerPref();
                    ProcessCheckAlive(false);
                }
            }
            catch (Exception e)
            {
                Debug.Log("Sesson is expired. " + e.Message);
                RemovePlayerPref();
                ProcessCheckAlive(false);
            }
        };
        api.onError = (errorJson) =>
        {
            Debug.Log("Sesson is expired");
            RemovePlayerPref();
            ProcessCheckAlive(false);
        };

        api.Call();
    }

    static void RemovePlayerPref()
    {
        PlayerPrefs.DeleteKey(ApplicationSystem.CONST.AUTH_TOKEN_KEY);
        PlayerPrefs.DeleteKey(ApplicationSystem.CONST.USER_DATA_KEY);
        PlayerPrefs.Save();
    }
}

struct IsAliveData
{
    public bool is_alive;
}