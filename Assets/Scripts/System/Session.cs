﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Session{

    const string KEY_API_TOKEN = "api_token";

    public static string Token {
        get {
            return PlayerPrefs.GetString (KEY_API_TOKEN);
        }
        set {
            // TODO tone use encrypted playerprefs
            PlayerPrefs.SetString (KEY_API_TOKEN, value);
        }
    }

    public static bool HasToken ()
    {
        return PlayerPrefs.HasKey (KEY_API_TOKEN) && Token != "";
    }

    public static UserData LoggedInUser;
    public static InstructorData LoggedInInstructor {
        get {
            return LoggedInUser.instructor;
        }
        set {
            LoggedInUser.instructor = value;
        }
    }

    public static bool IsInstructor ()
    {
        return LoggedInInstructor != null;
    }

}
