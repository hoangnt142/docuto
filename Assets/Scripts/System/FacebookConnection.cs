﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Facebook.Unity;

public class FacebookConnection
{

    private static Action<bool> processLogin;


    public static void Login(Action<bool> callBack)
    {
        processLogin = callBack;

        // Init Facebook
        if (!FB.IsInitialized)
        {
            // Initialize the Facebook SDK
            FB.Init(InitCallback, OnHideUnity);
        }
        else
        {
            // Already initialized, signal an app activation App Event
            FB.ActivateApp();

            // Open FB login canvas
            OpenLoginForm();
        }
    }

    private static void OpenLoginForm()
    {
        // Login with read permission
        List<string> perms = new List<string>() { "public_profile", "email" };
        FB.LogInWithReadPermissions(perms, LoginCallback);
    }

    // Call back functions
    private static void InitCallback()
    {
        if (FB.IsInitialized)
        {
            // Signal an app activation App Event
            FB.ActivateApp();

            // Open FB login canvas
            OpenLoginForm();
        }
        else
        {
            Debug.Log("Failed to Initialize the Facebook SDK");
        }
    }

    private static void OnHideUnity(bool isGameShown)
    {
        if (!isGameShown)
        {
            // Pause the game - we will need to hide
            Time.timeScale = 0;
        }
        else
        {
            // Resume the game - we're getting focus again
            Time.timeScale = 1;
        }
    }

    private static void LoginCallback(ILoginResult result)
    {
        bool isLoggedIn = false;
        if (!String.IsNullOrEmpty(result.Error) || result.Cancelled)
        {
            isLoggedIn = false;
        }
        else { isLoggedIn = FB.IsLoggedIn; }

        processLogin(isLoggedIn);
    }

    public static void LogOut()
    {
        FB.LogOut();
    }

    public static void GetData(Action<IDictionary<string, object>> callback, params string[] fields)
    {
        // call facebook API
        string listFields = String.Join(",", fields);

        FB.API("/me?fields=" + listFields, HttpMethod.GET, (res) => { callback(res.ResultDictionary); });
    }

    // GET USER FACE BOOK IMAGE
    // - node: `picture` or `photos`
    // - Use `type`, `width` and `height` at the moment as no need anymore. And it can be improve.
    public static void GetImage(Action<byte[]> callback, string node, string type = "", int width = 0, int height = 0)
    {
        string queryString = "";
        if (type != "") queryString += "type=" + type + "&";
        if (width > 0) queryString += "width=" + width + "&";
        if (height > 0) queryString += "height=" + height + "&";


        if (queryString != "")
        {
            queryString = queryString.Substring(0, queryString.Length - 1);
            queryString = "?" + queryString;
        }

        FB.API("/me/" + node + queryString, HttpMethod.GET, (res) => { callback(res.Texture.GetRawTextureData()); });
    }

}
