﻿using LitJson;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppPurchase:MonoBehaviour  {
    private static AppPurchase _instance;
    public static AppPurchase Instance
    {
        get
        {
            if(_instance == null)
            {
                _instance = FindObjectOfType<AppPurchase>();
            }
            return _instance;
        }
    }
    public Action OnConnectFailed { get; private set; }
    public Action<UM_PurchaseResult,AppProductData,int> OnPurchaseSuccess { get; private set; }
    public Action OnPurchaseFailed { get; private set; }

    private string processingProductId = null;
    private AppProductData appProductData;
    public void PurchaseAppProduct(int feeYen, Action<UM_PurchaseResult, AppProductData, int> onPurchaseSuccess, Action<int> onGetAppProductFailed,Action onConnectServiceFailed,Action onPurchaseFailed)
    {
        this.OnPurchaseSuccess = onPurchaseSuccess;
        this.OnConnectFailed = onConnectServiceFailed;
        this.OnPurchaseFailed = onPurchaseFailed;

        int osType  = AppPurchase.ValidPlatform();
        if (osType == 0)
        {
            Debug.Log("Invalid platform");
            onGetAppProductFailed(0);
            return;
        }
        var data = new {
            fee_yen = feeYen,
            os_type  = osType
        };

        var api = new API("app_products/", BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) =>
        {
            appProductData = JsonMapper.ToObject<AppProductData>(dataJson);
            string productId;
            switch (osType)
            {
                case 1:
                    productId = appProductData.ios_sku_id;
                    break;
                case 2:
                    productId = appProductData.android_sku_id;
                    break;
                default:
                    Debug.Log("Invalid platform");
                    onGetAppProductFailed(0);
                    return;
            }
            // check product is registered or not, if not, register product
            if (RegisterInAppProduct(appProductData))
            {
                processingProductId = appProductData.code;
                CallConnect();
            }
            else
            {
                // purchase product when product register already
                Purchase(appProductData.code);
            }
        };
        api.onError = (errors) =>
        {
            JsonData jsonData = JsonMapper.ToObject(errors);
            int errorType = 0;
            if (jsonData.Keys.Contains("error_type"))
            {
                errorType = (int)jsonData["error_type"];
            }
            onGetAppProductFailed(errorType);
        };
        api.Call();
    }

    public static int ValidPlatform()
    {
        switch (Application.platform)
        {
            case RuntimePlatform.IPhonePlayer: return 1;
            case RuntimePlatform.Android: return 2;

            default:
                return 0;
        }
    }

    public void Purchase(string productId)
    {
        processingProductId = productId;
        if (!UM_InAppPurchaseManager.Client.IsConnected)
        {
            CallConnect();
        }
        else
        {
            CallPurchase(productId);
        }
    }

    private void CallConnect()
    {
        Debug.Log("Connecting............");
        UM_InAppPurchaseManager.Client.OnServiceConnected += OnBillingConnectFinishedAction;
        UM_InAppPurchaseManager.Client.Connect();
    }
    private void CallPurchase(string productId)
    {
        Debug.Log("Purchasing.............");
        UM_InAppPurchaseManager.Client.OnPurchaseFinished += OnPurchaseFlowFinishedAction;
		UM_InAppPurchaseManager.Client.Subscribe (productId);
    }

    private void OnBillingConnectFinishedAction(UM_BillingConnectionResult result)
    {
        UM_InAppPurchaseManager.Client.OnServiceConnected -= OnBillingConnectFinishedAction;
        if (result.isSuccess)
        {
            Debug.Log("Connected");
            if (processingProductId != null)
            {
                CallPurchase(processingProductId);
            }
        }
        else
        {
            Debug.Log("Failed to connect");
            if (OnConnectFailed != null)
            {
                OnConnectFailed();
            }
        }

        processingProductId = null;
    }

    private void OnPurchaseFlowFinishedAction(UM_PurchaseResult result)
    {
        UM_InAppPurchaseManager.Client.OnPurchaseFinished -= OnPurchaseFlowFinishedAction;
        if (result.isSuccess)
        {
            int osType = AppPurchase.ValidPlatform();
            Debug.Log("Product " + result.product.id + " purchase Success");
            if (OnPurchaseSuccess != null)
            {
                OnPurchaseSuccess(result, appProductData, osType);
            }
        }
        else
        {
            Debug.Log("Product " + result.product.id + " purchase Failed");
            if (OnPurchaseFailed != null)
            {
                OnPurchaseFailed();
            }
        }
    }
    private bool RegisterInAppProduct(AppProductData data)
    {
        if (ProductIdIsAdded(data.code))
        {
            return false;
        }
        UM_InAppProduct product = ConvertToInAppProduct(data);
		UltimateMobileSettings.Instance.AddProduct(product);
		UM_InAppPurchaseManager.UpdatePlatfromsInAppSettings ();
        return true;
    }

    private bool ProductIdIsAdded(string productId)
    {
        foreach (var item in UM_InAppPurchaseManager.InAppProducts)
        {
            if(item.id == productId)
            {
                return true;
            }
        }
        return false;
    }

    private UM_InAppProduct ConvertToInAppProduct(AppProductData data)
    {
        UM_InAppProduct product = new UM_InAppProduct();
        product.id = data.code;
        product.Type = UM_InAppType.Consumable;
        product.DisplayName = data.name;
		product.AndroidId = data.android_sku_id == null ? "" : data.android_sku_id;
		product.IOSId = data.ios_sku_id == null ? "" : data.ios_sku_id;
		product.AmazonId = "";
        return product;
    }
}
