﻿using System;
using System.Collections.Generic;
using UnityEngine;
using Facebook.Unity;

public class Authenticator
{
    public static Action OnLoggedInSuccess;
    public static Action OnLoggedInFail;
    public static Action OnLoggedOut;

    public static void Init(Action onSuccess, Action onFail, Action onLogout)
    {
        OnLoggedInSuccess = onSuccess;
        OnLoggedInFail = onFail;
        OnLoggedOut = onLogout;
    }

    // Call to API login
    public static void CallLoginAPI(string email, string password)
    {
        LoginAPI.Login(email, password, LoginApiCallback);
    }

    private static void LoginApiCallback(bool isSuccess)
    {
        if (isSuccess)
        {
            OnLoggedInSuccess();
        }
        else
        {
            OnLoggedInFail();
        }
    }


    public static void LoginWithFacebook()
    {
        FacebookConnection.Login(FacebookLoginCallback);
    }

    private static void FacebookLoginCallback(bool isSuccess)
    {
        if (isSuccess)
        {
            Debug.Log("Facebook's user login success");

            OnLoggedInSuccess();
        }
        else
        {
            Debug.Log("Facebook's user cancelled login");

            OnLoggedInFail();
        }
    }

    public static void Logout()
    {
        // Logout Facebook if login with facebook
        if (FB.IsLoggedIn) FacebookConnection.LogOut();

        OnLoggedOut();
    }

    public static bool Validate(bool withDialog = true)
    {
        if (!Session.HasToken())
        {
            if (withDialog)
                DialogView.Show("ユーザー登録が必要です。登録しますか？", () =>
                {
                    PageManager.CurrentPage.Hide();
                    PageManager.Show(PageType.RegisterUser);
                }, () => { });
            else
            {
                PageManager.CurrentPage.Hide();
                PageManager.Show(PageType.RegisterUser);
            }

            return false;
        }

        return true;
    }
}