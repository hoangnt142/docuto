﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ColorManager : MonoBehaviour {

    static ColorManager instance;

    public static Color CommonUserColor {
        get {
            return instance.commonUserColor;
        }
    }

    public static Color InstructorColor {
        get {
            return instance.instructorColor;
        }
    }

    [SerializeField]
    Color commonUserColor;

    [SerializeField]
    Color instructorColor;

    void Awake ()
    {
        instance = this;
    }

}
