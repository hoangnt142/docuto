﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugApplication : MonoBehaviour {

    public bool isDeleteAllImagesOnStart;

	// Use this for initialization
	void Awake () {
        if (isDeleteAllImagesOnStart) {
            ImageLoader.DeleteAllImages ();
        }
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
