﻿﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PageManager : MonoBehaviour
{

    public enum ContentType
    {
        full,
        withButtons
    }

    static PageManager instance;
    static MenuType menuType;

    public static MenuType MenuType {
        get {
            return menuType;
        }
    }

    public static Page CurrentPage {
        get {
            return instance.currentPage;
        }
    }

    [SerializeField]
    PageType initialPageType;

    [SerializeField]
    List<Page> pages;

    [SerializeField]
    RectTransform fullContent;

    [SerializeField]
    RectTransform content;

    Page currentPage;
    List<Page> pageHistories = new List<Page> ();
    Dictionary<PageType, Page> pagePool = new Dictionary<PageType, Page> ();

    static bool canBackPage = true;

    void Awake ()
    {
        instance = this;
    }

    public static void ShowInitialPage ()
    {
        LoginAPI.IsAlive((isAlive) => {
            if(isAlive)
                instance._Show(PageType.CourseList, MenuType.CommonUser);
            else
                instance._Show(instance.initialPageType, MenuType.CommonUser);
        });

    }

    Page _Show (PageType pageType, MenuType menuType = MenuType.None)
    {
        if (currentPage != null && currentPage.PageType == pageType) {
            return null;
        }

        Page page = Instantiate(pageType);
        page.Show (menuType);

        // Active footer menu for current page
        FooterView.Reload(pageType, page.MenuType);

        switch (page.ContentType) {
        case ContentType.full:
            FooterView.Hide ();
            break;

        case ContentType.withButtons:
            FooterView.Show (page.MenuType);
            break;
        }

        if (currentPage != null) {
            currentPage.Hide ();
            Debug.Log ("hide : " + currentPage.PageType);
        }
        AddPageHistory (page);
        currentPage = page;

        return page;
    }

    public static Page Instantiate (PageType pageType)
    {
        Page page = null;

        if (instance.pagePool.ContainsKey (pageType)) 
        {
            page = instance.pagePool [pageType];
        }
        else
        {
            var obj = Instantiate (instance.pages.Find (p => p.PageType == pageType).gameObject);
            page = obj.GetComponent<Page> ();
            switch (page.ContentType) {
            case ContentType.full:

                obj.transform.SetParent (instance.fullContent, false);
                break;

            case ContentType.withButtons:
                obj.transform.SetParent (instance.content, false);
                break;
            }
            instance.pagePool.Add (pageType, page);
        }

        return page;
    }

    public static Page Show (PageType type, MenuType menuType = MenuType.None)
    {
        return instance._Show (type, menuType);
    }

    public static void GoBackPage ()
    {
        if (!canBackPage) return;

        if (CurrentPage.PageType == PageType.Login)
        {
            instance._Show(PageType.RegisterUser);
        }else if((CurrentPage.PageType == PageType.ProfileEdit && ((ProfileEditPage)(Instantiate(PageType.ProfileEdit))).DataChanged())
            || (CurrentPage.PageType == PageType.InstructorProfileEdit && ((InstructorProfileEditPage)(Instantiate(PageType.InstructorProfileEdit))).DataChanged())
            )
        {
            DialogView.Show("本当に戻りますか？", () =>
            {
                var page = instance.GetBackPage();

                if (page == null) ShowInitialPage();
                else instance._Show(page.PageType);
            }, () =>
            {
                return;
            });
        }
        else
        {
            var page = instance.GetBackPage ();

            if (page == null) ShowInitialPage(); 
            else instance._Show (page.PageType);
        }
    }

    public static bool ContainPage(PageType page)
    {
        return instance.pagePool.ContainsKey(page);
    }

    public static void ActiveBackPage(bool active) {
        canBackPage = active;
    }

    public static void AddPageHistory (Page page)
    {
        instance.pageHistories.Add (page);
        if (instance.pageHistories.Count > 50) {
            instance.pageHistories.RemoveAt (0);
        }
    }

    Page GetBackPage ()
    {
        if (pageHistories.Count == 0)
        {
            if (pagePool.ContainsKey(initialPageType))
                return pagePool[initialPageType];

            return null;
        }

        var page = pageHistories [pageHistories.Count - 2];
        pageHistories.RemoveAt (pageHistories.Count - 1);
        pageHistories.RemoveAt (pageHistories.Count - 1);
        return page;
    }

    public static void ResetPageHistory ()
    {
        if (instance.pageHistories.Count > 0)
            instance.pageHistories.Clear();

        instance.pageHistories = new List<Page> ();
    }

    public static void RemoveLastPage()
    {
        if (instance.pageHistories.Count > 0)
            instance.pageHistories.RemoveAt(instance.pageHistories.Count - 1);
    }

    public static void Clean()
    {
        foreach (Page child in instance.pages)
        {
            if (child.PageType == CurrentPage.PageType)
                continue;

            if (instance.pagePool.ContainsKey(child.PageType))
            {
                child.Hide();
                Destroy(instance.pagePool[child.PageType].gameObject);
                instance.pagePool.Remove(child.PageType);
            }
        }

        ResetPageHistory();
        AddPageHistory(CurrentPage);
    }

    // refresh all pages which implement Refresh method
    public static void RefreshAllPages()
    {
        List<PageType> refreshedPages = new List<PageType>();
        foreach(KeyValuePair<PageType, Page> child in instance.pagePool)
        {
            if (!refreshedPages.Contains(child.Key))
            {
                child.Value.Refresh();
                refreshedPages.Add(child.Key);
            }
        }
    }

    public static void RefreshPage(PageType page)
    {
        foreach (KeyValuePair<PageType, Page> child in instance.pagePool)
        {
            if (child.Key == page)
            {
                child.Value.Refresh();
                break;
            }
        }
    }

    public static Page GetActivedPage(PageType page)
    {
        if (instance.pagePool.ContainsKey(page))
            return instance.pagePool[page];

        return null;
    }
}

public enum PageType
{
    None,
    RegisterUser,
    Login,
    CourseList,
    CourseDetail,
    Subscribe,
    ConfirmSubscribe,
    MyCourse,
    ChatList,
    Chat,
    InstructorCourseList,
    NoticeList,
    Notice,
    Config,
    ProfileEdit,
    InstructorProfileEdit,
    InstructorConfig,
    InstructorMyCourse,
    InstructorEditFinish,
    EditCoursePage,
    InstructorLessonEdit,
    UnSubscribe,
    UnSubscribeConfirm,
    LessonDetailPage,
    ResetPassword,
    Agreement,
    Setting,
    Query,
    LeaveCourse,
    SelectPicture,
    InstructorVideo,
    BankAccount,
    InstructorCourseDetail,
    UploadVideoPage,
    InstructorVideoDetailPage,
    HowToUse,
    InstructorHowToUse,
    InstructorHowToUseDetail,
    HowToUseDetail,
    HowToUsePage1,
    HowToUsePage2,
    HowToUsePage3,
}

public enum MenuType
{
    None,
    CommonUser,
    Instructor,
    Both
}