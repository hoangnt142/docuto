﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CourseImageLoader : ImageLoader {

    static string API_BASE = "image/course/";
    static string IMAGE_PREFIX = "image_course_";

    public static void GetImage(string id, ImageSizes preferedSize, Action<Sprite> callback)
    {
        size = preferedSize;
        string api = API_BASE + id;
        var fileName = CreateFileName(IMAGE_PREFIX, id);
        GetImage(fileName, api, callback);
    }

    public static void SaveSprite(string id, Sprite sprite)
    {
        var fileName = CreateFileName(IMAGE_PREFIX, id);
        ImageStore.SetSprite(fileName, sprite);
    }

    public static void RemoveImage(string id)
    {
        DeleteImage(CreateFileName(IMAGE_PREFIX, id));
    }
}
