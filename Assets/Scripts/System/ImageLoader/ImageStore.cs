﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ImageStore
{
    static Dictionary<string, Sprite> imageDic = new Dictionary<string, Sprite> ();

    public static bool HasSprite (string name)
    {
        return imageDic.ContainsKey (name);
    }

    public static void SetSprite (string name, Sprite sprite)
    {
        if (imageDic.ContainsKey (name)) {
            Debug.Log ("update sprite on image store");
            imageDic [name] = sprite;
            return;
        }

        imageDic.Add (name, sprite);
    }

    public static Sprite GetSprite (string name)
    {
        Debug.Log ("get sprite : " + name);
        if (imageDic.ContainsKey (name)){
            return imageDic [name];
        } else {
            return null;
        }
    }

    public static void DeleteSprite(string name)
    {
        if (imageDic.ContainsKey(name))
        {
            Debug.Log("remove sprite : " + name);
            imageDic.Remove(name);
        }
    }
}
