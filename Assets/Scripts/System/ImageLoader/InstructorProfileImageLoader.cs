﻿using System;
using UnityEngine;

public class InstructorProfileImageLoader : ImageLoader
{
    static string API_BASE = "image/instructor_profile/";
    static string IMAGE_PREFIX = "image_instructor_profile_";

    public static void GetImage (string id, ImageSizes preferedSize, Action<Sprite> callback)
    {
        size = preferedSize;
        string api = API_BASE + id;
        var fileName = CreateFileName(IMAGE_PREFIX, id);
        GetImage (fileName, api, callback);
    }

    public static void SaveSprite (string id, Sprite sprite)
    {
        var fileName = CreateFileName(IMAGE_PREFIX, id);
        ImageStore.SetSprite (fileName, sprite);
    }
}
