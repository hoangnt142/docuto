﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
using BestHTTP;

public class ImageLoader
{
    static string IMAGE_PATH = Application.persistentDataPath + "/images/";
    protected static ImageSizes size;

    protected static void GetImage (string fileName, string api, Action<Sprite> callback)
    {
        if (callback == null) {
            callback = delegate {};
        }

        Debug.Log ("fileName : " + fileName);
        if (ImageStore.HasSprite (fileName)) {
            callback (ImageStore.GetSprite (fileName));
        } else if (HasImage (fileName)) {
            Debug.Log ("has image : " + fileName);
            var sprite = LoadImage (fileName);
            callback (sprite);
        } else {
            Debug.Log ("download image");
            DownloadImage (api, fileName, callback);
        }
    }

    static void DownloadImage (string api_url, string fileName, Action<Sprite> callback)
    {

        if (!ImageDownloadProcess.HasProcess (fileName)) {
            var data = new Hashtable();
            data.Add("size", size.ToString());
            var server = new Server (api_url, BestHTTP.HTTPMethods.Get, data, (isSuccess, request, response) => {
                Texture2D tex = null;
                if (isSuccess) {
                    switch (response.StatusCode) {
                    case 200:
                    case 304:
                        tex = response.DataAsTexture2D;
                        break;
                    }
                }

                if (tex == null) {
                    // TODO failure case
                    ImageDownloadProcess.DoProcess (fileName, null);
                } else {
                    var sprite = ConvetTexToSprite (tex);
                    ImageDownloadProcess.DoProcess (fileName, sprite);
                    if (!HasImage (fileName)) {
                        SaveImage (response.Data, fileName);
                        ImageStore.SetSprite (fileName, sprite);
                    }
                }
            });
            server.Call (1);
        }
        ImageDownloadProcess.AddProcess (fileName, callback);
    }

    static void SaveImage (byte [] data, string name)
    {
        var filePath = Path.Combine (IMAGE_PATH, name);
        Debug.Log ("save image : " + filePath);
        Directory.CreateDirectory (IMAGE_PATH);
        FileStream fileSave;
        fileSave = new FileStream (filePath, FileMode.Create);

        BinaryWriter binary;
        binary = new BinaryWriter (fileSave);
        binary.Write (data);
        fileSave.Close ();
    }

    static Sprite LoadImage (string name)
    {
        Debug.Log ("load image : " + IMAGE_PATH + name);
        byte [] bytes;
        bytes = File.ReadAllBytes (IMAGE_PATH + name);
        var tex = new Texture2D (1, 1);
        tex.LoadImage (bytes);
        ImageStore.SetSprite (name, ConvetTexToSprite (tex));
        return ConvetTexToSprite (tex);
    }

    static bool HasImage (string name)
    {
        var filePath = Path.Combine (IMAGE_PATH, name);
        return File.Exists (filePath);
    }

    static Sprite ConvetTexToSprite (Texture2D tex)
    {
        return Sprite.Create (tex, new Rect (0, 0, tex.width, tex.height), Vector2.one * 0.5f);
    }

    public static void DeleteAllImages ()
    {
        DirectoryInfo di = new DirectoryInfo (IMAGE_PATH);
        if (di.Exists) {
            di.Delete (true);
        }
    }

    public static void DeleteImage(string fileName)
    {
        if (HasImage(fileName))
        {
            var filePath = Path.Combine(IMAGE_PATH, fileName);
            File.Delete(filePath);
        }

        if (ImageStore.HasSprite(fileName))
        {
            ImageStore.DeleteSprite(fileName);
        }
    }

    protected static string CreateFileName(string prefix, string name)
    {
        string fileName = prefix + name;
        if(size != ImageSizes.orig)
            fileName = fileName + "_" + size.ToString();
        
        return fileName;
    }
}

public enum ImageSizes
{
    orig,
    icon,
    icon2x,
    thumb,
    detail
}

