﻿using System;
using UnityEngine;

public class InstructorBackgroundImageLoader : ImageLoader
{
    static string API_BASE = "image/instructor_background/";
    static string IMAGE_PREFIX = "image_instructor_background_";

    public static void GetImage (string id, Action<Sprite> callback)
    {
        string api = API_BASE + id;
        var fileName = IMAGE_PREFIX + id;
        GetImage (fileName, api, callback);
    }

    public static void SaveSprite (string id, Sprite sprite)
    {
        var fileName = IMAGE_PREFIX + id;
        ImageStore.SetSprite (fileName, sprite);
    }
}
