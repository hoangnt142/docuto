﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VideoThumbLoader : ImageLoader
{
    static string API_BASE = "image/video/";
    static string IMAGE_PREFIX = "course_video_thumb_";

    public static void GetImage(string id, Action<Sprite> callback)
    {
        string api = API_BASE + id;
        var fileName = IMAGE_PREFIX + id;
        GetImage(fileName, api, callback);
    }

    public static void SaveSprite(string id, Sprite sprite)
    {
        var fileName = IMAGE_PREFIX + id;
        ImageStore.SetSprite(fileName, sprite);
    }

    public static void RemoveImage(string id)
    {
        var fileName = IMAGE_PREFIX + id;
        DeleteImage(fileName);
    }
}
