﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageLoaderTest : MonoBehaviour
{
    public string userId;
    public string token;
    public List<Image> images;

    void Awake ()
    {
        if (token.Length > 0) {
            Session.Token = token;
        }
    }

    void Start ()
    {
        foreach (var img in images) {
            ProfileImageLoader.GetImage (userId, ImageSizes.detail, (Sprite sprite) => {
                img.sprite = sprite;
                Debug.Log ("do callback : " + img.gameObject.name);
            });
        }
    }
}
