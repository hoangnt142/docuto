﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class ImageDownloadProcess
{

    static Dictionary<string, Process> processDic = new Dictionary<string, Process> ();

    public static void AddProcess (string name, Action<Sprite> callback)
    {
        if (HasProcess (name)) {
            processDic [name].callbacks += callback;
        } else {
            var process = new Process ();
            process.callbacks += callback;
            processDic.Add (name, process);
        }
    }

    public static void DoProcess (string name, Sprite sprite)
    {
        if (HasProcess (name)) {
            processDic [name].Do (sprite);
            processDic.Remove (name);
        }
    }

    public static bool HasProcess (string name)
    {
        return processDic.ContainsKey (name);
    }

    class Process
    {
        public event Action<Sprite> callbacks;
        public void Do (Sprite sprite)
        {
            callbacks (sprite);
        }
    }
}
