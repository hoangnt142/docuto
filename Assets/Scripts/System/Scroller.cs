﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scroller : MonoBehaviour
{
    [SerializeField]
    RectTransform contentRectTranform;


    void Start()
    {
        RectTransform rectTranform = (RectTransform)gameObject.transform;

        // disable scroll if the device is long
        if (contentRectTranform.rect.height < rectTranform.rect.height)
        {
            ScrollRect scroll = gameObject.GetComponent<ScrollRect>();
            scroll.vertical = false;
        }
    }
}
