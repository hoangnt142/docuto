﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ApplicationSystem : MonoBehaviour
{

    [SerializeField]
    bool debugTutorial = true;

    [SerializeField]
    Canvas rootCanvas;

    public class CONST
    {
        public const string KEY_IS_FIRST_RUN_TIME = "is_first_run_time";
        public const string KEY_IS_FIRST_REGISTER_INSTRUCTOR = "is_first_register_insturctor_";
        public const string AUTH_TOKEN_KEY = "auth_token";
        public const string USER_DATA_KEY = "user_data";
        public const string LOGGED_IN_EMAIL_KEY = "logged_in_email";
    }

    static ApplicationSystem instance;

    Vector2 screenSize;

    void Awake ()
    {
        Application.targetFrameRate = 60;

#if UNITY_ANDROID
        // process android status bar
        ApplicationChrome.statusBarState = ApplicationChrome.States.TranslucentOverContent;
        ApplicationChrome.navigationBarState = ApplicationChrome.States.Hidden;
#endif
    }

    private void Start ()
    {
        instance = this;

        if (debugTutorial || !PlayerPrefs.HasKey (CONST.KEY_IS_FIRST_RUN_TIME) || PlayerPrefs.GetInt (CONST.KEY_IS_FIRST_RUN_TIME) != 1) {
            TutorialModal.Show(TutorialModal.TutorialType.commonUser, () =>
            {
                PageManager.ShowInitialPage();
            });
            PlayerPrefs.SetInt (CONST.KEY_IS_FIRST_RUN_TIME, 1);
        } else {
            PageManager.ShowInitialPage ();
        }

        GetScreenSize();
        StartupConfigs();
    }

    void GetScreenSize()
    {
        //if (!screenSize.Equals(default(Vector2))) return;

        GameObject _hiddenObject = new GameObject("HiddenObject");
        _hiddenObject.SetActive(false);
        _hiddenObject.transform.SetParent(rootCanvas.transform);
        RectTransform rect = _hiddenObject.AddComponent<RectTransform>();
        rect.pivot = new Vector2(0.5f, 0.5f);
        rect.localScale = Vector3.one;
        rect.offsetMin = new Vector2(0, 0);
        rect.offsetMax = new Vector2(0, 0);

        rect.anchorMin = new Vector2(0, 0);
        rect.anchorMax = new Vector2(0, 1);
        float screenHeight = rect.rect.height;

        rect.anchorMin = new Vector2(0, 0);
        rect.anchorMax = new Vector2(1, 0);
        float screenWidth = rect.rect.width;

        screenSize = new Vector2(screenWidth, screenHeight);
        Destroy(_hiddenObject);
    }

    // This is for Android
    public static int GetAndroidKeyboardHeight ()
    {
        using (AndroidJavaClass UnityClass = new AndroidJavaClass ("com.unity3d.player.UnityPlayer")) {
            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject> ("currentActivity").Get<AndroidJavaObject> ("mUnityPlayer").Call<AndroidJavaObject> ("getView");

            using (AndroidJavaObject Rct = new AndroidJavaObject ("android.graphics.Rect")) {
                View.Call ("getWindowVisibleDisplayFrame", Rct);

                return Screen.height - Rct.Call<int> ("height");
            }
        }
    }

    public static Canvas RootCanvas
    {
        get { return instance.rootCanvas; }
    }

    public static Vector2 ScreenSize
    {
        get { return instance.screenSize; }
    }

    static void StartupConfigs()
    {
        // for JSonMapper register importers
        LitJson.JsonMapper.RegisterImporter<int, long>((int value) =>
        {
            return (long)value;
        });
    }

    public static bool IsDebugTutorial()
    {
        return instance.debugTutorial;
    }
}
