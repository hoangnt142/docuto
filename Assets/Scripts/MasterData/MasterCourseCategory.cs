﻿using System;
using System.Collections.Generic;
using LitJson;

public class MasterCourseCategory : MasterData<CourseCategoryData>
{

    public static void GetData (Action<List<CourseCategoryData>> onSuccess, Action onFail = null)
    {
        if (HasData) {
            onSuccess (data);
        } else {
            Load ("course_categories", onSuccess, onFail);
        }
    }

    public static int GetCategoryIndex(int categoryId)
    {
        for (int i = 0; i < data.Count; i++)
        {
            CourseCategoryData category = data[i];
            if(category.id == categoryId)
            {
                return i;
            }
        }
        return -1;
    }


    public static void Clear()
    {
        if (data != null) data.Clear();
    }
}