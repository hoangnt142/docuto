﻿using System;
using System.Collections.Generic;
using LitJson;
using UnityEngine;
public class MasterData<T>
{
    public static List<T> data;

    public static bool HasData {
        get {
            return data != null && data.Count > 0;
        }
    }

    protected static void Load (string endpoint, Action<List<T>> onSuccess, Action onFail)
    {
        var api = new API (endpoint, BestHTTP.HTTPMethods.Get, data);
        api.onSuccess = (dataJson) => {
            try {
                data = JsonMapper.ToObject<List<T>> (dataJson);
                onSuccess (data);
            } catch (Exception e) {
                Debug.Log (endpoint + " error: " + e.Message);
                onFail ();
            }
        };
        api.onError = (errorJson) => {
            if (onFail != null) {
                onFail ();
            }
        };
        api.Call ();
    }
}
