﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProgressBar : MonoBehaviour
{
    [SerializeField]
    GameObject content;

    [SerializeField]
    Slider bar;

    [SerializeField]
    Text message;

    static ProgressBar instance;

    void Awake()
    {
        instance = this;
        content.SetActive(false);
    }

    public static void Show()
    {
        HeaderView.DisableGraphicRaycaster();
        PageManager.ActiveBackPage(false);
        if (PageManager.CurrentPage != null)
            PageManager.CurrentPage.HideAllNativeEditBox();

        instance.content.SetActive(true);
    }

    public static void Show(float percent)
    {
        Show();
        instance.bar.value = percent;
    }

    public static void Show(float percent, string msg)
    {
        Show(percent);

        instance.message.text = msg;
    }

    public static void Hide()
    {
        HeaderView.EnableGraphicRaycaster();
        PageManager.ActiveBackPage(true);
        if (PageManager.CurrentPage != null)
            PageManager.CurrentPage.ShowAllNativeEditBox();

        instance.bar.value = 0;
        instance.content.SetActive(false);
    }

    public static void SetValue(float percent)
    {
        instance.bar.value = percent;
    }
}
