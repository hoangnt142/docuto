﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TutorialModal : MonoBehaviour
{
    [SerializeField]
    GameObject content, nextButton, indicator;

    [SerializeField]
    float padding = 24f, moveSpeed = 15f;

    [SerializeField]
    List<Sprite> userTutorials;

    [SerializeField]
    List<Sprite> instructorTutorials;

    [SerializeField]
    Image buttonImage;

    [SerializeField]
    Sprite nextButtonSprite, startButtonSprite, activeIndicatorSprite, inactiveIndicatorSprite, roundBorderSprite;

    [SerializeField]
    Color activeColor = new Color(117, 203, 199, 255), inactiveColor = new Color(154, 153, 154, 255);

    static TutorialModal instance;
    static TutorialType currentType;
    static int currentPage;
    static bool isDoneShow;
    static bool isInitTutorial;

    List<GameObject> items;
    List<GameObject> indicatorItems;
    RectTransform scrollPanel;
    ScrollRect scrollRect;
    Action finishedAction;
    Vector2 initPosition;
    int selectedIndex;

    public enum TutorialType
    {
        commonUser,
        instructor
    }

    void Awake()
    {
        instance = this;
        scrollRect = gameObject.GetComponent<ScrollRect>();
        scrollPanel = scrollRect.content;
        initPosition = scrollPanel.anchoredPosition;
		items = new List<GameObject>();
		indicatorItems = new List<GameObject>();
    }

    void Start()
    {
        currentPage = 0;
        currentType = TutorialType.commonUser;
    }

    void Update()
    {
        if(!isDoneShow && isInitTutorial)
        {
            LerpToCenter(-items[selectedIndex].GetComponent<RectTransform>().anchoredPosition.x);
        }
    }

    void CreateIndicatorObjects(int number)
    {
        // create indicator objects
        for (int i = 0; i < number; i++)
        {
            GameObject item = new GameObject("Item " + i);
            RectTransform itemRect = item.AddComponent<RectTransform>();
            Image itemImage = item.AddComponent<Image>();
            item.transform.SetParent(indicator.transform);
            itemRect.sizeDelta = new Vector2(16.2f, 16.2f);
            itemRect.localScale = Vector3.one;
            itemImage.sprite = instance.inactiveIndicatorSprite;

            // because round image is not smooth, so add circle border
            GameObject itemBorder = new GameObject("Circle Border");
            RectTransform itemBorderRect = itemBorder.AddComponent<RectTransform>();
            Image itemBorderImage = itemBorder.AddComponent<Image>();
            itemBorder.transform.SetParent(item.transform);
            itemBorderRect.pivot = new Vector2(0.5f, 0.5f);
            itemBorderRect.anchorMin = new Vector2(0, 0);
            itemBorderRect.anchorMax = new Vector2(1, 1);
            itemBorderRect.offsetMin = new Vector2(-0.8f, -0.8f);
            itemBorderRect.offsetMax = new Vector2(0.8f, 0.8f);
            itemBorderRect.localScale = Vector3.one;
            itemBorderImage.sprite = instance.roundBorderSprite;
            itemBorderImage.color = instance.inactiveColor;

            indicatorItems.Add(item);
        }
    }

    public static void Show(TutorialType type, Action finishAction)
    {
        isInitTutorial = false;
        // clear indicator
        foreach (Transform child in instance.indicator.transform)
            Destroy(child.gameObject);
        // clear content
        foreach (Transform child in instance.content.transform)
            Destroy(child.gameObject);
        instance.items.Clear();
        instance.indicatorItems.Clear();
        instance.scrollPanel.anchoredPosition = instance.initPosition;

        // create objects
        if(type == TutorialType.commonUser)
        {
            // setup indicators
            instance.CreateIndicatorObjects(instance.userTutorials.Count);

            // create game objects
            for (int i = 0; i < instance.userTutorials.Count; i++)
            {
                GameObject turotiralImageObject = new GameObject("TurotiralImage " + i);
                //turotiralImageObject.SetActive(false);
                turotiralImageObject.transform.SetParent(instance.content.transform);

                RectTransform rectTranf = turotiralImageObject.AddComponent<RectTransform>();
                turotiralImageObject.AddComponent<Image>().sprite = instance.userTutorials[i];

                rectTranf.anchorMin = Vector2.zero;
                rectTranf.anchorMax = Vector2.one;
                rectTranf.offsetMax = Vector2.zero;
                rectTranf.offsetMin = Vector2.zero;
                rectTranf.localScale = Vector3.one;

                rectTranf.anchoredPosition = new Vector2(i * (rectTranf.rect.size.x + instance.padding), rectTranf.anchoredPosition.y);

                instance.items.Add(turotiralImageObject);
            }
        }
        if(type == TutorialType.instructor)
        {
            // setup indicators
            instance.CreateIndicatorObjects(instance.instructorTutorials.Count);

            // create game objects
            for (int i = 0; i < instance.instructorTutorials.Count; i++)
            {
                GameObject turotiralImageObject = new GameObject("TurotiralImage " + i);
                //turotiralImageObject.SetActive(false);
                turotiralImageObject.transform.SetParent(instance.content.transform);

                RectTransform rectTranf = turotiralImageObject.AddComponent<RectTransform>();
                turotiralImageObject.AddComponent<Image>().sprite = instance.instructorTutorials[i];

                rectTranf.anchorMin = Vector2.zero;
                rectTranf.anchorMax = Vector2.one;
                rectTranf.offsetMax = Vector2.zero;
                rectTranf.offsetMin = Vector2.zero;
                rectTranf.localScale = Vector3.one;

                rectTranf.anchoredPosition = new Vector2(i * (rectTranf.rect.size.x + instance.padding), rectTranf.anchoredPosition.y);

                instance.items.Add(turotiralImageObject);
            }
        }

        currentType = type;
        currentPage = 0;
        isInitTutorial = true;
        instance.content.SetActive(true);
        instance.nextButton.SetActive(true);
        instance.scrollRect.enabled = false;
        // I don't know why header can not overlapped by this modal
        //instance.header.SetActive(false);
        HeaderView.Hide();
        instance.finishedAction = finishAction;

        _Show(0);
    }

    static void _Show(int page)
    {
        if (instance.userTutorials == null || instance.instructorTutorials == null) return;

        if ((currentType == TutorialType.commonUser && page < instance.userTutorials.Count - 1) || 
            (currentType == TutorialType.instructor && page < instance.instructorTutorials.Count - 1))
            instance.buttonImage.sprite = instance.nextButtonSprite;
        else
            instance.buttonImage.sprite = instance.startButtonSprite;

        if ((currentType == TutorialType.commonUser && page >= instance.userTutorials.Count) || 
            (currentType == TutorialType.instructor && page >= instance.instructorTutorials.Count))
        {
            instance.content.SetActive(false);
            instance.nextButton.SetActive(false);
            //instance.header.SetActive(true);
            HeaderView.Show();

            instance.finishedAction();
            return;
        }

        Debug.Log("Show tutorial page " + page);
        isDoneShow = false;
        instance.scrollRect.enabled = true;
        instance.selectedIndex = page;
    }

    public void Next()
    {
        currentPage++;
        _Show(currentPage);
    }

    void LerpToCenter(float position)
    {
        float newX = Mathf.Lerp(scrollPanel.anchoredPosition.x, position, Time.deltaTime * moveSpeed);

        if (Mathf.Abs(position - newX) < 4.2f)
        {
            newX = position;

            SelectIndicator(selectedIndex);
        }

        Vector2 newPosition = new Vector2(newX, scrollPanel.anchoredPosition.y);
        scrollPanel.anchoredPosition = newPosition;

        if (Mathf.Abs(newX) >= Mathf.Abs(position) - 1f && Mathf.Abs(newX) <= Mathf.Abs(position) + 1f)
        {
            isDoneShow = true;
            scrollRect.enabled = false;
        }
    }

    void SelectIndicator(int index)
    {
        // inactive all
        foreach(GameObject item in indicatorItems)
        {
            item.GetComponent<Image>().sprite = inactiveIndicatorSprite;
            item.transform.GetChild(0).GetComponent<Image>().color = inactiveColor;
        }

        // active index
        indicatorItems[index].GetComponent<Image>().sprite = activeIndicatorSprite;
        indicatorItems[index].transform.GetChild(0).GetComponent<Image>().color = activeColor;
    }
}
