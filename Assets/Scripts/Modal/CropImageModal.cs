﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using HedgehogTeam.EasyTouch;

public class CropImageModal : MonoBehaviour
{
    const float MASK_CIRCLE_CROP_SIZE = 256;
    const float MASK_RECTANGLE_CROP_WIDTH = 256;
    const float MASK_RECTANGLE_CROP_HEIGHT = 256;

    [SerializeField]
    GameObject mainContent;

    [SerializeField]
    RectTransform cropArea;

    [SerializeField]
    GameObject scrollContent;

    [SerializeField]
    Image sourceImage;

    [SerializeField]
    Image maskImage;

    [SerializeField]
    Texture2D roundedMask;

    [SerializeField]
    Texture2D rectangleMask;

    Vector2 distanceCourseImage;
    bool isDragging;
    bool moveCourseImage;
    bool moveCourseImageToLeft;
    bool moveCourseImageToRight;
    bool moveCourseImageUp;
    bool moveCourseImageDown;
    float padding = 3;
    float moveSpeed = 1.5f;
    float cropScale = 1;
    float cropAreaWidth;
    float cropAreaHeight;
    float cropSizeWidth;
    float cropSizeHeight;

    Vector2 _cropSize;
    bool _toRounded;

    public static Action<Sprite> onCrop;
    static CropImageModal instance;

    public static void ShowCropImage(Texture2D sourceTex, float circleDiameter)
    {
        ShowCropImage(sourceTex, new Vector2(circleDiameter, circleDiameter), true);
    }

    public static void ShowCropImage(Texture2D sourceTex, Vector2 cropSize, bool toRounded = false)
    {
        instance.Reset();
        instance.mainContent.SetActive(true);
        instance.sourceImage.sprite = Sprite.Create(sourceTex, new Rect(0, 0, sourceTex.width, sourceTex.height), Vector2.one * 0.5f);

        instance._cropSize = cropSize;
        instance._toRounded = toRounded;

        float maskScaleX = 1;
        float maskScaleY = 1;
        if (toRounded)
        {
            float circleCropSize = Math.Max(cropSize.x, cropSize.y);

            if (circleCropSize < MASK_CIRCLE_CROP_SIZE || circleCropSize > instance.cropAreaWidth || circleCropSize > instance.cropAreaHeight)
            {
                circleCropSize = MASK_CIRCLE_CROP_SIZE;
            }

            instance.cropScale = circleCropSize / Math.Max(cropSize.x, cropSize.y);
            instance.cropSizeWidth = circleCropSize;
            instance.cropSizeHeight = circleCropSize;
        }
        else
        {
            if (cropSize.x > cropSize.y)
            {
                maskScaleX = cropSize.x / cropSize.y;
                instance.cropScale = MASK_RECTANGLE_CROP_HEIGHT / cropSize.y;
            }
            else if (cropSize.y > cropSize.x)
            {
                maskScaleY = cropSize.y / cropSize.x;
                instance.cropScale = MASK_RECTANGLE_CROP_WIDTH / cropSize.x;
            }

            float scalePercent = 0.95f;
            float newCropSizeWidth = MASK_RECTANGLE_CROP_HEIGHT * maskScaleX;
            float newCropSizeHeight = MASK_RECTANGLE_CROP_WIDTH * maskScaleY;
            // I. crop size large than crop area
            if (newCropSizeWidth >= instance.cropAreaWidth)
            {
                newCropSizeWidth = instance.cropAreaWidth * scalePercent;
                newCropSizeHeight = newCropSizeWidth * cropSize.y / cropSize.x;
                instance.cropScale = newCropSizeWidth / cropSize.x;
            }
            else if (newCropSizeHeight >= instance.cropAreaHeight)
            {
                newCropSizeHeight = instance.cropAreaHeight * scalePercent;
                newCropSizeWidth = newCropSizeHeight * cropSize.x / cropSize.y;
                instance.cropScale = newCropSizeHeight / cropSize.y;
            }

            // II. crop size smaller than crop area.
            // this will be process only I. processed. Because cropScaleX and cropScaleY always >= 1
            if (newCropSizeWidth < MASK_RECTANGLE_CROP_WIDTH)
            {
                newCropSizeWidth = MASK_RECTANGLE_CROP_WIDTH;
                newCropSizeHeight = newCropSizeWidth * cropSize.y / cropSize.x;
                instance.cropScale = newCropSizeWidth / cropSize.x;
            }
            else if (newCropSizeHeight < MASK_RECTANGLE_CROP_HEIGHT)
            {
                newCropSizeHeight = MASK_RECTANGLE_CROP_HEIGHT;
                newCropSizeWidth = newCropSizeHeight * cropSize.y / cropSize.x;
                instance.cropScale = newCropSizeHeight / cropSize.y;
            }

            // III. compare with source image and source image with crop area
            if (newCropSizeWidth > sourceTex.width || cropSize.x >= sourceTex.width)
            {
                newCropSizeWidth = sourceTex.width;
                if (sourceTex.width >= instance.cropAreaWidth) newCropSizeWidth = instance.cropAreaWidth * scalePercent;

                newCropSizeHeight = newCropSizeWidth * cropSize.y / cropSize.x;
                if (newCropSizeHeight > sourceTex.height)
                {
                    newCropSizeHeight = sourceTex.height;
                }
                instance.cropScale = newCropSizeWidth / sourceTex.width;
            }
            else if (newCropSizeHeight > sourceTex.height || cropSize.y >= sourceTex.height)
            {
                newCropSizeHeight = sourceTex.height;
                if (sourceTex.height >= instance.cropAreaHeight) newCropSizeHeight = instance.cropAreaHeight * scalePercent;

                newCropSizeWidth = newCropSizeHeight * cropSize.x / cropSize.y;
                if (newCropSizeWidth > sourceTex.width)
                {
                    newCropSizeWidth = sourceTex.width;
                }
                instance.cropScale = newCropSizeHeight / sourceTex.height;
            }

            maskScaleX = newCropSizeWidth / MASK_RECTANGLE_CROP_WIDTH;
            maskScaleY = newCropSizeHeight / MASK_RECTANGLE_CROP_HEIGHT;

            instance.cropSizeWidth = newCropSizeWidth;
            instance.cropSizeHeight = newCropSizeHeight;
        }

        instance.SetMask(toRounded, new Vector2(maskScaleX, maskScaleY));
        instance.CalculateComponentsSize();
        HeaderView.DisableGraphicRaycaster();

        EasyTouch.On_Pinch += instance.On_Pinch;
        EasyTouch.On_TouchDown2Fingers += instance.On_TouchStart;
        EasyTouch.On_TouchUp2Fingers += instance.On_TouchEnd;
        PageManager.CurrentPage.HideAllNativeEditBox();
    }

    public void Crop()
    {
        Vector2 zoomScale = sourceImage.rectTransform.localScale;
        Vector2 croppedSize = new Vector2(cropSizeWidth / cropScale, cropSizeHeight / cropScale);
        float croppedPositionX = (sourceImage.rectTransform.rect.width * zoomScale.x - cropSizeWidth) / 2 + scrollContent.transform.localPosition.x - sourceImage.rectTransform.localPosition.x;
        float croppedPositionY = (sourceImage.rectTransform.rect.height * zoomScale.y - cropSizeHeight) / 2 + scrollContent.transform.localPosition.y - sourceImage.rectTransform.localPosition.y;

        if (croppedPositionX < 0) croppedPositionX = 0;
        if (croppedPositionY < 0) croppedPositionY = 0;
        Vector2 croppedPosition = new Vector2(croppedPositionX / cropScale, croppedPositionY / cropScale);
        Sprite sprite = CropImage.Crop(sourceImage, croppedSize, croppedPosition, zoomScale);
        if (onCrop != null)
        {
            onCrop(sprite);
        }

        Close();
    }

    public void Close()
    {
        onCrop = null;
        mainContent.SetActive(false);
        HeaderView.EnableGraphicRaycaster();

        EasyTouch.On_Pinch -= On_Pinch;
        EasyTouch.On_TouchDown2Fingers -= On_TouchStart;
        EasyTouch.On_TouchUp2Fingers -= On_TouchEnd;
        if (PageManager.CurrentPage != null)
            PageManager.CurrentPage.ShowAllNativeEditBox();
    }

    void Reset()
    {
        cropScale = 1;
        distanceCourseImage = Vector2.zero;

        scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(256, 256);
        scrollContent.GetComponent<RectTransform>().localPosition = Vector2.zero;
        scrollContent.GetComponent<RectTransform>().localScale = Vector2.one;
        sourceImage.rectTransform.sizeDelta = new Vector2(256, 256);
        sourceImage.rectTransform.localPosition = Vector2.zero;
        sourceImage.rectTransform.localScale = Vector2.one;
        maskImage.rectTransform.sizeDelta = new Vector2(1080, 1080);
    }

    void On_TouchStart(Gesture gesture)
    {
        cropArea.GetComponent<ScrollRect>().enabled = false;
        isDragging = false;
    }

    void On_TouchEnd(Gesture gesture)
    {
        cropArea.GetComponent<ScrollRect>().enabled = true;
    }

    void On_Pinch(Gesture gesture)
    {
        float deltaZoom = gesture.deltaPinch * 0.001f;
        Vector2 zoomScale = sourceImage.rectTransform.localScale;
        //return case
        if ((zoomScale.x < 0 || zoomScale.y < 0 || (zoomScale.x > 5 && deltaZoom > 0) || (zoomScale.y > 5 && deltaZoom > 0)) ||
            (sourceImage.rectTransform.rect.width * zoomScale.x <= cropSizeWidth && deltaZoom < 0) ||
            (sourceImage.rectTransform.rect.height * zoomScale.y <= cropSizeHeight && deltaZoom < 0)
        ) return;

        //sourceImage.rectTransform.sizeDelta *= (1 + deltaZoom);
        // move scroll content to center of the image
        Vector2 newCourseSize = Vector2.Scale(sourceImage.rectTransform.rect.size, zoomScale);
        Vector3 currentCoursePosition = sourceImage.rectTransform.localPosition;
        Vector3 newCoursePosition = currentCoursePosition;
        if (
            (newCourseSize.x < cropAreaWidth || newCourseSize.y < cropAreaHeight) ||
            (Mathf.Abs(currentCoursePosition.x) > (newCourseSize.x / 2 - cropAreaWidth * 3 / 4)) ||
            (Mathf.Abs(currentCoursePosition.y) > (newCourseSize.y / 2 - cropAreaHeight * 3 / 4))
           )
        {
            newCoursePosition.x = Mathf.Lerp(currentCoursePosition.x, 0, Time.deltaTime * 4.5f);
            newCoursePosition.y = Mathf.Lerp(currentCoursePosition.y, 0, Time.deltaTime * 4.5f);
            sourceImage.rectTransform.localPosition = newCoursePosition;

        }
        Vector3 currentContentPosition = scrollContent.GetComponent<RectTransform>().localPosition;
        if (Mathf.Abs(currentContentPosition.x) + cropSizeWidth / 2 > Mathf.Abs(newCoursePosition.x) + newCourseSize.x / 2)
        {
            Vector3 newContentPosition = new Vector3();
            //newContentPosition.x = Mathf.Lerp(currentContentPosition.x, newCoursePosition.x + newCourseSize.x / 2 - cropSizeWidth / 2, Time.deltaTime * scaleMoveSpeed);
            newContentPosition.x = Mathf.Lerp(currentContentPosition.x, 0, Time.deltaTime * 19f);
            newContentPosition.y = currentContentPosition.y;
            scrollContent.GetComponent<RectTransform>().localPosition = newContentPosition;
        }
        if (Mathf.Abs(currentContentPosition.y) + cropSizeHeight / 2 > Mathf.Abs(newCoursePosition.y) + newCourseSize.y / 2)
        {
            Vector3 newContentPosition = new Vector3();
            //newContentPosition.y = Mathf.Lerp(currentContentPosition.y, newCoursePosition.y + newCourseSize.y / 2 - cropSizeHeight, Time.deltaTime * scaleMoveSpeed);
            newContentPosition.y = Mathf.Lerp(currentContentPosition.y, 0, Time.deltaTime * 19f);
            newContentPosition.x = currentContentPosition.x;
            scrollContent.GetComponent<RectTransform>().localPosition = newContentPosition;
        }

        sourceImage.rectTransform.localScale += new Vector3(deltaZoom, deltaZoom, 0);
        distanceCourseImage += sourceImage.rectTransform.sizeDelta * deltaZoom / 2;

        CalculateScrollContentSize();
    }

    void Awake()
    {
        instance = this;
        cropAreaWidth = cropArea.rect.width;
        cropAreaHeight = cropArea.rect.height;

        mainContent.SetActive(false);
    }

    void Update()
    {
        if (isDragging && moveCourseImage)
        {
            float courseY = sourceImage.rectTransform.localPosition.y;
            if (distanceCourseImage.y < 0) moveCourseImageUp = moveCourseImageDown = false;
            float newY = courseY;

            if (moveCourseImageUp)
            {
                newY = Mathf.Lerp(courseY, distanceCourseImage.y, Time.deltaTime * moveSpeed);
            }

            if (moveCourseImageDown)
            {
                newY = Mathf.Lerp(courseY, -distanceCourseImage.y, Time.deltaTime * moveSpeed);
            }

            float courseX = sourceImage.rectTransform.localPosition.x;
            if (distanceCourseImage.x < 0) moveCourseImageToLeft = moveCourseImageToRight = false;
            float newX = courseX;

            if (moveCourseImageToLeft)
            {
                newX = Mathf.Lerp(courseX, -distanceCourseImage.x, Time.deltaTime * moveSpeed);
            }

            if (moveCourseImageToRight)
            {
                newX = Mathf.Lerp(courseX, distanceCourseImage.x, Time.deltaTime * moveSpeed);
            }

            sourceImage.rectTransform.localPosition = new Vector3(newX, newY);
        }
    }

    public void OnSwipe(Vector2 vector)
    {
        Vector3 scrollPosition = scrollContent.transform.localPosition;

        // move vertical
        float distanceWidth = (cropAreaWidth - cropSizeWidth) / 2 - padding;
        if (scrollPosition.x >= distanceWidth)
        {
            moveCourseImage = true;
            moveCourseImageToLeft = true;
        }
        else moveCourseImageToLeft = false;

        if (scrollPosition.x <= -distanceWidth)
        {
            moveCourseImage = true;
            moveCourseImageToRight = true;
        }
        else moveCourseImageToRight = false;

        // move hozizontal
        float distanceHeight = (cropAreaHeight - cropSizeHeight) / 2 - padding;
        if (scrollPosition.y <= -distanceHeight)
        {
            moveCourseImage = true;
            moveCourseImageUp = true;
        }
        else moveCourseImageUp = false;

        if (scrollPosition.y >= distanceHeight)
        {
            moveCourseImage = true;
            moveCourseImageDown = true;
        }
        else moveCourseImageDown = false;

        //stop move if the crop object is on center
        if (!moveCourseImageUp && !moveCourseImageDown && !moveCourseImageToLeft && !moveCourseImageToRight)
            moveCourseImage = false;
    }

    public void OnStartDrag()
    {
        isDragging = true;
    }

    public void OnEndDrad()
    {
        isDragging = false;
        moveCourseImage = false;
    }

    //void SetMask (Image maskImage, Image maskSubImage = null)
    void SetMask(bool toRounded, Vector2 scale)
    {
        if (toRounded)
        {
            maskImage.sprite = Sprite.Create(roundedMask, new Rect(0, 0, roundedMask.width, roundedMask.height), Vector2.one * 0.5f);
        }
        else
        {
            maskImage.sprite = Sprite.Create(rectangleMask, new Rect(0, 0, rectangleMask.width, rectangleMask.height), Vector2.one * 0.5f);

            Rect maskRect = maskImage.rectTransform.rect;
            maskImage.rectTransform.sizeDelta = new Vector2(maskRect.width * scale.x, maskRect.height * scale.y);
        }

    }

    void CalculateComponentsSize()
    {
        sourceImage.SetNativeSize();
        Vector2 vecetorScale = new Vector2(cropScale, cropScale);
        sourceImage.rectTransform.sizeDelta = Vector2.Scale(new Vector2(sourceImage.rectTransform.rect.width, sourceImage.rectTransform.rect.height), vecetorScale);

        CalculateScrollContentSize();

        distanceCourseImage = new Vector2();
        distanceCourseImage.x = (sourceImage.rectTransform.rect.width - cropAreaWidth) / 2;
        distanceCourseImage.y = (sourceImage.rectTransform.rect.height - cropAreaHeight) / 2;
    }

    void CalculateScrollContentSize()
    {
        float newWidth = cropAreaWidth + (cropAreaWidth - cropSizeWidth);
        float newHeight = cropAreaHeight + (cropAreaHeight - cropSizeHeight);

        Vector2 zoomScale = sourceImage.rectTransform.localScale;
        if (sourceImage.rectTransform.rect.width * zoomScale.x < cropAreaWidth)
        {
            newWidth = newWidth - (cropAreaWidth - sourceImage.rectTransform.rect.width * zoomScale.x);
        }

        if (sourceImage.rectTransform.rect.height * zoomScale.y < cropAreaHeight)
        {
            newHeight = newHeight - (cropAreaHeight - sourceImage.rectTransform.rect.height * zoomScale.y);
        }
        scrollContent.GetComponent<RectTransform>().sizeDelta = new Vector2(newWidth, newHeight);
    }

    public void RotateImage()
    {
        // rotate the Texture2D
        // sourceImage.transform.RotateAround(sourceImage.rectTransform.position, Vector3.back, 90);
        Texture2D sourceTexture = sourceImage.sprite.texture;
        sourceTexture = RotateTexture(sourceTexture, true);

        // Reset the crop image display
        ShowCropImage(sourceTexture, _cropSize, _toRounded);
    }

    Texture2D RotateTexture(Texture2D originalTexture, bool clockwise)
    {
        Color32[] original = originalTexture.GetPixels32();
        Color32[] rotated = new Color32[original.Length];
        int w = originalTexture.width;
        int h = originalTexture.height;
 
        int iRotated, iOriginal;
 
        for (int j = 0; j < h; ++j)
        {
            for (int i = 0; i < w; ++i)
            {
                iRotated = (i + 1) * h - j - 1;
                iOriginal = clockwise ? original.Length - 1 - (j * w + i) : j * w + i;
                rotated[iRotated] = original[iOriginal];
            }
        }
 
        Texture2D rotatedTexture = new Texture2D(h, w);
        rotatedTexture.SetPixels32(rotated);
        rotatedTexture.Apply();
        return rotatedTexture;
    }

    public static bool IsShowing()
    {
        return instance.mainContent.activeInHierarchy;
    }
}
