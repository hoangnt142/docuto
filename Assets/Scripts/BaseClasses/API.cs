﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Text;
using BestHTTP;
using LitJson;

public class API : IDisposable
{
    static string BASE_API = "api/v1/";

    public delegate void OnSuccessDelegate (string dataJson);
    public delegate void OnErrorDelegate (string errorJson);
    public delegate void OnPaginationDelegate(bool hasNext);

    public OnSuccessDelegate onSuccess;
    public OnErrorDelegate onError;
    public OnPaginationDelegate onPagination;



    string api;
    HTTPMethods method;
    object data;
    Server server;

    public API (string api, HTTPMethods method, object data)
    {
        this.api = BASE_API + api;
        this.method = method;
        this.data = data;
        this.onSuccess = (dataJson) => { };
        this.onError = (errorJson) => { };
    }

    public void Call (int priority = 0)
    {
        server = new Server (api, method, data, OnRequestFinished);
        server.Call(priority);
    }

    public void ApiCallback (bool isSuccess, JsonData data, JsonData errors)
    {
        if (isSuccess) {
            string dataString = "";
            if (data != null) dataString = JsonMapper.ToJson(data);

            onSuccess (dataString);
        } else {
            string errorString = "";
            if (errors != null)
            {
                errorString = JsonMapper.ToJson(errors);
                try
                {
                    ErrorsData errorsData = JsonMapper.ToObject<ErrorsData>(errorString);
                    switch (errorsData.error_type)
                    {
                        case 999:
                            LoginAPI.Logout(() =>
                            {
                                DialogView.Show("再ログインしてください", () =>
                                {
                                    PageManager.Show(PageType.Login);
                                    //Destroy clone page
                                    PageManager.Clean();
                                });
                            });
                            break;

                        default:
                            // do nothing, just send error string to onError delegate
                            break;
                    }
                }
                catch (Exception e)
                {
                    Debug.Log("Error " + e.Message);
                }
            }

            onError (errorString);
        }
    }

    void OnRequestFinished (bool isSuccess, HTTPRequest request, HTTPResponse response)
    {
        // in case server return error, response is not in JSON format
        // this code will throw eror and suspend process
        JsonData apiResponse = null;
        try
        {
            apiResponse = JsonMapper.ToObject(response.DataAsText);
        }
        catch (Exception e)
        {
            Debug.Log("API::OnRequestFinished error: " + e.Message);
        }

            bool responseIsSuccess = false;
            JsonData responseData = null;
            JsonData responseErrors = null;
        if (isSuccess) {
            switch (response.StatusCode) {
            case 200:
            case 304:
                if (apiResponse.Keys.Contains("is_success"))
                {
                    responseIsSuccess = (apiResponse["is_success"].ToJson().ToLower() == "true"); //apiResponse.Keys.Contains("is_success");
                    Debug.Log(apiResponse["is_success"]);
                }
                if (apiResponse.Keys.Contains("errors"))
                {
                    responseErrors = apiResponse["errors"];
                    Debug.Log(apiResponse["errors"]);
                }
                if (apiResponse.Keys.Contains("data"))
                {
                    responseData = apiResponse["data"];
                    Debug.Log(apiResponse["data"]);
                }
                if (apiResponse.Keys.Contains("pagination"))
                {
                    if(onPagination != null) onPagination(apiResponse["pagination"]["end_page"].ToJson().ToLower() != "true");
                }

                //ApiCallback (apiResponse.is_success, apiResponse.data, apiResponse.errors);
                ApiCallback (responseIsSuccess, responseData, responseErrors);
                break;
            case 422:

                if (apiResponse.Keys.Contains("errors"))
                {
                    responseErrors = apiResponse["errors"];
                    Debug.Log(apiResponse["errors"]);
                }

                ApiCallback(false, null, responseErrors);
                break;

            default:

                ApiCallback (false, null, null);
                break;
            }

        } else {
            ApiCallback (false, null, null);
        }
    }

    public void Dispose()
    {
        if(server != null) server.Dispose();
        api = null;
        data = null;
        onSuccess = (dataJson) => { };
        onError = (errorJson) => { };
        server = null;
    }
}