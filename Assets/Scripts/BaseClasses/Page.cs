﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Page : MonoBehaviour
{
    [SerializeField]
    PageType pageType;

    [SerializeField]
    MenuType menuType = MenuType.CommonUser;

    [SerializeField]
    PageManager.ContentType contentType;

    MenuType currentMenuType = MenuType.None;

    public PageType PageType {
        get {
            return pageType;
        }
    }

    public MenuType MenuType {
        get {
            if (currentMenuType == MenuType.None) {
                currentMenuType = menuType; // initialize
            }
            return currentMenuType;
        }
    }

    public PageManager.ContentType ContentType {
        get {
            return contentType;
        }
    }

    public event Action onHide = delegate { };
    public event Action onShow = delegate { };
    public List<NativeEditBoxHelper> nativeEditBoxHelpers = new List<NativeEditBoxHelper> ();

    bool isShown = false;
    protected bool IsShown {
        get {
            return isShown;
        }
    }

    public bool IsInstructorPage
    {
        get
        {
            return currentMenuType == MenuType.Instructor;
        }
    }

    public virtual void Show (MenuType overwriteMenuType = MenuType.None)
    {
        if (overwriteMenuType != MenuType.None && menuType == MenuType.Both) {
            currentMenuType = overwriteMenuType;
        }
        Show ();
        isShown = true;
    }

    public virtual void Show ()
    {
        var canvas = gameObject.GetComponent<CanvasGroup> ();
        if (canvas != null) {
            canvas.alpha = 1;
            canvas.interactable = true;
            canvas.blocksRaycasts = true;
        } else {
            gameObject.SetActive (true);
        }
        onShow ();
        isShown = true;
    }

    public virtual void Hide ()
    {
        var canvas = gameObject.GetComponent<CanvasGroup> ();
        if (canvas != null) {
            canvas.alpha = 0;
            canvas.interactable = false;
            canvas.blocksRaycasts = false;
        } else {
            gameObject.SetActive (false);
        }
        onHide ();
        isShown = false;
    }

    public virtual void Refresh()
    {
        // Must overide on instance page
    }

    public void HideAllNativeEditBox ()
    {
        foreach (var helper in nativeEditBoxHelpers) {
            helper.Hide ();
        }
    }

    public void ShowAllNativeEditBox ()
    {
        foreach (var helper in nativeEditBoxHelpers) {
            helper.Show ();
        }
    }

}
