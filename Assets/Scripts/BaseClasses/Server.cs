﻿using System;
using System.Text;
using UnityEngine;
using BestHTTP;
using LitJson;

public class Server : IDisposable
{
    const string BASE_URL = "http://160.16.231.104/";
    const double TIME_OUT = 30;

    string url_path;
    HTTPMethods method;
    object data;
    HTTPRequest request;

    public delegate void OnGetResponse (bool isSuccess, HTTPRequest request, HTTPResponse response);
    OnGetResponse onGetResponse = delegate { };

    public static string MakeUrl(string path, bool addAuthToken = false)
    {
        if (addAuthToken)
        {
            path = path + (path.IndexOf('?') >= 0 ? "&" : "?") + "auth_token=" + Session.Token;
        }
        return BASE_URL + path;
    }

    public Server (string url_path, HTTPMethods method, object data, OnGetResponse onGetResponse)
    {
        this.url_path = url_path;
        this.method = method;
        this.data = data;
        this.onGetResponse = onGetResponse;
    }

    public void Call (int priority = 0)
    {
        HTTPManager.MaxConnectionPerServer = 6;
        request = new HTTPRequest (new Uri (BASE_URL + url_path),
                                                       method, OnRequestFinished);
        request.SetHeader ("Content-Type", "application/json; charset=UTF-8");
        if (Session.HasToken ()) {
            request.SetHeader ("Authorization", Session.Token);
        }

        if (data != null) {
            var json = JsonMapper.ToJson (data);
            request.RawData = Encoding.UTF8.GetBytes (json);
        }
        //set request time out
        request.ConnectTimeout = TimeSpan.FromSeconds (TIME_OUT);
        request.Timeout = TimeSpan.FromSeconds (TIME_OUT);
        request.Priority = priority;
        request.Send ();
    }

    void OnRequestFinished (HTTPRequest req, HTTPResponse res)
    {
        bool isSuccess = false;
        switch (req.State) {
        // The request aborted, initiated by the user.
        case HTTPRequestStates.Aborted:
            Debug.LogWarning ("Request Aborted!");
            break;

        // Ceonnecting to the server timed out.
        case HTTPRequestStates.ConnectionTimedOut:
            Debug.LogError ("Connection Timed Out!");
            break;

        // The request didn't finished in the given time.
        case HTTPRequestStates.TimedOut:
            Debug.LogError ("Processing the request Timed Out!");
            break;

        // The request finished without any problem.
        case HTTPRequestStates.Finished:
            Debug.Log ("Request Finished With Status: " + res.StatusCode);
            Debug.Log ("Request Finished With Text: " + res.DataAsText);
            isSuccess = true;
            break;

        // The request finished with an unexpected error.
        // The request's Exception property may contain more information about the error.
        case HTTPRequestStates.Error:
            Debug.LogError ("Request Finished with Error! " +
              (req.Exception != null ?
              (req.Exception.Message + "\n" + req.Exception.StackTrace) :
              "No Exception"));
            break;
        }


        onGetResponse (isSuccess, req, res);
    }

    public void Dispose()
    {
        Debug.Log("Abort request with state " + request.State);
        request.Abort();
        request.Dispose();
    }
}
