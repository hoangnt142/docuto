﻿public struct LessonData
{
    public int id;
    public int video_id;
    public string title;
    public string video_url;
    public string thumbnail_url;
    public string explanation;
    public string necessity;
    public string updated_at;
    public string published_at;
    public string closed_at;
    public bool has_lesson;
    public int course_id;
    public string course_title;

    public InstructorData instructor;
}
