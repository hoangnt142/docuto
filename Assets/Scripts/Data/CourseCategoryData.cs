﻿public struct CourseCategoryData : IScrollerItemData
{
    public int id;
    public string name;
    public bool isFavorite;
}
