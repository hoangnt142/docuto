﻿﻿using System;

public struct UserData
{
    public Int32 id;
    public string name;
    public string email;
    public Int32 sex;
    public string auth_token;
    public string profile_image_path;
    public string background_image_path;
    public string birthday;
    // facebook fields
    public string fb_id;
    public byte[] fb_profile_picture;

    public InstructorData instructor;
}
