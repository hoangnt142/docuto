﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AppProductData
{
    public int id;
    public string name;
    public string code;
    public int fee_yen;
    public string ios_sku_id;
    public string android_sku_id;
    public int os_type;
}