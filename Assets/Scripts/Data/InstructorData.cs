﻿public class InstructorData
{
    public int id;
    public int user_id;
    public string name;
    public string email;
    public string title;
    public string category_text;
    public string explanation;
    public string profile_image_url;
    public BankData bank;
}
