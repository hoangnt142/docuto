using System;

public struct ChatGroupData
{
    public int id;
    public int chat_group_id;
    public ChatData last_chat;
    public int unread_num;
    public System.Collections.Generic.List<ChatData> chats;
    public UserData to_user;
    public ChatTargetData target;
}

public struct ChatTargetData
{
    public int id;
    public int user_id;
    public string name;
    public bool is_instructor;
}
