﻿public struct InstructorProfile
{
    public byte[] profile_image;
    public byte[] background_image;
    public string name;
    public string category_text;
    public string explanation;
}

