﻿﻿using System;

public struct NoticeData
{

    public int id;

    public string publish_at;

    public string title;

    public string image_url;

    public string content;

    public bool is_new;

    public bool is_read;

    public int notice_type;

    public int instructor_id;

    // for chat information
    public int chat_group_id;
    public string chat_time;
    public int chat_instructor_id;
    public int chat_user_id;
    public string chat_name;
}
