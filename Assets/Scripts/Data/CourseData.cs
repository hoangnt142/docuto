﻿using System;
using System.Collections.Generic;

public struct CourseData
{
    public Int32 id;
    public string title;
    public string image_path;
    public string sub_title;
    public CourseCategoryData course_category;
    public string explanation;
    public int fee_yen;
    public string necessity;
    public int update_type;
    public bool is_favorite;
    public CourseFeeData course_fee;
    public InstructorData instructor;
    public int level;
    public string image;    //base64
    public int video_id;
    public string unsubsribe_reason;
    public int num_of_students;

    public bool is_public;

    public string updated_at;

    public bool is_subscribing;

    // used on only request
    public Int32 course_category_id;

    public int GetFee()
    {
        if(course_fee != null)
        {
            return course_fee.fee_yen;
        }
        return fee_yen;
    }
}

public struct CourseListData : IScrollerItemData
{
    public Int32 id;
    public string message;
    public List<CourseData> list;
}

public struct DeleteCourseResponseData
{
    public bool does_send_request;
}

