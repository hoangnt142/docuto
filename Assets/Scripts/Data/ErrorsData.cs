﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct ErrorsData
{
    public int error_type;
    public string error_message;
}
