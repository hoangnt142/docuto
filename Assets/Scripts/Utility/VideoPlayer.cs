﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.Video;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.IO;

public class VideoPlayer : MonoBehaviour
{
    [SerializeField]
    bool autoPlay, loop, showThumbOnPause, tapToPause = true;

    [SerializeField]
    float defaultVolume = 0.7f;

    [SerializeField]
    Sprite spritePlayIcon, spritePauseIcon, spriteSoundIcon, spriteSoundOffIcon, spriteFullScreenIcon, spriteMinimizeScreenIcon;

    [SerializeField]
    GameObject playButtonObj, timeControlObj;


    UnityEngine.Video.VideoPlayer videoPlayer;
    AudioSource audioSource;
    RawImage rawImage;

    bool isUrlStream, isPlaying, soundMuted, hasThumb;
    string url;
    string htmlUrl;
    GameObject objBlurFrame;
    GameObject objThumbImagePanel;
    GameObject objTimeControlPanel;
    GameObject objPlayButtonPanel;
    Image playIconOnPlayButtonPanel;
    Image playIconOnTimeControlPanel;
    Image soundIconOnTimeControlPanel;
    Image switchScreenOnTimeControlPanel;
    Text textTimeDisplay;
    Slider volumeControl;
    Slider timerControl;
    Transform parrentTranform;
    RectTransform initVideoRect;
    Sprite currentVideoThumb;
    int initRectranfIndex;

    bool isSeekingTime, isFullScreenVideo;
    Coroutine playingCoroutine, seekCoroutine;

    // Initialization
    void Awake()
    {
        videoPlayer = gameObject.AddComponent<UnityEngine.Video.VideoPlayer>();
        audioSource = gameObject.AddComponent<AudioSource>();
        rawImage = gameObject.AddComponent<RawImage>();

        // create child objects
        objPlayButtonPanel = Instantiate(playButtonObj, transform);
        RectTransform playButtonRect = objPlayButtonPanel.GetComponent<RectTransform>();
        playIconOnPlayButtonPanel = playButtonRect.GetChild(1).gameObject.GetComponent<Image>();


        //UnityEngine.Object timeControlObj = AssetDatabase.LoadAssetAtPath("Assets/Prefabs/Pages/Partial/VideoPlayer/TimeControl.prefab", typeof(UnityEngine.Object));
        objTimeControlPanel = Instantiate(timeControlObj, transform);
        RectTransform timeControlRect = objTimeControlPanel.GetComponent<RectTransform>();
        playIconOnTimeControlPanel = timeControlRect.Find("PlayButton").gameObject.GetComponent<Image>();
        timeControlRect.Find("PlayButton").gameObject.AddComponent<Button>().onClick.AddListener(PlayOrPause);

        textTimeDisplay = timeControlRect.Find("PlayTime").gameObject.GetComponent<Text>();

        soundIconOnTimeControlPanel = timeControlRect.Find("Volume").Find("Icon").gameObject.GetComponent<Image>();
        timeControlRect.Find("Volume").Find("Icon").gameObject.AddComponent<Button>().onClick.AddListener(OnSoundClick);

        volumeControl = timeControlRect.Find("Volume").Find("Range").gameObject.GetComponent<Slider>();
        volumeControl.maxValue = 1;
        volumeControl.minValue = 0;
        volumeControl.value = defaultVolume;
        volumeControl.onValueChanged.AddListener(OnChangeVolume);

        timerControl = timeControlRect.Find("Timer").gameObject.GetComponent<Slider>();
        timerControl.minValue = 0;
        timerControl.value = 0;

        switchScreenOnTimeControlPanel = timeControlRect.Find("SwitchScreen").gameObject.GetComponent<Image>();
        timeControlRect.Find("SwitchScreen").gameObject.AddComponent<Button>().onClick.AddListener(SwitchVideoScreen);

        EventTrigger trigger = timerControl.gameObject.AddComponent<EventTrigger>();
        var pointerDown = new EventTrigger.Entry();
        pointerDown.eventID = EventTriggerType.PointerDown;
        pointerDown.callback.AddListener((e) => {
            if (seekCoroutine != null)
                StopCoroutine(seekCoroutine);

            isSeekingTime = true;
        });
        var pointerUp = new EventTrigger.Entry();
        pointerUp.eventID = EventTriggerType.PointerUp;
        pointerUp.callback.AddListener((e) => {
            if (seekCoroutine != null)
                StopCoroutine(seekCoroutine);
            if (playingCoroutine != null)
                StopCoroutine(playingCoroutine);

            //isSeekingTime = false;
            if (videoPlayer != null)
            {
                float nTime = Mathf.Clamp(timerControl.value, 0, 1);
                ShowVideoTimer(nTime * Duration);

                seekCoroutine = StartCoroutine(SeekingVideo(timerControl.value));
            }
        });
        var pointerDrag = new EventTrigger.Entry();
        pointerDrag.eventID = EventTriggerType.Drag;
        pointerDrag.callback.AddListener((e) =>
        {
            if (videoPlayer != null)
            {
                float nTime = Mathf.Clamp(timerControl.value, 0, 1);
                ShowVideoTimer(nTime * Duration);
            }
        });
        trigger.triggers.Add(pointerDown);
        trigger.triggers.Add(pointerUp);
        trigger.triggers.Add(pointerDrag);
        //timerControl.onValueChanged.AddListener(OnSeekVideo);



        objBlurFrame = new GameObject("BlurFrame");
        objBlurFrame.AddComponent<Image>().color = new Color32(255, 255, 255, 111);
        RectTransform blurFrameRect = objBlurFrame.GetComponent<RectTransform>();
        blurFrameRect.parent = transform;
        blurFrameRect.pivot = new Vector2(0.5f, 0.5f);
        blurFrameRect.anchorMin = new Vector2(0, 0);
        blurFrameRect.anchorMax = new Vector2(1, 1);
        blurFrameRect.offsetMin = new Vector2(0, 0);
        blurFrameRect.offsetMax = new Vector2(0, 0);
        blurFrameRect.localScale = Vector3.one;

        objThumbImagePanel = new GameObject("ThumbnailImage");
        objThumbImagePanel.AddComponent<Image>().color = new Color32(255, 255, 255, 255);
        RectTransform thumbImageRect = objThumbImagePanel.GetComponent<RectTransform>();
        thumbImageRect.parent = transform;
        thumbImageRect.pivot = new Vector2(0.5f, 0.5f);
        thumbImageRect.anchorMin = new Vector2(0, 0);
        thumbImageRect.anchorMax = new Vector2(1, 1);
        thumbImageRect.offsetMin = new Vector2(0, 0);
        thumbImageRect.offsetMax = new Vector2(0, 0);
        thumbImageRect.localScale = Vector3.one;
        objThumbImagePanel.SetActive(false);

        thumbImageRect.SetAsFirstSibling();
        //blurFrameRect.SetAsLastSibling();
        timeControlRect.SetAsLastSibling();
        objTimeControlPanel.SetActive(false);

        // add video handler
        videoPlayer.isLooping = loop;
        audioSource.loop = loop;
        videoPlayer.loopPointReached += OnLoopPointReached;
        videoPlayer.seekCompleted += OnSeekComplete;
        //videoPlayer.sendFrameReadyEvents = true;
        //videoPlayer.frameReady += FrameReady;


        if (tapToPause)
        {
            Button mainButton = gameObject.AddComponent<Button>();
            mainButton.transition = Selectable.Transition.None;
            mainButton.onClick.AddListener(OnVideoTapped);
        }
    }

    float Duration
    {
        get
        {
            return videoPlayer.frameCount / videoPlayer.frameRate;
        }
    }

    void OnLoopPointReached(UnityEngine.Video.VideoPlayer player)
    {
        if (!loop)
        {
            Stop();
            ShowPlayerTool(true);
        }
    }

    void OnSeekComplete(UnityEngine.Video.VideoPlayer player)
    {
        if (isPlaying)
        {
            videoPlayer.Play();
            audioSource.Play();
        }

        ShowLoading(false);
        isSeekingTime = false;
    }

    void Start()
    {
        initVideoRect = rawImage.rectTransform;
        parrentTranform = gameObject.transform.parent;
        initRectranfIndex = gameObject.transform.rectTransform().GetSiblingIndex();
    }

    void Update()
    {
        if(videoPlayer != null && videoPlayer.isPlaying)
        {
            if (!isSeekingTime)
            {
                timerControl.value = (float)videoPlayer.time / Duration;
                ShowVideoTimer(videoPlayer.time);
            }
        }
    }

    public VideoPlayer SetVideoThumb(Sprite sprite)
    {
        Image thumbImage = objThumbImagePanel.GetComponent<Image>();
        thumbImage.sprite = sprite;
        thumbImage.color = new Color32(255, 255, 255, 255);

        objThumbImagePanel.SetActive(true);
        hasThumb = true;
        currentVideoThumb = sprite;

        return this;
    }

    public VideoPlayer SetVideoPath(string videoPath)
    {
        url = videoPath;
        isUrlStream = false;
        isPlaying = false;

        StartCoroutine(prepareVideo(@"file://" + videoPath));
        return this;
    }


    public VideoPlayer SetVideoUrl(string videoUrl)
    {
        url = videoUrl;
        isUrlStream = true;
        isPlaying = false;

        StartCoroutine(prepareVideo(videoUrl));

        return this;
    }

    public VideoPlayer SetHtmlUrl(string url)
    {
        htmlUrl = url;

        return this;
    }

    // Deprecated
    //IEnumerator prepareVideoStream(string videoUrl)
    //{
    //    Dictionary<string, string> headers = new Dictionary<string, string>();
    //    headers.Add("Authorization", Session.Token);

    //    WWW www = new WWW(videoUrl);
    //    yield return www;

    //    string videoPath = Application.dataPath + "/" + Convert.ToBase64String(Encoding.UTF8.GetBytes(videoUrl)) + ".mp4";
    //    File.WriteAllBytes(videoPath, www.bytes);


    //    StartCoroutine(prepareVideo(@"file://" + videoPath));
    //}


    public string GetVideoURL()
    {
        return url;
    }

    public void StartPlay()
    {
        soundMuted = false;

        if (playingCoroutine != null)
            StopCoroutine(playingCoroutine);
        playingCoroutine = StartCoroutine(playVideo());

        //playIconOnPlayButtonPanel.sprite = spritePauseIcon;
        playIconOnTimeControlPanel.sprite = spritePauseIcon;

        ShowBlurFrame(false);
    }

    public void Play()
    {
        audioSource.mute = false;
        videoPlayer.Play();
        audioSource.Play();
        isPlaying = true;

        playIconOnTimeControlPanel.sprite = spritePauseIcon;
    }

    public void Pause()
    {
        if(!showThumbOnPause) objThumbImagePanel.SetActive(false);

        videoPlayer.Pause();
        audioSource.Pause();
        isPlaying = false;

        playIconOnTimeControlPanel.sprite = spritePlayIcon;
    }

    public void Stop()
    {
        if (hasThumb) objThumbImagePanel.SetActive(true);

        isPlaying = false;
        if(videoPlayer != null) videoPlayer.Stop();
        if(audioSource != null) audioSource.Stop();

        timerControl.value = 0;
        //playIconOnPlayButtonPanel.sprite = spritePlayIcon;
        playIconOnTimeControlPanel.sprite = spritePlayIcon;
    }

    public void ShowPlayerTool(bool isShow)
    {
        foreach (Transform child in gameObject.GetComponent<Transform>())
        {
            child.gameObject.SetActive(isShow);
        }
        if(objTimeControlPanel != null) objTimeControlPanel.SetActive(false);
    }

    public void ShowBlurFrame(bool isShow)
    {
        objBlurFrame.SetActive(isShow);
        objThumbImagePanel.SetActive(isShow);
        objPlayButtonPanel.SetActive(isShow);
    }

    public void ShowFirstFrame()
    {
        soundMuted = true;
        //StartCoroutine(playVideo(0, 0.01f));
        rawImage.texture = videoPlayer.texture;
        videoPlayer.Play();
        videoPlayer.time = 1.0f;
        videoPlayer.Pause();
    }

    public byte[] GetBytes()
    {
        return File.ReadAllBytes(url);
    }

    public void OnVideoTapped()
    {
        if(!String.IsNullOrEmpty(htmlUrl)) Webview.Show(htmlUrl);

        return;

        //if(!isPlaying)
        //{
        //    if (videoPlayer != null && videoPlayer.isPlaying) videoPlayer.Stop();
        //    videoPlayer.Prepare();
        //    StartPlay();
        //    return;
        //}

        //PlayOrPause();
    }

    public void PlayOrPause()
    {
        if (videoPlayer.isPlaying)
        {
            ShowBlurFrame(true);
            Pause();
        }
        else
        {
            ShowBlurFrame(false);
            Play();
        }
    }

    IEnumerator prepareVideo(string videoUrl)
    {
        //Disable Play on Awake for both Video and Audio
        videoPlayer.playOnAwake = false;
        audioSource.playOnAwake = false;

        if (videoPlayer.isPlaying) Stop();

        // Vide clip from Url
        videoPlayer.source = VideoSource.Url;
        videoPlayer.url = videoUrl;

        //Set Audio Output to AudioSource
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        videoPlayer.Prepare();

        //Wait until video is prepared
        while (!videoPlayer.isPrepared)
        {
            yield return null;
        }

        Debug.Log("Done Preparing Video");

        //prepare video time
        timerControl.minValue = 0;
        timerControl.maxValue = 1; //videoPlayer.frameCount;
        ShowVideoTimer(0);

        if (autoPlay)
            StartPlay();
        else
            if(!hasThumb) ShowFirstFrame();
    }

    IEnumerator playVideo(float startTime = 0, float endTime = 0)
    {
        while (!videoPlayer.isPrepared)
        {
            yield return null;
        }

        //Assign the Texture from Video to RawImage to be displayed
        rawImage.texture = videoPlayer.texture;

        if (soundMuted)
        {
            audioSource.mute = true;
        }
        audioSource.volume = defaultVolume;


        //Play Video
        Debug.Log("Playing Video");
        videoPlayer.Play();
        audioSource.Play();
        isPlaying = true;

        if (startTime > 0 && videoPlayer.canSetTime) videoPlayer.time = startTime;
        if (endTime > 0)
        {
            while (videoPlayer.isPlaying)
            {
                Debug.LogWarning("Video Time: " + Mathf.FloorToInt((float)videoPlayer.time));

                if (endTime > 0 && videoPlayer.time >= endTime)
                    Stop();

                yield return null;
            }

            Debug.Log("Done Playing Video");
        }
    }

    IEnumerator SeekingVideo(float playingTime)
    {
        //if(isSeekingTime) yield return null;
        //_tmpVideoPlayer = videoPlayer;
        //rawImage.texture = _tmpVideoPlayer.texture;
        //_tmpVideoPlayer.Pause();
        ShowLoading(true);

        //isSeekingTime = true;
        videoPlayer.Stop();
        audioSource.Stop();
        //yield return null;

        //Set Audio Output to AudioSource
        videoPlayer.audioOutputMode = VideoAudioOutputMode.AudioSource;

        //Assign the Audio from Video to AudioSource to be played
        videoPlayer.EnableAudioTrack(0, true);
        videoPlayer.SetTargetAudioSource(0, audioSource);

        //Set video To Play then prepare Audio to prevent Buffering
        videoPlayer.Prepare();
        while (!videoPlayer.isPrepared)
        {
            yield return null;
        }

        rawImage.texture = videoPlayer.texture;
        videoPlayer.Pause();
        audioSource.Pause();
        while (!videoPlayer.canStep || !videoPlayer.canSetTime)
        {
            yield return null;
        }

        float nTime = Mathf.Clamp(playingTime, 0, 1);
        videoPlayer.time = nTime * Duration;
    }

    void OnChangeVolume(float val)
    {
        audioSource.volume = val;

        if (val > 0)
        {
            soundIconOnTimeControlPanel.sprite = spriteSoundIcon;
            audioSource.mute = false;
        }
        else
        {
            soundIconOnTimeControlPanel.sprite = spriteSoundOffIcon;
            audioSource.mute = true;
        }
    }

    void OnSoundClick()
    {
        if (!audioSource.mute)
        {
            soundIconOnTimeControlPanel.sprite = spriteSoundOffIcon;
            audioSource.mute = true;
            defaultVolume = volumeControl.value;
            volumeControl.value = 0;
        }
        else
        {
            soundIconOnTimeControlPanel.sprite = spriteSoundIcon;
            audioSource.mute = false;
            volumeControl.value = defaultVolume;
        }
    }

    void SwitchVideoScreen()
    {
        RectTransform timeControlRect = objTimeControlPanel.GetComponent<RectTransform>();
        RectTransform mainRectTransform = gameObject.transform.rectTransform();

        if(isFullScreenVideo)
        {
            HeaderView.Show();
            gameObject.transform.SetParent(parrentTranform);
            mainRectTransform.SetSiblingIndex(initRectranfIndex);

            //Minimize
            mainRectTransform.pivot = initVideoRect.pivot;
            mainRectTransform.anchorMin = initVideoRect.anchorMin;
            mainRectTransform.anchorMax = initVideoRect.anchorMax;

            mainRectTransform.rotation = Quaternion.AngleAxis(0, Vector3.forward);
            mainRectTransform.offsetMin = initVideoRect.offsetMin;
            mainRectTransform.offsetMax = initVideoRect.offsetMax;

            // update icon
            switchScreenOnTimeControlPanel.sprite = spriteFullScreenIcon;
            isFullScreenVideo = false;

            // change time control height
            timeControlRect.sizeDelta = new Vector2(timeControlRect.sizeDelta.x, timeControlRect.sizeDelta.y - 10);
        }
        else
        {
            gameObject.transform.SetParent(ApplicationSystem.RootCanvas.transform);

            //Fullscreen
            Vector2 screenSize = ApplicationSystem.ScreenSize;
            mainRectTransform.pivot = new Vector2(0.5f, 0.5f);
            mainRectTransform.anchorMin = Vector2.zero;
            mainRectTransform.anchorMax = Vector2.one;
            mainRectTransform.offsetMin = new Vector2(0, 0);
            mainRectTransform.offsetMax = new Vector2(0, 0);
            mainRectTransform.rotation = Quaternion.AngleAxis(-90, Vector3.forward);
            mainRectTransform.sizeDelta = new Vector2(screenSize.y - screenSize.x, screenSize.x - screenSize.y);
            HeaderView.Hide();

            // update icon
            switchScreenOnTimeControlPanel.sprite = spriteMinimizeScreenIcon;
            isFullScreenVideo = true;

            // change time control height
            timeControlRect.sizeDelta = new Vector2(timeControlRect.sizeDelta.x, timeControlRect.sizeDelta.y + 10);
        }
    }

    void ShowVideoTimer(double playTime)
    {
        TimeSpan time = TimeSpan.FromSeconds(playTime);
        TimeSpan duration = TimeSpan.FromSeconds(Duration);
        string timeFormat = string.Format("{0:D2}:{1:D2}:{2:D2}", time.Hours, time.Minutes, time.Seconds);
        if (time.Hours <= 0)
            timeFormat = string.Format("{0:D2}:{1:D2}", time.Minutes, time.Seconds);
        string durationFormat = string.Format("{0:D2}:{1:D2}:{2:D2}", duration.Hours, duration.Minutes, duration.Seconds);
        if (duration.Hours <= 0)
            durationFormat = string.Format("{0:D2}:{1:D2}", duration.Minutes, duration.Seconds);

        textTimeDisplay.text = timeFormat + "|" + durationFormat;
    }

    void ShowLoading(bool isShow)
    {
        Image thumbImage = objThumbImagePanel.GetComponent<Image>();
        if (isShow)
        {
            thumbImage.sprite = null;
            thumbImage.color = new Color32(0, 0, 0, 0);
        }
        else
        {
            thumbImage.sprite = currentVideoThumb;
            thumbImage.color = new Color32(255, 255, 255, 255);
        }

        objThumbImagePanel.SetActive(isShow);
    }

    public void Dispose()
    {
        if(videoPlayer != null) Stop();
        isUrlStream = isPlaying = soundMuted = hasThumb = false;
        url = "";

        ShowPlayerTool(true);
    }
}
