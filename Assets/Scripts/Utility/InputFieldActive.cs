﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class InputFieldActive : MonoBehaviour {

    private InputField inputField;
    private Button buttonComponent;

    // Use this for initialization
    void Start() {
        inputField = transform.parent.GetComponent<InputField>();
        buttonComponent = GetComponent<Button>();
        inputField.onEndEdit.AddListener(delegate { OnEditEnd(inputField); });
        buttonComponent.onClick.AddListener(OnButtonClick);
    }

    void OnButtonClick(){
        buttonComponent.enabled = false;
        inputField.enabled = true;
        inputField.ActivateInputField();
        inputField.Select();
    }
    
    void OnEditEnd(InputField inputField) {
        buttonComponent.enabled = true;
        inputField.enabled = false;
    }
}
