﻿using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CropImage
{
    public static Sprite Crop(Image sourceImage, Vector2 croppSize, Vector2 position, Vector2 zoomScale)
    {
        Texture2D sourceTexture = sourceImage.sprite.texture;

        Vector2 revZoomScale = new Vector2(1 / Mathf.Abs(zoomScale.x), 1 / Mathf.Abs(zoomScale.y));
        Vector2 croppedPosition = Vector2.Scale(position, revZoomScale);
        Vector2 croppedSize = Vector2.Scale(croppSize, revZoomScale);

        if (croppedPosition.x < 0) croppedPosition.x = 0;
        if (croppedPosition.x > sourceTexture.width) croppedPosition.x = sourceTexture.width;
        if (croppedPosition.y < 0) croppedPosition.y = 0;
        if (croppedPosition.y > sourceTexture.height) croppedPosition.y = sourceTexture.height;

        if (croppedSize.x > sourceTexture.width) croppedSize.x = sourceTexture.width;
        if (croppedSize.y > sourceTexture.height) croppedSize.y = sourceTexture.height;

        var colors = sourceTexture.GetPixels(Mathf.FloorToInt(croppedPosition.x), Mathf.FloorToInt(croppedPosition.y), Mathf.FloorToInt(croppedSize.x), Mathf.FloorToInt(croppedSize.y));

        Texture2D tex = new Texture2D(Mathf.FloorToInt(croppedSize.x), Mathf.FloorToInt(croppedSize.y), TextureFormat.RGBA32, false);
        tex.SetPixels(colors);
        tex.Apply(true, false);

        TextureScale.Bilinear(tex, Mathf.FloorToInt(croppSize.x), Mathf.FloorToInt(croppSize.y));
        var sprite = Sprite.Create(tex, new Rect(0f, 0f, Mathf.FloorToInt(croppSize.x), Mathf.FloorToInt(croppSize.y)), new Vector2(0.5f, 0.5f));
        return sprite;
    }

    public static byte[] GetPng(Texture2D tex)
    {
        return tex.EncodeToPNG();
    }
}
