﻿using System;
using System.IO;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Uploader : MonoBehaviour
{
    [SerializeField]
    long packageSize = 1000000;

    public Action OnSuccess;
    public Action OnFail;

    bool isUploadSuccess;
    bool isUploadProcesStoped;
    bool isProcessUpload;
    int totalVideoPart;
    int numberUploaded;
    int numberOfRunningThread;
    string path;

    Dictionary<int, Segment> errorSegs = new Dictionary<int, Segment>();

    Dictionary<UploaderDataKeys, object> data = new Dictionary<UploaderDataKeys, object>();
    public Uploader SetData<T>(UploaderDataKeys key, T value)
    {
        if (data.ContainsKey(key)) data.Remove(key);
        data.Add(key, value);

        return this;
    }
    public T GetData<T>(UploaderDataKeys key)
    {
        if (!data.ContainsKey(key)) return default(T);

        T value = (T)data[key];
        return value;
    }

    private void Update()
    {
        if(isProcessUpload)
        {
            float percent = ((float)numberUploaded)/((float)totalVideoPart);
            if (percent < 0.01f) percent = 0.01f;
            ProgressBar.SetValue(percent);
        }
    }

    // Upload video
    public void UploadVideo(string uploadPath)
    {
        path = uploadPath;
        numberUploaded = 0;
        numberOfRunningThread = 0;
        isUploadProcesStoped = false;
        errorSegs.Clear();

        ProgressBar.Show(0.01f);
        StartCoroutine(_UploadVideo());
    }

    IEnumerator _UploadVideo()
    {
        // first return to show loadingview
        yield return null;

        long packSize = packageSize; //1Mbs
        long videoLength = 0;
        string videoName = "";

        isProcessUpload = true;
        isUploadSuccess = true;
        try
        {
            // new FileStream(videoPlayer.GetVideoURL(), FileMode.Open))
            // check error before upload
            using (var _stream = new BinaryReader(new FileStream(path, FileMode.Open)))
            {
                videoLength = (new FileInfo(path)).Length;
                videoName = (new FileInfo(path)).Name;
                totalVideoPart = (int)Math.Ceiling(((double)videoLength / packSize));
            }
        }
        catch (Exception e)
        {
            Debug.Log("Uploader::_UploadVideo error: " + e.Message);
            isUploadSuccess = false;
            isUploadProcesStoped = true;
        }

        if (!isUploadProcesStoped)
        {
            using (var reader = new BinaryReader(new FileStream(path, FileMode.Open)))
            {
                int desIndex = 0;
                bool stopLoop = false;
                while (!isUploadProcesStoped && !stopLoop && (desIndex * packSize) < videoLength)
                {
                    // seek stream to to begin of the pack
                    reader.BaseStream.Seek(desIndex * packSize, SeekOrigin.Begin);

                    //if ((videoLength - desIndex * packSize) < packSize)
                    if (videoLength <= (packSize * (desIndex + 1)))
                    {
                        packSize = videoLength - (desIndex * packSize);
                        stopLoop = true;
                    }

                    byte[] packSend = new byte[packSize];
                    int part = desIndex + 1;

                    reader.Read(packSend, 0, (int)packSize);
                    //Array.Copy(videoBytes, desIndex * packSize, packSend, 0, packSize);

                    Debug.Log("Uploading video part " + part);
                    // store seg to dictionary
                    Segment seg = new Segment();
                    seg.path = path;
                    seg.totalPart = totalVideoPart;
                    seg.name = videoName;
                    seg.length = videoLength;
                    seg.part = part;
                    seg.pack = packSend;
                    errorSegs.Add(part, seg);

                    // upload video
                    VideoAPI.Upload(packSend, videoName, part, totalVideoPart, GetData<int>(UploaderDataKeys.LESSON_ID), GetData<int>(UploaderDataKeys.COURSE_ID), ProcessUploadVideo, 1);

                    desIndex++;
                    numberOfRunningThread++;
                    // to prevent out of memory, just run 40 threads in parallel.
                    // because each thread has atless 1Mb size
                    while (numberOfRunningThread > 40)
                    {
                        yield return null;
                    }

                    //return to main frame
                    yield return null;
                }
            }
        }

        // wait for all upload processes
        while (!isUploadProcesStoped)
        {
            yield return null;
        }

        EndingUploadProcess();
    }

    void ProcessUploadVideo(int part, bool isSuccess)
    {
        // one thread success, free up the count
        numberOfRunningThread--;

        if (isSuccess)
        {
            Debug.Log("Video part " + part + " uploaded");
            numberUploaded++;
            if (numberUploaded == totalVideoPart) isUploadProcesStoped = true;

            // remove part from errors dictionary
            if (errorSegs.ContainsKey(part)) errorSegs.Remove(part);
        }
        else
        {
            Debug.LogError("Video part " + part + " error upload");

            // try again
            StartCoroutine(TryUploadVideoAgain(errorSegs[part]));
        }
    }

    IEnumerator TryUploadVideoAgain(Segment errSeg)
    {
        yield return null;

        if (!isUploadProcesStoped)
        {
            int maxTryNumber = 3; // try in 3 times
            int numberOfTry = 1;
            bool endOfAPIProcess;
            bool isRetrySuccess = false;

            while (numberOfTry <= maxTryNumber && !isUploadProcesStoped)
            {
                print("Try to upload part " + errSeg.part + " ... " + numberOfTry);
                endOfAPIProcess = false;
                isRetrySuccess = false;
                VideoAPI.Upload(errSeg.pack, errSeg.name, errSeg.part, errSeg.totalPart, GetData<int>(UploaderDataKeys.LESSON_ID), GetData<int>(UploaderDataKeys.COURSE_ID), (_part, _issuccess) =>
                {
                    if (_issuccess)
                    {
                        Debug.Log("Video part " + _part + " uploaded in trying " + numberOfTry);
                        numberUploaded++;
                        if (numberUploaded == totalVideoPart) isUploadProcesStoped = true;
                        isRetrySuccess = true;

                        // remove part from errors dictionary
                        if (errorSegs.ContainsKey(_part))
                            errorSegs.Remove(_part);
                        // and end loop
                        numberOfTry = maxTryNumber + 1;
                    }
                    else
                    {
                        Debug.LogError("Video part " + _part + " error upload in trying " + numberOfTry);
                        numberOfTry++;
                    }

                    endOfAPIProcess = true;
                }, 0);

                yield return new WaitUntil(() => { return endOfAPIProcess; });
            }

            // final of trying
            if (!isRetrySuccess)
            {
                isUploadSuccess = false;
                isUploadProcesStoped = true;
                //StopCoroutine(_UploadVideo());
                VideoAPI.Dispose();
            }
        }

        yield return null;
    }

    void EndingUploadProcess()
    {
        ProgressBar.Hide();

        if (isUploadSuccess) OnSuccess();
        else OnFail();

        isProcessUpload = false;
    }

    class Segment
    {
        public string path;
        public string name;
        public int part;
        public int totalPart;
        public long length;
        public byte[] pack;
    }
}


public enum UploaderDataKeys
{
    COURSE_ID,
    LESSON_ID
}
