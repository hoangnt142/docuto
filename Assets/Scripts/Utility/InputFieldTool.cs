﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldTool : MonoBehaviour
{

    InputField inf;

    protected void Start()
    {
        inf = this.gameObject.GetComponent<InputField>();
        inf.onValueChanged.AddListener(new UnityEngine.Events.UnityAction<string>(ResizeInput));
    }

    public void ResizeInput(string text)
    {
        if (gameObject.GetComponent<LayoutElement>() != null && string.IsNullOrEmpty(text))
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, gameObject.GetComponent<LayoutElement>().minHeight);

        if (inf == null) return;

        Debug.Log("some kind of resizing horror");
        var fullText = inf.text;

        Vector2 extents = inf.textComponent.rectTransform.rect.size;
        var settings = inf.textComponent.GetGenerationSettings(extents);
        settings.generateOutOfBounds = false;
        var prefheight = new TextGenerator().GetPreferredHeight(fullText, settings) + 10;
        if (gameObject.GetComponent<LayoutElement>() != null && prefheight > gameObject.GetComponent<LayoutElement>().minHeight)
            gameObject.GetComponent<RectTransform>().sizeDelta = new Vector2(gameObject.GetComponent<RectTransform>().sizeDelta.x, prefheight);
    }
}
