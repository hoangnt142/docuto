﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputFieldInteractWithTouchScreenKeyboard : MonoBehaviour
{
    const float PADDING = 196.2f;

    private GameObject page;
    private InputField inputField;
    private RectTransform pageRectTrans;
    private RectTransform rectTransform;
    private Vector2 pageAnchoredPosition;

    private bool isKeyboardOverlapped;
    private bool isKeyboardShowed;
    private float keyboardHeight;
    private float currentKeyboardHeight;
    private float BottomDistance {
        get {
            return Screen.height/2 + (Mathf.Abs(rectTransform.position.y) - Mathf.Abs(pageRectTrans.position.y));
        }
    }

    // Use this for initialization
    void Start()
    {
        inputField = gameObject.GetComponent<InputField>();
        rectTransform = gameObject.GetComponent<RectTransform>();

        page = FindParrentPage();
        pageRectTrans = page.GetComponent<RectTransform>();
        pageAnchoredPosition = pageRectTrans.anchoredPosition;
    }

    public void LateUpdate()
    {
        if (inputField.isFocused)
        {
            isKeyboardShowed = true;
            keyboardHeight = GetKeyboardHeight();

            //print("--------> show keyboard...");
            if (keyboardHeight > currentKeyboardHeight)
            {
                isKeyboardOverlapped = false;
            }

            if (keyboardHeight > rectTransform.position.y && !isKeyboardOverlapped)
            {
                isKeyboardOverlapped = true;
                currentKeyboardHeight = keyboardHeight;
                float moveUpDistance = keyboardHeight - BottomDistance + PADDING;
                pageRectTrans.anchoredPosition = new Vector2(pageAnchoredPosition.x, pageAnchoredPosition.y + moveUpDistance);
            }
        }
        else
        {
            //print("--------> hide keyboard...");
            if (isKeyboardOverlapped || isKeyboardShowed)
            {
                isKeyboardShowed = false;
                isKeyboardOverlapped = false;
                pageRectTrans.anchoredPosition = pageAnchoredPosition;
            }
        }
    }

    private float GetKeyboardHeight()
    {
        if (Application.isEditor)
        {
            return TouchScreenKeyboard.area.height;//102.4f; // fake TouchScreenKeyboard height ratio for debug in editor        
        }

#if UNITY_ANDROID        
        using (AndroidJavaClass UnityClass = new AndroidJavaClass("com.unity3d.player.UnityPlayer"))
        {
            AndroidJavaObject View = UnityClass.GetStatic<AndroidJavaObject>("currentActivity").Get<AndroidJavaObject>("mUnityPlayer").Call<AndroidJavaObject>("getView");
            using (AndroidJavaObject rect = new AndroidJavaObject("android.graphics.Rect"))
            {
                View.Call("getWindowVisibleDisplayFrame", rect);
                int viewPortHeight = rect.Call<int>("height");
                return Screen.height - viewPortHeight;
            }
        }
#else
        return TouchScreenKeyboard.area.height;
#endif
    }

    public  GameObject FindParrentPage()
    {
        Transform t = transform;
        while (t.parent != null)
        {
            if (t.parent.GetComponent<Page>() != null)
            {
                return t.parent.gameObject;
            }
            t = t.parent.transform;
        }
        return null;
    }
}
