﻿using System.IO;
using UnityEngine;

#if UNITY_EDITOR
using UnityEditor;

public class EditorIO
{
    public static Texture2D GetPngTextureFromStrage ()
    {
        var path = EditorUtility.OpenFilePanel ("select png", "", "png");
        return ReadPng (path);
    }

    public static Texture2D ReadPng (string path)
    {
        FileStream fileStream = new FileStream (path, FileMode.Open, FileAccess.Read);
        BinaryReader bin = new BinaryReader (fileStream);
        byte [] readBinary = bin.ReadBytes ((int)bin.BaseStream.Length);
        bin.Close ();

        int pos = 16; // start from 16 bytes

        int width = 0;
        for (int i = 0; i < 4; i++) {
            width = width * 256 + readBinary [pos++];
        }

        int height = 0;
        for (int i = 0; i < 4; i++) {
            height = height * 256 + readBinary [pos++];
        }

        Texture2D texture = new Texture2D (width, height);
        texture.LoadImage (readBinary);

        return texture;
    }
}
#endif
