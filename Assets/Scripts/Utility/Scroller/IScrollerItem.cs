using UnityEngine;
using System.Collections;

public interface IScrollerItem
{
    bool IsActive { get; }

    void Setup(IScrollerItemData data);

    void Activate();

    void Deactivate();
}
