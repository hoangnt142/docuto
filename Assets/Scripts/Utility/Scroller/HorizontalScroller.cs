using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(ScrollRect))]
public class HorizontalScroller : MonoBehaviour
{
    // Region Public
    [SerializeField]
    float padding = 50f;

    [SerializeField]
    float moveSpeed = 15f;

    [SerializeField]
    float snapSpeed = 5f;

    [SerializeField]
    float leadingLineWidth;

    [SerializeField]
    float trailingLineWidth;

    [SerializeField]
    bool snapByClick;

    [SerializeField]
    GameObject itemPrefab;

    public Action<IScrollerItem> OnSelectedItem;
    public Action OnStartScroll;
    public Action OnEndScroll;
    public Action<IScrollerItem> onItemClick;

    // Region Pivate 
    RectTransform trailingLine;
    RectTransform centerLine;
    RectTransform leadingLine;
    RectTransform scrollPanel;
    List<GameObject> items;
    ScrollRect scrollRect;
    Vector2 initAnchoredPosition;
    List<Vector2> itemAnchoredPositions;

    float[] distances;
    float[] distancePosition;
    float itemWidth;
    float itemWidthInWorld;
    float initMoveSpeed;
    int itemLength;
    int selectedItemIndex;
    int lastSelectedItemIndex;

    bool isDragging = false;
    bool isScrolling = false;
    bool isDoneSelect = false;
    bool isInvokingSelectionCallback = false;
    bool isInvokingStartScrollCallback = false;
    bool isInvokingEndScrollCallback = false;
    bool init;
    bool autoSelectItem = true;

    void Start()
    {
        scrollRect = gameObject.GetComponent<ScrollRect>();
        items = new List<GameObject>();
        itemAnchoredPositions = new List<Vector2>();

        var instanceItemPrefab = Instantiate(itemPrefab, scrollRect.content.gameObject.GetComponent<Transform>());
        itemWidth = instanceItemPrefab.GetComponent<RectTransform>().rect.size.x;
        itemWidth += padding;
        Destroy(instanceItemPrefab);

        GameObject objLeadingLine = new GameObject("LeadingLine");
        objLeadingLine.AddComponent<RectTransform>();
        objLeadingLine.AddComponent<Image>();
        objLeadingLine.SetActive(false);
        objLeadingLine.transform.SetParent(gameObject.transform);
        leadingLine = objLeadingLine.GetComponent<RectTransform>();
        leadingLine.pivot = new Vector2(1, 0);
        leadingLine.anchorMin = new Vector2(1, 0);
        leadingLine.anchorMax = new Vector2(1, 1);
        leadingLine.offsetMin = new Vector2(0, 0);
        leadingLine.offsetMax = new Vector2(0, 0);
        leadingLine.SetAsLastSibling();
        if (leadingLineWidth > 0)
        {
            objLeadingLine.GetComponent<RectTransform>().sizeDelta = new Vector2(leadingLineWidth, 0);
            objLeadingLine.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            objLeadingLine.SetActive(true);
        }

        GameObject objCenterLine = new GameObject("CenterLine");
        objCenterLine.AddComponent<RectTransform>();
        objCenterLine.SetActive(false);
        objCenterLine.transform.SetParent(gameObject.transform);
        centerLine = objCenterLine.GetComponent<RectTransform>();
        centerLine.pivot = new Vector2(0.5f, 0.5f);
        centerLine.anchorMin = new Vector2(0.5f, 0);
        centerLine.anchorMax = new Vector2(0.5f, 1);
        centerLine.offsetMin = new Vector2(0, 0);
        centerLine.offsetMax = new Vector2(0, 0);
        centerLine.SetAsLastSibling();


        GameObject objTrailingLine = new GameObject("TrailingLine");
        objTrailingLine.AddComponent<RectTransform>();
        objTrailingLine.AddComponent<Image>();
        objTrailingLine.SetActive(false);
        objTrailingLine.transform.SetParent(gameObject.transform);
        trailingLine = objTrailingLine.GetComponent<RectTransform>();
        trailingLine.pivot = new Vector2(0, 0);
        trailingLine.anchorMin = new Vector2(0, 0);
        trailingLine.anchorMax = new Vector2(0, 1);
        trailingLine.offsetMin = new Vector2(0, 0);
        trailingLine.offsetMax = new Vector2(0, 0);
        trailingLine.SetAsLastSibling();
        if (trailingLineWidth > 0)
        {
            objTrailingLine.GetComponent<RectTransform>().sizeDelta = new Vector2(trailingLineWidth, 0);
            objTrailingLine.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
            objTrailingLine.SetActive(true);
        }

        scrollPanel = scrollRect.content;
        initAnchoredPosition = scrollPanel.anchoredPosition;
        initMoveSpeed = moveSpeed;
        if (snapSpeed < 1) snapSpeed = 1;
    }

    public void LoadData(List<IScrollerItemData> listItemData)
    {
        GameObject scrollPanelObject = scrollPanel.gameObject;

        foreach (Transform child in scrollPanelObject.GetComponent<Transform>())
        {
            Destroy(child.gameObject);
        }

        //do nothing with empty list
        if (listItemData.Count < 1)
        {
            selectedItemIndex = 0;
            return;
        }

        // reset anchored position to previous position if update scroll data
        if (itemAnchoredPositions != null && itemAnchoredPositions.Count > 0)
        {
            if (lastSelectedItemIndex > 0)
            {
                scrollPanel.anchoredPosition = new Vector2(-itemAnchoredPositions[lastSelectedItemIndex].x, scrollPanel.anchoredPosition.y);
            }
            else
            {
                scrollPanel.anchoredPosition = initAnchoredPosition;
            }
        }

        itemLength = 0;
        items = new List<GameObject>();
        itemAnchoredPositions = new List<Vector2>();

        foreach (IScrollerItemData itemData in listItemData)
        {
            var instanceItemPrefab = Instantiate(itemPrefab, scrollPanelObject.GetComponent<Transform>());
            instanceItemPrefab.GetComponent<RectTransform>().anchoredPosition = new Vector2(itemLength * itemWidth, instanceItemPrefab.GetComponent<RectTransform>().anchoredPosition.y);
            instanceItemPrefab.GetComponent<IScrollerItem>().Setup(itemData);

            if (snapByClick)
            {
                if (instanceItemPrefab.GetComponent<Button>() == null)
                    instanceItemPrefab.AddComponent<Button>();

                Button itemButton = instanceItemPrefab.GetComponent<Button>();

                itemButton.onClick.RemoveAllListeners();
                itemButton.onClick.AddListener( delegate { onItemClick.Invoke(instanceItemPrefab.GetComponent<IScrollerItem>()); } );
            }

            items.Add(instanceItemPrefab);
            itemLength++;
        }

        distances = new float[items.Count];
        distancePosition = new float[items.Count];
        itemWidthInWorld = items[1].GetComponent<RectTransform>().position.x - items[0].GetComponent<RectTransform>().position.x;

        // refresh view
        moveSpeed = initMoveSpeed;
        init = false;
        isInvokingSelectionCallback = false;
        isInvokingEndScrollCallback = false;
        isInvokingStartScrollCallback = false;
        autoSelectItem = true;
    }

    void Update()
    {
        if (!Application.isPlaying || items.Count == 0) return;

        float scrollBound = (items.Count - (float) Math.Floor(leadingLine.position.x / itemWidthInWorld)) * itemWidthInWorld / 2;

        for (int i = 0; i < items.Count; i++)
        {
            distancePosition[i] = centerLine.GetComponent<RectTransform>().position.x - items[i].GetComponent<RectTransform>().position.x;
            distances[i] = Mathf.Abs(distancePosition[i]);

            //float distanceItemPosition = items[i].GetComponent<RectTransform>().anchoredPosition.x;
            float itemPosition = items[i].GetComponent<RectTransform>().position.x;
            if (itemPosition < 0 && itemPosition < -scrollBound)
            {
                MoveToTheEnd(i);
            }

            if (itemPosition > 0 && itemPosition > scrollBound + leadingLine.position.x)
            {
                MoveToTheStart(i);
            }
        }

        if(!init)
        {
            for (int i = 0; i < items.Count; i++)
            {
                itemAnchoredPositions.Insert(i, items[i].GetComponent<RectTransform>().anchoredPosition);
            }                
            init = true;
        }

        if (!isDragging)
        {
            if (scrollRect.velocity.magnitude < 17.82f * snapSpeed)
            {
                isScrolling = false;
                if(!isInvokingEndScrollCallback && OnEndScroll != null)
                {
                    OnEndScroll.Invoke();
                    isInvokingEndScrollCallback = true;
                }
            }
        }

        if (!isScrolling && !isDoneSelect)
        {
            if (autoSelectItem)
            {
                float minDistance = Mathf.Min(distances);

                for (int i = 0; i < items.Count; i++)
                {
                    if (minDistance >= distances[i])
                    {
                        selectedItemIndex = i;
                        break;
                    }
                }
            }

            // reset anchored position to initial position
            if (Mathf.Abs(scrollPanel.anchoredPosition.x) > Mathf.Ceil(itemLength / 2) * itemWidth && lastSelectedItemIndex != selectedItemIndex)
            {
                // reset items
                for (int i = 0; i < items.Count; i++)
                {
                    items[i].GetComponent<RectTransform>().anchoredPosition = itemAnchoredPositions[i];
                    //if (i != selectedItemIndex) items[i].SetActive(false);
                }
                // reset scroll panel
                scrollPanel.anchoredPosition = new Vector2(-itemAnchoredPositions[lastSelectedItemIndex].x, scrollPanel.anchoredPosition.y);

                //for (int i = 0; i < items.Count; i++)
                //{
                //    items[i].SetActive(true);
                //}
            }

            LerpToCenter(-items[selectedItemIndex].GetComponent<RectTransform>().anchoredPosition.x);

            lastSelectedItemIndex = selectedItemIndex;
        }
    }

    void LerpToCenter(float position)
    {
        float newX = Mathf.Lerp(scrollPanel.anchoredPosition.x, position, Time.deltaTime * moveSpeed);

        moveSpeed *= snapSpeed;
        // snap item
        if (Mathf.Abs(position - newX) < snapSpeed)
        {
            newX = position;

            // DONE
            isDoneSelect = true;
        }

        if (Mathf.Abs(newX) >= Mathf.Abs(position) - 1f && Mathf.Abs(newX) <= Mathf.Abs(position) + 1f && !isInvokingSelectionCallback)
        {
            isInvokingSelectionCallback = true;
            items[selectedItemIndex].GetComponent<IScrollerItem>().Activate();

            Debug.Log("invoke seletion call back for item " + items[selectedItemIndex].name);
            OnSelectedItem.Invoke(items[selectedItemIndex].GetComponent<IScrollerItem>());
        }

        Vector2 newPosition = new Vector2(newX, scrollPanel.anchoredPosition.y);
        scrollPanel.anchoredPosition = newPosition;
    }

    public void GotoItem(int index)
    {
        items[selectedItemIndex].GetComponent<IScrollerItem>().Deactivate();
        moveSpeed = initMoveSpeed;
        isInvokingSelectionCallback = false;
        autoSelectItem = false;
        isDoneSelect = false;
        selectedItemIndex = index;
    }

    public GameObject GetSelected()
    {
        return items[selectedItemIndex];
    }

    public IScrollerItem GetSelectedItem()
    {
        return GetItem(selectedItemIndex);
    }

    public IScrollerItem GetItem(int index)
    {
        return items[index].GetComponent<IScrollerItem>();
    }

    void MoveToTheEnd(int i)
    {
        float currentX = items[i].GetComponent<RectTransform>().anchoredPosition.x;
        float currentY = items[i].GetComponent<RectTransform>().anchoredPosition.y;

        Vector2 newPosition = new Vector2(currentX + (itemLength * itemWidth), currentY);
        items[i].GetComponent<RectTransform>().anchoredPosition = newPosition;
    }

    void MoveToTheStart(int i)
    {
        float currentX = items[i].GetComponent<RectTransform>().anchoredPosition.x;
        float currentY = items[i].GetComponent<RectTransform>().anchoredPosition.y;

        Vector2 newPosition = new Vector2(currentX - (itemLength * itemWidth), currentY);
        items[i].GetComponent<RectTransform>().anchoredPosition = newPosition;
    }

    public void StartDrag()
    {
        for (int i = 0; i < items.Count; i++)
        {
            items[i].GetComponent<IScrollerItem>().Deactivate();
        }

        if (!isInvokingStartScrollCallback && OnStartScroll != null)
        {
            OnStartScroll.Invoke();
            isInvokingStartScrollCallback = true;
        }

        moveSpeed = initMoveSpeed;
        isInvokingSelectionCallback = false;
        isInvokingEndScrollCallback = false;
        isDragging = true;
        isScrolling = true;
        isDoneSelect = false;
        autoSelectItem = true;
    }

    public void EndDrag()
    {
        isDragging = false;
        isInvokingStartScrollCallback = false;
    }

    public void Reset(bool isFullReset = true)
    {
        if (isFullReset)
        {
            scrollPanel.anchoredPosition = initAnchoredPosition;
            lastSelectedItemIndex = selectedItemIndex = 0;
            init = false;
        }
        moveSpeed = initMoveSpeed;
        isInvokingSelectionCallback = false;
        isInvokingEndScrollCallback = false;
        isInvokingStartScrollCallback = false;
        autoSelectItem = true;
    }
}
