﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NativeEditBoxHelper : MonoBehaviour
{

    NativeEditBox nativeEditBox;

    [SerializeField]
    Page page;

    void Awake ()
    {
        nativeEditBox = GetComponent<NativeEditBox> ();

        this.page.onShow += OnShow;
        this.page.onHide += OnHide;
        this.page.nativeEditBoxHelpers.Add (this);
    }

    void OnShow ()
    {
        Show ();
    }

    void OnHide ()
    {
        Hide ();
    }

    public void Show ()
    {
        nativeEditBox.SetVisible (true);
    }

    public void Hide ()
    {
        nativeEditBox.SetVisible (false);
    }
}
