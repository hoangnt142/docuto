﻿using System.IO;
using UnityEditor;
using UnityEditor.Callbacks;
#if UNITY_IOS
using UnityEditor.iOS.Xcode;

public class UnityIOSPostprocess
{
    /// <summary>
    /// ビルド後の処理( Android / iOS共通 )
    /// </summary>
    [PostProcessBuild (1)]
    public static void OnPostProcessBuild (BuildTarget target, string path)
    {
        // iOSじゃなければ処理しない
        if (target != BuildTarget.iOS) {
            return;
        }

        AddUrlScheme (path);
    }

    /// <summary>
    /// URLスキームを追加します
    /// </summary>
    /// <param name="path"> 出力先のパス </param>
    public static void AddUrlScheme (string path)
    {
        var plistPath = Path.Combine (path, "Info.plist");
        var plist = new PlistDocument ();

        // 読み込み
        plist.ReadFromFile (plistPath);

        // ほぼ定型文
        var urlTypeArray = plist.root.CreateArray ("CFBundleURLTypes");
        var urlTypeDict = urlTypeArray.AddDict ();
        var urlSchemeArray = urlTypeDict.CreateArray ("CFBundleURLSchemes");

        // URLスキームを追加
        urlSchemeArray.AddString ("benefitness");

        // 書き込み
        plist.WriteToFile (plistPath);
    }
}
#endif
